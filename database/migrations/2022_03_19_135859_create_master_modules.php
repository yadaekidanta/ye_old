<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterModules extends Migration
{
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });
        Schema::create('days', function (Blueprint $table) {
            $table->string('d',2)->primaryKey();
        });
        Schema::create('faq_categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });
        Schema::create('faqs', function (Blueprint $table) {
            $table->id();
            $table->integer('faq_category_id')->default(0);
            $table->longText('question');
            $table->longText('answer');
        });
        Schema::create('isic_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });
        Schema::create('isics', function (Blueprint $table) {
            $table->id();
            $table->integer('isic_type_id')->default(0);
            $table->string('code');
            $table->string('name');
        });
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->longText('title');
            $table->longText('slug');
            $table->longText('body');
            $table->string('thumbnail');
            $table->string('is_public',1);
            $table->string('viewer',5);
            $table->integer('created_by')->default(0);
            $table->timestamps();
        });
        Schema::create('new_likes', function (Blueprint $table) {
            $table->id();
            $table->integer('new_id');
            $table->integer('employee_id')->nullable();
            $table->timestamps();
        });
        Schema::create('new_comments', function (Blueprint $table) {
            $table->id();
            $table->integer('new_id');
            $table->integer('employee_id')->nullable();
            $table->longText('body')->nullable();
            $table->timestamps();
        });
        Schema::create('reference_numbers', function (Blueprint $table) {
            $table->id();
            $table->string('code',10);
            $table->string('name');
        });
        Schema::create('legal_doc_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });
        Schema::create('legal_docs', function (Blueprint $table) {
            $table->id();
            $table->integer('doc_type_id')->default(0);
            $table->string('code');
            $table->string('attachment');
        });
        Schema::create('legal_policies', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->longText('body');
        });
        Schema::create('product_categories', function (Blueprint $table) {
            $table->id();
            $table->integer('product_category_id')->default(0);
            $table->string('name');
            $table->longText('description')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('tag_title')->nullable();
            $table->longText('tag_description')->nullable();
            $table->string('tag_keyword')->nullable();
            $table->enum('st',['Published','Unpublished'])->nullable();
        });
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('product_category_id')->default(0);
            $table->string('title');
            $table->string('slug')->nullable();
            $table->longText('description')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('tags');
            $table->string('demo_url');
            $table->string('regular_price')->default(0);
            $table->string('extend_price')->default(0);
            $table->string('is_regular_price')->default(0);
            $table->string('is_extend_price')->default(0);
            $table->string('regular_discount',2)->default(0);
            $table->string('extend_discount',2)->default(0);
            $table->enum('st',['Published','Unpublished'])->nullable();
            $table->integer('created_by')->default(0);
            $table->timestamps();
        });
        Schema::create('product_files', function (Blueprint $table) {
            $table->id();
            $table->integer('product_id')->default(0);
            $table->string('files');
            $table->string('is_extend')->default(0);
            $table->timestamps();
        });
        Schema::create('product_galleries', function (Blueprint $table) {
            $table->id();
            $table->integer('product_id')->default(0);
            $table->string('photo');
        });
        Schema::create('product_reviews', function (Blueprint $table) {
            $table->id();
            $table->integer('product_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('rate',3)->nullable();
            $table->longtext('comment')->nullable();
            $table->timestamps();
        });
        Schema::create('media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->morphs('model');
            $table->string('collection_name');
            $table->string('name');
            $table->string('file_name');
            $table->string('mime_type')->nullable();
            $table->string('disk');
            $table->unsignedBigInteger('size');
            $table->json('manipulations');
            $table->json('custom_properties');
            $table->json('responsive_images');
            $table->unsignedInteger('order_column')->nullable();
            $table->nullableTimestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('banks');
        Schema::dropIfExists('days');
        Schema::dropIfExists('faq_categories');
        Schema::dropIfExists('faqs');
        Schema::dropIfExists('isic_types');
        Schema::dropIfExists('isics');
        Schema::dropIfExists('legal_doc_types');
        Schema::dropIfExists('legal_docs');
        Schema::dropIfExists('legal_policies');
        Schema::dropIfExists('product_categories');
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_files');
        Schema::dropIfExists('product_galleries');
        Schema::dropIfExists('product_reviews');
        Schema::dropIfExists('media');
        Schema::dropIfExists('news');
        Schema::dropIfExists('new_likes');
        Schema::dropIfExists('new_comments');
        Schema::dropIfExists('reference_numbers');
    }
}
