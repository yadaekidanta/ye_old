<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingModules extends Migration
{
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('address');
            $table->string('phone');
            $table->string('email');
            $table->string('logo');
            $table->string('icon');
            $table->timestamps();
        });
        Schema::create('company_banks', function (Blueprint $table) {
            $table->id();
            $table->integer('bank_id')->default(0);
            $table->string('name');
            $table->string('account_no',25);
            $table->string('branch_name');
            $table->timestamps();
        });
        Schema::create('mails', function (Blueprint $table) {
            $table->id();
            $table->string('driver');
            $table->string('host');
            $table->string('port');
            $table->string('from_address');
            $table->string('from_name');
            $table->string('encryption');
            $table->string('username');
            $table->string('password');
        });
        Schema::create('histories', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('year',4)->nullable();
        });
        Schema::create('banners', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('banner')->nullable();
            $table->enum('pages',['home','about','service','product','case_studies','contact'])->nullable();
        });
        Schema::create('contents', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->longText('description')->nullable();
            $table->string('banner')->nullable();
            $table->enum('type',['about','visi_misi','what_we_do','our_strategy','why_choose_us','our_process','join_team','what_makes_us_different'])->nullable();
        });
        Schema::create('announcements', function (Blueprint $table) {
            $table->id();
            $table->integer('employee_id')->default(0);
            $table->longText('to');
            $table->longText('subject');
            $table->longText('messages');
            $table->enum('type',['General','Interview','Meeting','Mutasi','Payroll','Promotion','Punishment','Others']);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('companies');
        Schema::dropIfExists('company_banks');
        Schema::dropIfExists('mails');
        Schema::dropIfExists('histories');
        Schema::dropIfExists('banner');
        Schema::dropIfExists('contents');
        Schema::dropIfExists('announcements');
    }
}
