<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatModules extends Migration
{
    public function up()
    {
        Schema::create('chat_messages', function (Blueprint $table) {
            $table->id();
            $table->longText('body');
            $table->bigInteger('from_id')->index()->nullable();
            $table->bigInteger('to_id')->index()->nullable();
            $table->enum('type',['c','e'])->default('c');
            $table->timestamp('read_at')->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('chat_messages');
    }
}
