<?php

use App\Models\Legality\LegalDoc;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Office\AuthController;
use App\Http\Controllers\Office\MainController;
use App\Http\Controllers\Office\ProfileController;
use App\Http\Controllers\Office\CRM\LeadController;
use App\Http\Controllers\Office\CRM\ClientController;
use App\Http\Controllers\Office\HRM\DayOffController;
use App\Http\Controllers\Office\Master\BankController;
use App\Http\Controllers\Office\Master\IsicController;
use App\Http\Controllers\Office\Setting\FaqController;
use App\Http\Controllers\Office\CRM\CustomerController;
use App\Http\Controllers\Office\HRM\CalenderController;
use App\Http\Controllers\Office\HRM\EmployeeController;
use App\Http\Controllers\Office\HRM\PositionController;
use App\Http\Controllers\Office\JobApplicantsController;
use App\Http\Controllers\Office\Regional\CityController;
use App\Http\Controllers\Office\HRM\DepartmentController;
use App\Http\Controllers\Office\HRM\VacancyJobController;
use App\Http\Controllers\Office\Master\ProductController;
use App\Http\Controllers\Office\HRM\EmployeeKpiController;
use App\Http\Controllers\Office\Master\IsicTypeController;
use App\Http\Controllers\Office\HRM\EmployeeMemoController;
use App\Http\Controllers\Office\Regional\CountryController;
use App\Http\Controllers\Office\Regional\VillageController;
use App\Http\Controllers\Office\Legality\LegalDocController;
use App\Http\Controllers\Office\Regional\ProvinceController;
use App\Http\Controllers\Office\Communication\ChatController;
use App\Http\Controllers\Office\HRM\JobApplicationController;
use App\Http\Controllers\Office\Master\DueReminderController;
use App\Http\Controllers\Office\Master\ProductFileController;
use App\Http\Controllers\Office\Setting\FaqCategoryController;
use App\Http\Controllers\Office\Legality\LegalPolicyController;
use App\Http\Controllers\Office\Regional\SubdistrictController;
use App\Http\Controllers\Office\Legality\LegalDocTypeController;
use App\Http\Controllers\Office\Master\ProductGalleryController;
use App\Http\Controllers\Office\HRM\EmployeeAttendanceController;
use App\Http\Controllers\Office\Master\ProductCategoryController;
use App\Http\Controllers\Office\HRM\EmployeeBankAccountController;
use App\Http\Controllers\Office\HRM\EmployeeKpiKeyResultController;
use App\Http\Controllers\Office\HRM\EmployeeKpiObjectiveController;
use App\Http\Controllers\Office\Legality\ReferenceNumberController;
use App\Http\Controllers\Office\Communication\NotificationController;

Route::group(['domain' => ''], function() {
    Route::redirect('/','auth',302);
    Route::prefix('office')->name('office.')->group(function(){
        Route::get('lang/{language}',[MainController::class, 'switch'])->name('switch.lang');
        Route::prefix('auth')->name('auth.')->group(function(){
            Route::get('',[AuthController::class, 'index'])->name('index');
            Route::post('login',[AuthController::class, 'do_login'])->name('login');
            Route::post('register',[AuthController::class, 'do_register'])->name('register');
            Route::post('forgot',[AuthController::class, 'do_forgot'])->name('forgot');
            Route::get('reset/{token}',[AuthController::class, 'reset'])->name('reset');
            Route::post('reset',[AuthController::class, 'do_reset'])->name('do_reset');
        });
        Route::middleware(['auth:office'])->group(function(){
            Route::get('dashboard', [MainController::class, 'index'])->name('dashboard');
            Route::get('counter-chat', [ChatController::class, 'counter'])->name('counter_chat');
            Route::get('counter-notif', [NotificationController::class, 'counter'])->name('counter_notif');
            Route::get('notification', [NotificationController::class, 'index'])->name('notification');
            // MASTER
            Route::resource('product-category', ProductCategoryController::class);
            Route::resource('{productCategory}/product', ProductController::class);
            Route::resource('bank', BankController::class);

            Route::resource('isic-type', IsicTypeController::class);
            Route::resource('{isicType}/isic', IsicController::class);
            Route::resource('country', CountryController::class);
            Route::resource('{country}/province', ProvinceController::class);
            Route::resource('{province}/city', CityController::class);
            Route::resource('{city}/district', SubdistrictController::class);
            Route::resource('{district}/village', VillageController::class);
            Route::resource('due-reminder', DueReminderController::class);
            
            Route::resource('subdistrict', SubdistrictController::class);
            Route::post('province/get_city',[ProvinceController::class, 'get_city'])->name('province.get_city');
            Route::post('city/get_subdistrict',[CityController::class, 'get_subdistrict'])->name('city.get_subdistrict');
            Route::post('subdistrict/get_postcode',[SubdistrictController::class, 'get_postcode'])->name('subdistrict.get_postcode');
            
            // CRM
            Route::post('customer/delete-checked',[CustomerController::class, 'destroy_checked'])->name('customer.destroy_checked');
            Route::resource('customer', CustomerController::class);

            Route::post('partner/delete-checked',[PartnerController::class, 'destroy_checked'])->name('partner.destroy_checked');
            Route::resource('partner', PartnerController::class);
            
            Route::post('lead/delete-checked',[LeadController::class, 'destroy_checked'])->name('lead.destroy_checked');
            Route::resource('lead', LeadController::class);
            Route::post('product-gallery/upload',[ProductGalleryController::class, 'upload'])->name('product-gallery.store');
            Route::post('product-file/upload',[ProductFileController::class, 'upload'])->name('product-file.store');
            Route::post('product-category/create-modal',[ProductCategoryController::class, 'create_modal'])->name('product-category.create_modal');
            Route::post('lead/{lead}/accept',[LeadController::class, 'accept'])->name('lead.accept');
            Route::post('client/delete-checked',[ClientController::class, 'destroy_checked'])->name('client.destroy_checked');
            Route::resource('client', ClientController::class);

            // HRM
            Route::resource('memo', EmployeeMemoController::class);
            Route::resource('department', DepartmentController::class);
            Route::post('department/get_position',[DepartmentController::class, 'get_position'])->name('department.get_position');
            Route::post('department/delete-checked',[DepartmentController::class, 'destroy_checked'])->name('department.destroy_checked');
            Route::post('{department}/position/delete-checked',[PositionController::class, 'destroy_checked'])->name('position.destroy_checked');
            Route::resource('{department}/position', PositionController::class);
            Route::post('employee/delete-checked',[EmployeeController::class, 'destroy_checked'])->name('employee.destroy_checked');
            Route::post('employee/upload',[EmployeeController::class, 'upload_cv'])->name('employee.upload_cv');
            Route::resource('employee', EmployeeController::class);
            Route::post('vacancy/delete-checked',[VacancyJobController::class, 'destroy_checked'])->name('vacancy.destroy_checked');
            Route::patch('vacancy/{vacancy}/open',[VacancyJobController::class, 'open'])->name('vacancy.open');
            Route::patch('vacancy/{vacancy}/close',[VacancyJobController::class, 'close'])->name('vacancy.close');
            Route::resource('attendance', EmployeeAttendanceController::class);
            Route::resource('vacancy', VacancyJobController::class);
            Route::post('kpi/delete-checked',[EmployeeKpiController::class, 'destroy_checked'])->name('kpi.destroy_checked');
            Route::resource('kpi', EmployeeKpiController::class);
            Route::post('{kpi}/kpi-objective/delete-checked',[EmployeeKpiObjectiveController::class, 'destroy_checked'])->name('kpi-objective.destroy_checked');
            Route::resource('{kpi}/kpi-objective', EmployeeKpiObjectiveController::class);
            Route::get('kpi-objective/{kpiObjective}',[EmployeeKpiObjectiveController::class, 'show'])->name('kpi-objective.show');
            Route::post('{kpi-objective}/kpi-key/delete-checked',[EmployeeKpiKeyResultController::class, 'destroy_checked'])->name('kpi-key.destroy_checked');
            Route::resource('{kpi-objective}/kpi-key', EmployeeKpiKeyResultController::class);
            Route::post('day-off/delete-checked',[DayOffController::class, 'destroy_checked'])->name('day-off.destroy_checked');
            Route::resource('day-off', DayOffController::class);
            Route::post('calender/delete-checked',[CalenderController::class, 'destroy_checked'])->name('calender.destroy_checked');
            Route::resource('calender', CalenderController::class);
            Route::post('my-banks/delete-checked',[EmployeeBankAccountController::class, 'destroy_checked'])->name('my-banks.destroy_checked');
            Route::resource('my-banks', EmployeeBankAccountController::class);
            // LEGALITY

            Route::post('legal-doc-type/delete-checked',[LegalDocTypeController::class, 'destroy_checked'])->name('legal-doc-type.destroy_checked');
            Route::resource('legal-doc-type', LegalDocTypeController::class);
            
            Route::post('legal-doc/delete-checked',[LegalDocController::class, 'destroy_checked'])->name('legal-doc.destroy_checked');
            Route::resource('legal-doc', LegalDocController::class);
            Route::post('legal-policy/delete-checked',[LegalPolicyController::class, 'destroy_checked'])->name('legal-policy.destroy_checked');
            Route::resource('legal-policy', LegalPolicyController::class);
            Route::post('reference-number/delete-checked',[ReferenceNumberController::class, 'destroy_checked'])->name('reference-number.destroy_checked');
            Route::resource('reference-number', ReferenceNumberController::class);
            // CHAT
            Route::get('chat/{employee}',[ChatController::class, 'show'])->name('chat.show');
            Route::resource('chat', ChatController::class);
            // SALE
            Route::post('voucher/delete-checked',[VoucherController::class, 'destroy_checked'])->name('voucher.destroy_checked');
            Route::resource('voucher', VoucherController::class);
            Route::post('sale/delete-checked',[SaleController::class, 'destroy_checked'])->name('sale.destroy_checked');
            Route::resource('sale', SaleController::class);
            // SETTING
            Route::get('setting', [MainController::class, 'setting'])->name('setting');
            Route::post('faq/delete-checked',[FaqController::class, 'destroy_checked'])->name('faq.destroy_checked');
            Route::resource('{faqCategory}/faq', FaqController::class);
            Route::post('faq-category/delete-checked',[FaqCategoryController::class, 'destroy_checked'])->name('faq-category.destroy_checked');
            Route::resource('faq-category', FaqCategoryController::class);
            
            // PROFILE
            Route::resource('profile', ProfileController::class);
            Route::resource('my-applicants', JobApplicantsController::class);
            Route::resource('job-applicants', JobApplicationController::class);
            Route::patch('job_applicant/{job_applicant}/reject',[JobApplicationController::class, 'reject'])->name('job-applicants.reject');
            Route::patch('job_applicant/{job_applicant}/accept',[JobApplicationController::class, 'accept'])->name('job-applicants.accept');
            
            // Legality
            // Route::resource('legal-doc', LegalDocController::class);
            // Route::resource('legal-doc-type', LegalDocTypeController::class);
            // Route::resource('legal-policy', LegalPolicyController::class);
            Route::get('logout',[AuthController::class, 'do_logout'])->name('auth.logout');
        });
    });
});