<style>html,body { padding: 0; margin:0; }</style>
<div style="font-family:Arial,Helvetica,sans-serif; line-height: 1.5; font-weight: normal; font-size: 15px; color: #2F3044; min-height: 100%; margin:0; padding:0; width:100%; background-color:#edf2f7">
	<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;margin:0 auto; padding:0; max-width:600px">
		<tbody>
			<tr>
				<td align="center" valign="center" style="text-align:center; padding: 40px">
					<a href="https://yadaekidanta.com" rel="noopener" target="_blank">
						<img src="https://yadaekidanta.com/img/icon.png" style="height: 45px" alt="logo">
							<tr>
								<td align="left" valign="center">
									<div style="text-align:left; margin: 0 20px; padding: 40px; background-color:#ffffff; border-radius: 6px">
										<!--begin:Email content-->
										<div style="padding-bottom: 30px; font-size: 17px;">
                                            Dear, <br>
                                            In place <br>
                                            Yours faithfully,
										</div>
                                        <div style="padding-bottom: 30px">
											Yada Ekidanta would like to thank you for your enthusiasm in the series of {{$jabatan}} interviews which were held on {{\Carbon\Carbon::now()->format('l, j F Y')}}. <br>
											At the same time, we as the Yada Ekidanta Team would like to inform you that you have <b>Not Passed</b> at the interview stage.
										</div>
                                        <div style="padding-bottom: 30px">
                                            Keep the spirit and don't give up, hopefully this is the beginning of your journey in building a higher career path! <br>
                                            <q>The pain of fighting is only temporary. You can feel it for a minute, an hour, a day, or a year. But if you give up, the pain will last forever.</q> ~ (Lance Armstrong)
                                        </div>
										<!--end:Email content-->
										<div style="padding-bottom: 10px">Kind regards,
										<br>Yada Ekidanta Team.
										<tr>
											<td align="center" valign="center" style="font-size: 13px; text-align:center;padding: 20px; color: #6d6e7c;">
												<p>Komplek Permata Buah Batu blok C 15B, Bandung, Jawa Barat, 40287.</p>
												<p>Copyright ©
												<a href="https://yadaekidanta.com" rel="noopener" target="_blank">Yada Ekidanta</a>.</p>
											</td>
										</tr></br></div>
									</div>
								</td>
							</tr>
						</img>
					</a>
				</td>
			</tr>
		</tbody>
	</table>
</div>