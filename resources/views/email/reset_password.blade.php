<style>html,body { padding: 0; margin:0; }</style>
<div style="font-family:Arial,Helvetica,sans-serif; line-height: 1.5; font-weight: normal; font-size: 15px; color: #2F3044; min-height: 100%; margin:0; padding:0; width:100%; background-color:#edf2f7">
	<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;margin:0 auto; padding:0; max-width:600px">
		<tbody>
			<tr>
				<td align="center" valign="center" style="text-align:center; padding: 40px">
					<a href="https://yadaekidanta.com" rel="noopener" target="_blank">
						<img src="https://yadaekidanta.com/img/icon.png" style="height: 45px" alt="logo">
							<tr>
								<td align="left" valign="center">
									<div style="text-align:left; margin: 0 20px; padding: 40px; background-color:#ffffff; border-radius: 6px">
										<!--begin:Email content-->
										<div style="padding-bottom: 30px; font-size: 17px;">
											<strong>Hi {{$data['employee']->name}},</strong>
										</div>
										<div style="padding-bottom: 30px">
											Sorry to hear you’re having trouble logging into {{config('app.name')}}. We got a message that you forgot your password. If this was you, you can get right back into your account or reset your password now.
										</div>
                                        <div style="padding-bottom: 40px; text-align:center;">
											<a href="{{route('office.auth.reset',$data['token'])}}" rel="noopener" style="text-decoration:none;display:inline-block;text-align:center;padding:0.75575rem 1.3rem;font-size:0.925rem;line-height:1.5;border-radius:0.35rem;color:#ffffff;background-color:#5BC7DD;border:0px;margin-right:0.75rem!important;font-weight:600!important;outline:none!important;vertical-align:middle" target="_blank">Reset Password</a>
										</div>
                                        <div style="padding-bottom: 30px">This password reset link will expire in 60 minutes. If you did not request a password reset, no further action is required.</div>
										<div style="padding-bottom: 30px">
											If you didn’t request a login link or a password reset, you can ignore this message and learn more about why you may have received it.
										</div>
										<!--end:Email content-->
										<div style="padding-bottom: 10px">Kind regards,
										<br>Yada Ekidanta Team.
										<tr>
											<td align="center" valign="center" style="font-size: 13px; text-align:center;padding: 20px; color: #6d6e7c;">
												<p>Komplek Permata Buah Batu blok C 15B, Bandung, Jawa Barat, 40287.</p>
												<p>Copyright ©
												<a href="https://yadaekidanta.com" rel="noopener" target="_blank">Yada Ekidanta</a>.</p>
											</td>
										</tr></br></div>
									</div>
								</td>
							</tr>
						</img>
					</a>
				</td>
			</tr>
		</tbody>
	</table>
</div>