@if($data->id)
@php
$explode_month = Str::beforeLast(date('Y-m-01'),'-');
$month = Str::after($explode_month,'-');
$year = Str::before(date('Y-m-01'),'-');
$date1 = date('Y-m-01');
$date2 = date("Y-m-d");

$diff = abs(strtotime($date2) - strtotime($date1));

$years = floor($diff/(365*60*60*24));
$months = floor(($diff-$years*365*60*60*24)/(30*60*60*24));
$total_month = number_format($months,0)+1;
$days = DB::select(DB::raw(" 
SELECT
    DATE_FORMAT(cal.my_date, '%e %b %Y') as tanggal,
    COUNT( t.id ) AS total 
FROM
    (
    SELECT
        s.start_date + INTERVAL ( days.d ) DAY AS my_date 
    FROM
        ( SELECT LAST_DAY( CURRENT_DATE ) + INTERVAL 1 DAY - INTERVAL $total_month MONTH AS start_date, LAST_DAY( CURRENT_DATE ) AS end_date ) AS s
    LEFT JOIN days ON days.d <= DATEDIFF( s.end_date, s.start_date ) ) AS cal LEFT JOIN employee_attendances AS t ON DATE_FORMAT(presence_at, '%Y-%m-%d') >= cal.my_date 
    AND DATE_FORMAT(presence_at, '%Y-%m-%d') < cal.my_date + INTERVAL 1 DAY 
WHERE
    MONTH ( my_date ) = '$month' AND YEAR(my_date) = '$year'
GROUP BY
    tanggal
ORDER BY
    LENGTH(tanggal), tanggal
"));
@endphp
@endif
<div class="toolbar" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
        <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
            <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">HRM</h1>
            <span class="h-20px border-gray-300 border-start mx-4"></span>
            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                <li class="breadcrumb-item text-muted">{{__('menu.employee')}}</li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-300 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{$data->id ? 'Update '. $data->name : 'Add new'}}</li>
            </ul>
        </div>
        <div class="d-flex align-items-center gap-2 gap-lg-3">
            <a href="javascript:;" onclick="main_content('content_list');" class="btn btn-sm btn-primary">Back</a>
        </div>
    </div>
</div>
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <form id="form_input" class="form d-flex flex-column flex-lg-row">
            <div class="d-flex flex-column gap-7 gap-lg-10 w-100 w-lg-300px mb-7 me-lg-10">
                <div class="card card-flush py-4">
                    <div class="card-header">
                        <div class="card-title">
                            <h2>Avatar</h2>
                        </div>
                    </div>
                    <div class="card-body text-center pt-0">
                        <div class="image-input image-input-empty image-input-outline mb-3" data-kt-image-input="true" style="">
                            <div class="image-input-wrapper w-150px h-150px" style="background-image: url(assets/media//stock/ecommerce/78.gif)"></div>
                            <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                                <i class="bi bi-pencil-fill fs-7"></i>
                                <input type="file" name="avatar" accept=".png, .jpg, .jpeg" />
                                <input type="hidden" name="avatar_remove" />
                            </label>
                            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                                <i class="bi bi-x fs-2"></i>
                            </span>
                            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                                <i class="bi bi-x fs-2"></i>
                            </span>
                        </div>
                        <div class="text-muted fs-7">Set the client avatar image. Only *.png, *.jpg and *.jpeg image files are accepted</div>
                    </div>
                </div>
                <div class="card card-flush py-4">
                    <div class="card-header">
                        <div class="card-title">
                            <h2>Upload CV</h2>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <input type="file" name="cv" class="form-control">
                        @if($data->cv)
                        <a download target="_blank" href="{{asset('storage/'.$data->cv)}}" class="btn mt-3 btn-small btn-bg-light btn-active-color-primary btn-sm me-1">
                            <span class="svg-icon svg-icon-3">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M19 15C20.7 15 22 13.7 22 12C22 10.3 20.7 9 19 9C18.9 9 18.9 9 18.8 9C18.9 8.7 19 8.3 19 8C19 6.3 17.7 5 16 5C15.4 5 14.8 5.2 14.3 5.5C13.4 4 11.8 3 10 3C7.2 3 5 5.2 5 8C5 8.3 5 8.7 5.1 9H5C3.3 9 2 10.3 2 12C2 13.7 3.3 15 5 15H19Z" fill="currentColor"/>
                                    <path d="M13 17.4V12C13 11.4 12.6 11 12 11C11.4 11 11 11.4 11 12V17.4H13Z" fill="currentColor"/>
                                    <path opacity="0.3" d="M8 17.4H16L12.7 20.7C12.3 21.1 11.7 21.1 11.3 20.7L8 17.4Z" fill="currentColor"/>
                                </svg>
                            </span>
                            Download
                        </a>
                        @endif
                        <div class="dropzone d-none">
                            <div class="dz-message needsclick">
                                <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                                <div class="ms-4">
                                    <h3 class="fs-5 fw-bolder text-gray-900 mb-1">Drop files here or click to upload.</h3>
                                    <span class="fs-7 fw-bold text-gray-400">Upload up only 1 files</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-flush py-4">
                    <div class="card-header">
                        <div class="card-title">
                            <h2>Department & Position</h2>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <select class="form-select mb-2" id="department" name="department">
                            <option value="">Choose Department</option>
                            @foreach ($department as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                        <div class="text-muted fs-7 mb-2">Set the employee department.</div>
                        <select class="form-select mb-2" id="position" name="position">
                            <option value="">Choose Department First</option>
                        </select>
                        <div class="text-muted fs-7">Set the employee position.</div>
                    </div>
                </div>
                <div class="card card-flush py-4">
                    <div class="card-header">
                        <div class="card-title">
                            <h2>Status</h2>
                        </div>
                        <div class="card-toolbar">
                            <div class="rounded-circle w-15px h-15px" id="employee_status"></div>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <select class="form-select mb-2" id="status" name="st">
                            <option value="">Pilih Status</option>
                            <option value="a">Active</option>
                            <option value="n">Non Active</option>
                        </select>
                        <div class="text-muted fs-7">Set the employee status.</div>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10">
                <ul class="nav nav-custom nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-bold mb-n2">
                    <li class="nav-item">
                        <a class="nav-link text-active-primary pb-4 active" data-bs-toggle="tab" href="#tab_general">General</a>
                    </li>
                    @if($data->id)
                    <li class="nav-item">
                        <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#tab_activities">Activities</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#tab_attendances">Attendances</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#tab_certificate">Certificates</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#tab_rewards">Rewards</a>
                    </li>
                    @endif
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="tab_general" role="tab-panel">
                        <div class="d-flex flex-column gap-7 gap-lg-10">
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>General</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="mb-10 fv-row">
                                        <label class="required form-label">NIP</label>
                                        <input type="text" name="nip" class="form-control mb-2" placeholder="Eg : 210521111" value="{{ $data->nip }}" />
                                        <div class="text-muted fs-7">A Employee ID is required and recommended to be unique.</div>
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label class="required form-label">Name</label>
                                        <input type="text" name="name" class="form-control mb-2" placeholder="Eg : Rizky Ramadhan" value="{{ $data->name }}" />
                                        <div class="text-muted fs-7">A Employee name is required.</div>
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label class="required form-label">Date Birth</label>
                                        <input type="text" id="date_birth" name="date_birth" class="form-control mb-2" placeholder="Eg : {{date('Y-m-d')}}" value="{{ $data->date_birth }}" />
                                        <div class="text-muted fs-7">A Employee date birth is required.</div>
                                    </div>
                                    <div>
                                        <label class="form-label required">Job Description</label>
                                        <div id="jobdesc" class="min-h-200px mb-2">{!!$data->jobdesc!!}</div>
                                        <textarea class="form-control d-none" name="jobdesc">{!!$data->jobdesc!!}</textarea>
                                        <div class="text-muted fs-7">Set a description to the product for better visibility.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Contact Info</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="mb-10 fv-row">
                                        <label class="form-label required">Phone Number</label>
                                        <input type="text" name="phone" class="form-control mb-2" placeholder="Eg : 087709020299" value="{{ $data->phone }}" />
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label class="form-label required">Email</label>
                                        <input type="text" name="email" class="form-control mb-2" placeholder="Eg : misterrizky@yadaekidanta.com" value="{{ $data->email }}" />
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label class="form-label required">Password</label>
                                        <input type="password" name="password" class="form-control mb-2" />
                                    </div>
                                </div>
                            </div>
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Address Info</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="mb-10 fv-row">
                                        <label class="form-label required">Address</label>
                                        <textarea class="form-control" name="address">{{$data->address}}</textarea>
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label class="form-label required">Province</label>
                                        <select class="form-control" name="province" id="province">
                                            <option value="">Choose Province</option>
                                            @foreach($province as $p)
                                                <option value="{{$p->id}}">{{$p->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label class="form-label required">City</label>
                                        <select class="form-control" name="city" id="city">
                                            <option value="">Choose Province First</option>
                                        </select>
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label class="form-label required">Subdistrict</label>
                                        <select class="form-control" name="subdistrict" id="subdistrict">
                                            <option value="">Choose City First</option>
                                        </select>
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label class="form-label required">Postcode</label>
                                        <input type="text" class="form-control" name="postcode" id="postcode" value="{{$data->postcode}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($data->id)
                    <div class="tab-pane fade" id="tab_activities" role="tab-panel">
                        <div class="d-flex flex-column gap-7 gap-lg-10">
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>{{$data->name}} Activities</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <!--begin::Timeline-->
                                    <div class="timeline-label">
                                        <!--begin::Item-->
                                        <div class="timeline-item">
                                            <!--begin::Label-->
                                            <div class="timeline-label fw-bolder text-gray-800 fs-6">08:42</div>
                                            <!--end::Label-->
                                            <!--begin::Badge-->
                                            <div class="timeline-badge">
                                                <i class="fa fa-genderless text-warning fs-1"></i>
                                            </div>
                                            <!--end::Badge-->
                                            <!--begin::Text-->
                                            <div class="fw-mormal timeline-content text-muted ps-3">Outlines keep you honest. And keep structure</div>
                                            <!--end::Text-->
                                        </div>
                                        <!--end::Item-->
                                        <!--begin::Item-->
                                        <div class="timeline-item">
                                            <!--begin::Label-->
                                            <div class="timeline-label fw-bolder text-gray-800 fs-6">10:00</div>
                                            <!--end::Label-->
                                            <!--begin::Badge-->
                                            <div class="timeline-badge">
                                                <i class="fa fa-genderless text-success fs-1"></i>
                                            </div>
                                            <!--end::Badge-->
                                            <!--begin::Content-->
                                            <div class="timeline-content d-flex">
                                                <span class="fw-bolder text-gray-800 ps-3">AEOL meeting</span>
                                            </div>
                                            <!--end::Content-->
                                        </div>
                                        <!--end::Item-->
                                        <!--begin::Item-->
                                        <div class="timeline-item">
                                            <!--begin::Label-->
                                            <div class="timeline-label fw-bolder text-gray-800 fs-6">14:37</div>
                                            <!--end::Label-->
                                            <!--begin::Badge-->
                                            <div class="timeline-badge">
                                                <i class="fa fa-genderless text-danger fs-1"></i>
                                            </div>
                                            <!--end::Badge-->
                                            <!--begin::Desc-->
                                            <div class="timeline-content fw-bolder text-gray-800 ps-3">Make deposit
                                            <a href="#" class="text-primary">USD 700</a>. to ESL</div>
                                            <!--end::Desc-->
                                        </div>
                                        <!--end::Item-->
                                        <!--begin::Item-->
                                        <div class="timeline-item">
                                            <!--begin::Label-->
                                            <div class="timeline-label fw-bolder text-gray-800 fs-6">16:50</div>
                                            <!--end::Label-->
                                            <!--begin::Badge-->
                                            <div class="timeline-badge">
                                                <i class="fa fa-genderless text-primary fs-1"></i>
                                            </div>
                                            <!--end::Badge-->
                                            <!--begin::Text-->
                                            <div class="timeline-content fw-mormal text-muted ps-3">Indulging in poorly driving and keep structure keep great</div>
                                            <!--end::Text-->
                                        </div>
                                        <!--end::Item-->
                                        <!--begin::Item-->
                                        <div class="timeline-item">
                                            <!--begin::Label-->
                                            <div class="timeline-label fw-bolder text-gray-800 fs-6">21:03</div>
                                            <!--end::Label-->
                                            <!--begin::Badge-->
                                            <div class="timeline-badge">
                                                <i class="fa fa-genderless text-danger fs-1"></i>
                                            </div>
                                            <!--end::Badge-->
                                            <!--begin::Desc-->
                                            <div class="timeline-content fw-bold text-gray-800 ps-3">New order placed
                                            <a href="#" class="text-primary">#XF-2356</a>.</div>
                                            <!--end::Desc-->
                                        </div>
                                        <!--end::Item-->
                                        <!--begin::Item-->
                                        <div class="timeline-item">
                                            <!--begin::Label-->
                                            <div class="timeline-label fw-bolder text-gray-800 fs-6">16:50</div>
                                            <!--end::Label-->
                                            <!--begin::Badge-->
                                            <div class="timeline-badge">
                                                <i class="fa fa-genderless text-primary fs-1"></i>
                                            </div>
                                            <!--end::Badge-->
                                            <!--begin::Text-->
                                            <div class="timeline-content fw-mormal text-muted ps-3">Indulging in poorly driving and keep structure keep great</div>
                                            <!--end::Text-->
                                        </div>
                                        <!--end::Item-->
                                        <!--begin::Item-->
                                        <div class="timeline-item">
                                            <!--begin::Label-->
                                            <div class="timeline-label fw-bolder text-gray-800 fs-6">21:03</div>
                                            <!--end::Label-->
                                            <!--begin::Badge-->
                                            <div class="timeline-badge">
                                                <i class="fa fa-genderless text-danger fs-1"></i>
                                            </div>
                                            <!--end::Badge-->
                                            <!--begin::Desc-->
                                            <div class="timeline-content fw-bold text-gray-800 ps-3">New order placed
                                            <a href="#" class="text-primary">#XF-2356</a>.</div>
                                            <!--end::Desc-->
                                        </div>
                                        <!--end::Item-->
                                        <!--begin::Item-->
                                        <div class="timeline-item mb-9">
                                            <!--begin::Label-->
                                            <div class="timeline-label fw-bolder text-gray-800 fs-6">10:30</div>
                                            <!--end::Label-->
                                            <!--begin::Badge-->
                                            <div class="timeline-badge">
                                                <i class="fa fa-genderless text-success fs-1"></i>
                                            </div>
                                            <!--end::Badge-->
                                            <!--begin::Text-->
                                            <div class="timeline-content fw-mormal text-muted ps-3">Finance KPI Mobile app launch preparion meeting</div>
                                            <!--end::Text-->
                                        </div>
                                        <!--end::Item-->
                                    </div>
                                    <!--end::Timeline-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab_attendances" role="tab-panel">
                        @php
                        $absen = DB::select(DB::raw(" 
                            SELECT
                                DATE_FORMAT(cal.my_date, '%e %b %Y') as tanggal,
                                SUM(case when t.employee_id = $data->id then 1 else 0 end) as total
                            FROM
                                (
                                    SELECT
                                        s.start_date + INTERVAL ( days.d ) DAY AS my_date 
                                    FROM
                                        ( SELECT LAST_DAY( CURRENT_DATE ) + INTERVAL 1 DAY - INTERVAL $total_month MONTH AS start_date, LAST_DAY( CURRENT_DATE ) AS end_date
                                ) AS s
                            LEFT JOIN days ON days.d <= DATEDIFF( s.end_date, s.start_date ) ) AS cal
                            LEFT JOIN employee_attendances AS t ON DATE_FORMAT(presence_at, '%Y-%m-%d') >= cal.my_date AND DATE_FORMAT(presence_at, '%Y-%m-%d') < cal.my_date + INTERVAL 1 DAY 
                            WHERE
                                MONTH ( my_date ) = '$month' AND YEAR(my_date) = '$year'
                            GROUP BY
                                tanggal
                            ORDER BY
                                LENGTH(tanggal), tanggal
                        "));
                        $total_absen = 0;
                        @endphp
                        <div class="d-flex flex-column gap-7 gap-lg-10">
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>{{$data->name}} Attendances on This Month</h2>
                                    </div>
                                    <div class="card-toolbar">
                                        <span class="fw-bolder me-5">Total Attendances:
                                            @foreach ($absen as $item)
                                                @php
                                                $total_absen += $item->total;
                                                @endphp
                                            @endforeach
                                            {{$total_absen}}
                                        </span>
                                        
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="table-responsive">
                                        <table class="table align-middle table-row-dashed fs-6 gy-5">
                                            <thead>
                                                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                                                    @foreach($days as $d)
                                                    <th class="min-w-10px">
                                                        {{ Str::before($d->tanggal,' ') }}
                                                    </th>
                                                    @endforeach
                                                </tr>
                                            </thead>
                                            <tbody class="fw-bold text-gray-600">
                                                <tr>
                                                @foreach($absen as $a)
                                                <td>
                                                    <span class="badge badge-circle badge-{{$a->total == 1 ? 'success' : 'danger'}}"></span>
                                                </td>
                                                @endforeach
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab_certificate" role="tab-panel">
                        <div class="d-flex flex-column gap-7 gap-lg-10">
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>{{$data->name}} Certificates</h2>
                                    </div>
                                    <div class="card-toolbar">
                                        <span class="fw-bolder me-5">Total Certificate:</span>
                                        
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <table class="table table-row-dashed fs-6 gy-5 my-0" id="table_rewards">
                                        <thead>
                                            <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                                                <th class="w-10px pe-2">
                                                    <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                                        <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#table_rewards .form-check-input" value="1" />
                                                    </div>
                                                </th>
                                                <th class="min-w-125px">Rating</th>
                                                <th class="min-w-175px">Customer</th>
                                                <th class="min-w-175px">Comment</th>
                                                <th class="min-w-100px text-end fs-7">Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="form-check form-check-sm form-check-custom form-check-solid mt-1">
                                                        <input class="form-check-input" type="checkbox" value="1" />
                                                    </div>
                                                </td>
                                                <td data-order="rating-5">
                                                    <div class="rating">
                                                        <div class="rating-label checked">
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                                </svg>
                                                            </span>
                                                        </div>
                                                        <div class="rating-label checked">
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                                </svg>
                                                            </span>
                                                        </div>
                                                        <div class="rating-label checked">
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                                </svg>
                                                            </span>
                                                        </div>
                                                        <div class="rating-label checked">
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                                </svg>
                                                            </span>
                                                        </div>
                                                        <div class="rating-label checked">
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                                </svg>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <a href="../../demo1/dist/apps/inbox/reply.html" class="d-flex text-dark text-gray-800 text-hover-primary">
                                                        <div class="symbol symbol-circle symbol-25px me-3">
                                                            <div class="symbol-label bg-light-danger">
                                                                <span class="text-danger">M</span>
                                                            </div>
                                                        </div>
                                                        <span class="fw-bolder">Melody Macy</span>
                                                    </a>
                                                </td>
                                                <td class="text-gray-600 fw-bolder">I like this design</td>
                                                <td class="text-end">
                                                    <span class="fw-bold text-muted">Today</span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab_rewards" role="tab-panel">
                        <div class="d-flex flex-column gap-7 gap-lg-10">
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>{{$data->name}} Rewards</h2>
                                    </div>
                                    <div class="card-toolbar">
                                        <span class="fw-bolder me-5">Total Rewards:</span>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <table class="table table-row-dashed fs-6 gy-5 my-0" id="table_rewards">
                                        <thead>
                                            <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                                                <th class="w-10px pe-2">
                                                    <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                                        <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#table_rewards .form-check-input" value="1" />
                                                    </div>
                                                </th>
                                                <th class="min-w-125px">Rating</th>
                                                <th class="min-w-175px">Customer</th>
                                                <th class="min-w-175px">Comment</th>
                                                <th class="min-w-100px text-end fs-7">Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="form-check form-check-sm form-check-custom form-check-solid mt-1">
                                                        <input class="form-check-input" type="checkbox" value="1" />
                                                    </div>
                                                </td>
                                                <td data-order="rating-5">
                                                    <div class="rating">
                                                        <div class="rating-label checked">
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                                </svg>
                                                            </span>
                                                        </div>
                                                        <div class="rating-label checked">
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                                </svg>
                                                            </span>
                                                        </div>
                                                        <div class="rating-label checked">
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                                </svg>
                                                            </span>
                                                        </div>
                                                        <div class="rating-label checked">
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                                </svg>
                                                            </span>
                                                        </div>
                                                        <div class="rating-label checked">
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                                </svg>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <a href="../../demo1/dist/apps/inbox/reply.html" class="d-flex text-dark text-gray-800 text-hover-primary">
                                                        <div class="symbol symbol-circle symbol-25px me-3">
                                                            <div class="symbol-label bg-light-danger">
                                                                <span class="text-danger">M</span>
                                                            </div>
                                                        </div>
                                                        <span class="fw-bolder">Melody Macy</span>
                                                    </a>
                                                </td>
                                                <td class="text-gray-600 fw-bolder">I like this design</td>
                                                <td class="text-end">
                                                    <span class="fw-bold text-muted">Today</span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="d-flex justify-content-end">
                    <a href="javascript:;" onclick="main_content('content_list');" class="btn btn-light me-5">Cancel</a>
                    <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{$data->id ? route('office.employee.update',$data->id) : route('office.employee.store')}}','{{$data->id ? 'PATCH' : 'POST'}}');" class="btn btn-primary">
                        <span class="indicator-label">Save Changes</span>
                        <span class="indicator-progress">Please wait...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    // obj_dropzone_files("cv","","1");
    obj_enddatenow('date_birth');
    obj_quill('jobdesc');
    $("#status").on('change', function() {
        if(this.value == "Active"){
            $("#employee_status").removeClass('bg-danger');
            $("#employee_status").addClass('bg-success');
        }else{
            $("#employee_status").removeClass('bg-success');
            $("#employee_status").addClass('bg-danger');
        }
    });
    @if($data->department_id)
    $('#department').val('{{$data->department_id}}');
    setTimeout(function(){ 
        $('#department').trigger('change');
        setTimeout(function(){ 
            $('#position').val('{{$data->position_id}}');
            $('#position').trigger('change');
        }, 1200);
    }, 500);
    @endif
    @if($data->province_id)
    $('#province').val('{{$data->province_id}}');
    setTimeout(function(){ 
        $('#province').trigger('change');
        setTimeout(function(){ 
            $('#city').val('{{$data->city_id}}');
            $('#city').trigger('change');
            setTimeout(function(){ 
                $('#subdistrict').val('{{$data->subdistrict_id}}');
                $('#subdistrict').trigger('change');
            }, 1200);
            setTimeout(function(){ 
                $('#postcode').val('{{$data->postcode}}');
            }, 1200);
        }, 1200);
    }, 500);
    @endif
    $("#department").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('office.department.get_position')}}",
            data: {department : $("#department").val()},
            success: function(response){
                $("#position").html(response);
            }
        });
    });
    $("#province").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('office.province.get_city')}}",
            data: {province : $("#province").val()},
            success: function(response){
                $("#city").html(response);
            }
        });
    });
    $("#city").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('office.city.get_subdistrict')}}",
            data: {city : $("#city").val()},
            success: function(response){
                $("#subdistrict").html(response);
            }
        });
    });
    $("#subdistrict").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('office.subdistrict.get_postcode')}}",
            data: {subdistrict : $("#subdistrict").val()},
            success: function(response){
                $("#postcode").html(response);
            }
        });
    });
</script>