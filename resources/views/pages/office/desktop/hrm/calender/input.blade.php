 <!--begin::Modal dialog-->
 <div class="modal-dialog modal-dialog-centered mw-650px">
     <!--begin::Modal content-->
     <div class="modal-content">
         <!--begin::Form-->
         <form class="form" id="kt_modal_add_event_form">
             <!--begin::Modal header-->
             <div class="modal-header">
                 <!--begin::Modal title-->
                 <h2 class="fw-bolder" data-kt-calendar="title">Add Event</h2>
                 <!--end::Modal title-->
                 <!--begin::Close-->
                 <div class="btn btn-icon btn-sm btn-color-gray-500 btn-active-icon-primary" data-bs-toggle="tooltip"
                     title="Close Modal Event" data-bs-dismiss="modal">
                     <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                     <span class="svg-icon svg-icon-1">
                         <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                             fill="none">
                             <rect opacity="0.5" x="6" y="17.3137" width="16" height="2"
                                 rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                             <rect x="7.41422" y="6" width="16" height="2" rx="1"
                                 transform="rotate(45 7.41422 6)" fill="currentColor" />
                         </svg>
                     </span>
                     <!--end::Svg Icon-->
                 </div>
                 <!--end::Close-->
             </div>
             <!--end::Modal header-->
             <!--begin::Modal body-->
             <div class="modal-body py-10 px-lg-17">
                 <!--begin::Input group-->
                 <div class="fv-row mb-9">
                     <!--begin::Label-->
                     <label class="fs-6 fw-bold required mb-2">Employee</label>
                     <!--end::Label-->
                     <select class="form-select mb-2" id="employee_id" name="employee_id">
                         <option value="">Pilih Employee</option>
                         @foreach ($employees as $value)
                             <option value="{{ $value->id }}"
                                 {{ $data->employee_id == $value->id ? 'selected' : '' }}>{{ $value->name }}
                             </option>
                         @endforeach
                     </select>
                 </div>
                 <!--end::Input group-->
                 <div class="fv-row mb-9">
                     <!--begin::Label-->
                     <label class="fs-6 fw-bold mb-2 required">Start Date</label>
                     <!--end::Label-->
                     <!--begin::Input-->
                     <input class="form-control form-control-solid" name="start" id="date"
                         value="{{ $data->date }}" />
                     <!--end::Input-->
                 </div>
                 <!--end::Input group-->
                 <div class="fv-row mb-9">
                     <!--begin::Label-->
                     <label class="fs-6 fw-bold mb-2 required">Amount</label>
                     <!--end::Label-->
                     <!--begin::Input-->
                     <input class="form-control form-control-solid" name="amount" id="amount"
                         value="{{ $data->amount }}" />
                     <!--end::Input-->
                 </div>
             </div>
             <!--end::Modal body-->
             <!--begin::Modal footer-->
             <div class="modal-footer flex-center">
                 <!--begin::Button-->
                 <button type="reset" id="kt_modal_add_event_cancel" class="btn btn-light me-3">Cancel</button>
                 <!--end::Button-->
                 <!--begin::Button-->
                 <button type="button" id="kt_modal_add_event_submit" class="btn btn-primary"
                     onclick="save('#tombol_simpan','#kt_modal_add_event_form','{{ $data->id ? route('office.calender.update', $data->id) : route('office.calender.store', $data->id) }}','{{ $data->id ? 'PATCH' : 'POST' }}');">
                     <span class="indicator-label">Save Changes</span>
                     <span class="indicator-progress">Please wait...
                         <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                 </button>
                 <!--end::Button-->
             </div>
             <!--end::Modal footer-->
         </form>
         <!--end::Form-->
     </div>
 </div>
 <script>
     $('#date').flatpickr({});
     $('#employee_id').select2({
         width: '100%',
         placeholder: 'Pilih Employee',
         allowClear: true,
     });

     function save(tombol, form, url, method) {
         $(tombol).attr('disabled', true);
         $(tombol).html('<span class="spinner-border spinner-border-sm"></span> Please wait...');
         $.ajax({
             url: url,
             type: method,
             data: $(form).serialize(),
             dataType: 'json',
             beforeSend: function() {},
             success: function(response) {
                 if (response.alert == 'success') {
                     toastr.success(response.message);
                     $('#kt_modal_add_event_close').click();
                     $('#kt_modal_add_event_cancel').click();
                     $(tombol).attr('disabled', false);
                     $(tombol).html('Save Changes');
                     $('#kt_modal_add_event_form')[0].reset();
                     $('#kt_modal_add_event').modal('hide');
                     load_calender();
                 } else {
                     toastr.error(response.message);
                     $(tombol).attr('disabled', false);
                     $(tombol).html('Save Changes');
                 }
             }
         });
     }

     $('#kt_modal_add_event_cancel').click(function() {
         $('#kt_modal_add_event_form')[0].reset();
     });
 </script>
