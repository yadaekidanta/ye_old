{{-- @if ($collection->count() > 0) --}}
<table class="table align-middle table-row-dashed fs-6 gy-5">
    <thead>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            @if(Auth::guard('office')->user()->department_id != 0)
            <th class="w-10px pe-2">
                <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                    <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target=".collection-check" />
                </div>
            </th>
            @endif
            <th class="min-w-250px">Department & Position</th>
            <th class="min-w-250px">Title</th>
            <th class="min-w-150px">Requirements</th>
            <th class="min-w-150px">Facilities</th>
            <th class="min-w-150px">Rates</th>
            <th class="min-w-150px">Status</th>
            <th class="text-end min-w-70px">Actions</th>
        </tr>
    </thead>
    <tbody class="fw-bold text-gray-600">
        @foreach ($collection as $key => $item)
        @php
        $name = Str::of($item->title)->explode(' ');
        $singkatan ='';
        foreach($name as $kata)
        {
        $singkatan .= substr($kata, 0, 1);
        }
        if($item->st == "Open"){
            $color = "success";
        }else{
            $color = "danger";
        }
        @endphp
        <tr>
            @if(Auth::guard('office')->user()->department_id != 0)
            <td>
                <div class="form-check form-check-sm form-check-custom form-check-solid">
                    <input class="form-check-input collection-check" name="list_id" type="checkbox" value="{{$item->id}}" />
                </div>
            </td>
            @endif
            <td>
                <div class="d-flex">
                    <a href="javascript:;" class="symbol symbol-50px">
                        <div class="symbol-label fs-2 fw-bold text-success">{{$singkatan}}</div>
                    </a>
                    <div class="ms-5">
                        <a href="javascript:;" class="text-gray-800 text-hover-primary fs-5 fw-bolder mb-1">{{$item->position_id ? $item->position->name : ''}}</a>
                        <div class="text-muted fs-7 fw-bolder">{{$item->department_id ? $item->department->name : ''}}</div>
                    </div>
                </div>
            </td>
            <td>
                <div class="d-flex">
                    <div class="ms-5">
                        <a href="javascript:;" class="text-gray-800 text-hover-primary fs-5 fw-bolder mb-1">{{$item->title}}</a>
                        <div class="text-muted fs-7 fw-bolder">{!!$item->description!!}</div>
                    </div>
                </div>
            </td>
            <td>
                <div class="d-flex">
                    <div class="ms-5">
                        <div class="text-muted fs-7 fw-bolder">{!!$item->requirement!!}</div>
                    </div>
                </div>
            </td>
            <td>
                <div class="d-flex">
                    <div class="ms-5">
                        <div class="text-muted fs-7 fw-bolder">{!!$item->facilities!!}</div>
                    </div>
                </div>
            </td>
            <td>
                <div class="d-flex">
                    <div class="ms-5">
                        <div class="text-muted fs-7 fw-bolder">{{number_format($item->rates)}}</div>
                    </div>
                </div>
            </td>
            <td>
                <div class="d-flex">
                    <div class="ms-5">
                        <div class="badge badge-light-{{$color}}">{{$item->st}}</div>
                    </div>
                </div>
            </td>
            <td class="text-end d-flex">
                @if(Auth::guard('office')->user()->department_id == 0)
                    @if($item->st == "Open")
                    <a href="javascript:;" onclick="load_input('{{route('office.my-applicants.edit',$item->id)}}');" class="btn btn-icon btn-bg-light btn-active-color-success btn-sm">
                        <span class="svg-icon svg-icon-3">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.3" d="M10 18C9.7 18 9.5 17.9 9.3 17.7L2.3 10.7C1.9 10.3 1.9 9.7 2.3 9.3C2.7 8.9 3.29999 8.9 3.69999 9.3L10.7 16.3C11.1 16.7 11.1 17.3 10.7 17.7C10.5 17.9 10.3 18 10 18Z" fill="currentColor"/>
                                <path d="M10 18C9.7 18 9.5 17.9 9.3 17.7C8.9 17.3 8.9 16.7 9.3 16.3L20.3 5.3C20.7 4.9 21.3 4.9 21.7 5.3C22.1 5.7 22.1 6.30002 21.7 6.70002L10.7 17.7C10.5 17.9 10.3 18 10 18Z" fill="currentColor"/>
                            </svg>
                        </span>
                    </a>
                    @endif
                @else
                    <a href="{{route('office.vacancy.show',$item->id)}}" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1">
                        <span class="svg-icon svg-icon-3">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14V4H6V20H18V8H20V21C20 21.6 19.6 22 19 22Z" fill="currentColor"/>
                                <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="currentColor"/>
                            </svg>
                        </span>
                    </a>
                    <a href="javascript:;" onclick="load_input('{{route('office.vacancy.edit',$item->id)}}');" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1">
                        <span class="svg-icon svg-icon-3">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" fill="currentColor"></path>
                                <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" fill="currentColor"></path>
                            </svg>
                        </span>
                    </a>
                
                    @if($item->st == "Closed")
                    <a href="javascript:;" onclick="handle_confirm('Are you sure want to open this job ?','{{__('custom.yes')}}','{{__('custom.no')}}','PATCH','{{route('office.vacancy.open',$item->id)}}');" class="btn btn-icon btn-bg-light btn-active-color-success btn-sm">
                        <span class="svg-icon svg-icon-3">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.3" d="M10 18C9.7 18 9.5 17.9 9.3 17.7L2.3 10.7C1.9 10.3 1.9 9.7 2.3 9.3C2.7 8.9 3.29999 8.9 3.69999 9.3L10.7 16.3C11.1 16.7 11.1 17.3 10.7 17.7C10.5 17.9 10.3 18 10 18Z" fill="currentColor"/>
                                <path d="M10 18C9.7 18 9.5 17.9 9.3 17.7C8.9 17.3 8.9 16.7 9.3 16.3L20.3 5.3C20.7 4.9 21.3 4.9 21.7 5.3C22.1 5.7 22.1 6.30002 21.7 6.70002L10.7 17.7C10.5 17.9 10.3 18 10 18Z" fill="currentColor"/>
                            </svg>
                        </span>
                    </a>
                    @else
                    <a href="javascript:;" onclick="handle_confirm('Are you sure want to close this job ?','{{__('custom.yes')}}','{{__('custom.no')}}','PATCH','{{route('office.vacancy.close',$item->id)}}');" class="btn btn-icon btn-bg-light btn-active-color-danger btn-sm">
                        <span class="svg-icon svg-icon-3">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.3" d="M6 19.7C5.7 19.7 5.5 19.6 5.3 19.4C4.9 19 4.9 18.4 5.3 18L18 5.3C18.4 4.9 19 4.9 19.4 5.3C19.8 5.7 19.8 6.29999 19.4 6.69999L6.7 19.4C6.5 19.6 6.3 19.7 6 19.7Z" fill="currentColor"/>
                                <path d="M18.8 19.7C18.5 19.7 18.3 19.6 18.1 19.4L5.40001 6.69999C5.00001 6.29999 5.00001 5.7 5.40001 5.3C5.80001 4.9 6.40001 4.9 6.80001 5.3L19.5 18C19.9 18.4 19.9 19 19.5 19.4C19.3 19.6 19 19.7 18.8 19.7Z" fill="currentColor"/>
                            </svg>
                        </span>
                    </a>
                    @endif
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('themes.office.desktop.pagination')}}
{{-- @else
<div class="text-center px-4 mb-3">
    <img class="mw-100 mh-300px" alt="" src="{{asset('keenthemes/media/illustrations/sigma-1/18.png')}}">
</div>
@endif --}}