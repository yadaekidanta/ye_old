<div class="timeline row d-flex">
    <div class="col-md-6">
        @foreach ($collection as $item)
            <!--begin::Timeline item-->
            <div class="timeline-item w-300px">
                <!--begin::Timeline line-->
                <div class="timeline-line w-40px"></div>
                <!--end::Timeline line-->
                <!--begin::Timeline icon-->
                <div class="timeline-icon symbol symbol-circle symbol-40px me-4">
                    <div class="symbol-label bg-light">
                        <!--begin::Svg Icon | path: icons/duotune/communication/com003.svg-->
                        <span class="svg-icon svg-icon-2 svg-icon-gray-500">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <path opacity="0.3"
                                    d="M2 4V16C2 16.6 2.4 17 3 17H13L16.6 20.6C17.1 21.1 18 20.8 18 20V17H21C21.6 17 22 16.6 22 16V4C22 3.4 21.6 3 21 3H3C2.4 3 2 3.4 2 4Z"
                                    fill="black" />
                                <path
                                    d="M18 9H6C5.4 9 5 8.6 5 8C5 7.4 5.4 7 6 7H18C18.6 7 19 7.4 19 8C19 8.6 18.6 9 18 9ZM16 12C16 11.4 15.6 11 15 11H6C5.4 11 5 11.4 5 12C5 12.6 5.4 13 6 13H15C15.6 13 16 12.6 16 12Z"
                                    fill="black" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </div>
                </div>
                <!--end::Timeline icon-->

                <!--begin::Timeline content-->
                <div class="timeline-content mb-10 mt-n1">

                    <!--begin::Timeline heading-->
                    <div class="pe-3 mb-5">
                        <!--begin::Title-->
                        <div class="fs-5 fw-bold mb-2">{{ $item->title }}</div>
                        <!--end::Title-->
                        <!--begin::Description-->
                        <div class="d-flex align-items-center mt-1 fs-6">
                            <!--begin::Info-->
                            <div class="text-muted me-2 fs-7">Added at {{ $item->created_at->format('d M Y') }}</div>
                            <!--end::Info-->
                            <!--begin::User-->
                            {{-- <div class="symbol symbol-circle symbol-25px" data-bs-toggle="tooltip"
                                data-bs-boundary="window" data-bs-placement="top" title="Nina Nilson">
                                <img src="assets/media/avatars/300-14.jpg" alt="img" />
                            </div> --}}
                            <!--end::User-->
                        </div>
                        <!--end::Description-->
                    </div>

                    <!--end::Timeline heading-->

                    <!--end::Timeline content-->
                </div>
            </div>
            <!--end::Timeline item-->
        @endforeach
    </div>
    <div id="content_detail" class="col-md-6 d-none">
        <!--begin::Timeline details-->
        <textarea name="body-{{ $item->id }}" id="body-{{ $item->id }}" class="form-control" rows="3"
            placeholder="Enter memo here...">{{ $item->body }}</textarea>
        <!--end::Timeline details-->
    </div>

</div>
