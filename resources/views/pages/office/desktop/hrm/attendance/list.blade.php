@php
$explode_month = Str::beforeLast($keyword,'-');
$month = Str::after($explode_month,'-');
$year = Str::before($keyword,'-');
$date1 = date($year.'-'.$month.'-'.'01');
$date2 = date("Y-m-d");

$diff = abs(strtotime($date2) - strtotime($date1));

$years = floor($diff/(365*60*60*24));
$months = floor(($diff-$years*365*60*60*24)/(30*60*60*24));
$total_month = number_format($months,0)+1;
@endphp
{{-- @if ($collection->count() > 0) --}}
<table class="table align-middle table-row-dashed fs-6 gy-5">
    <thead>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-250px">Name</th>
            @foreach($days as $d)
            <th class="min-w-10px">
                {{ Str::before($d->tanggal,' ') }}
            </th>
            @endforeach
        </tr>
    </thead>
    <tbody class="fw-bold text-gray-600">
        @foreach ($collection as $key => $item)
        <tr>
            <td>
                <div class="d-flex">
                    <a href="javascript:;" class="symbol symbol-50px">
                        <span class="symbol-label" style="background-image:url({{$item->image}});"></span>
                    </a>
                    <div class="ms-5">
                        <a href="javascript:;" class="text-gray-800 text-hover-primary fs-5 fw-bolder mb-1">{{$item->name}}</a>
                        <div class="text-muted fs-7 fw-bolder">{{$item->department_id ? $item->department->name : ''}} {{$item->position_id ? '- ' .$item->position->name : ''}}</div>
                    </div>
                </div>
            </td>
            @php
            $absen = DB::select(DB::raw(" 
                SELECT
                    DATE_FORMAT(cal.my_date, '%e %b %Y') as tanggal,
                    SUM(case when t.employee_id = $item->id then 1 else 0 end) as total
                FROM
                    (
                        SELECT
                            s.start_date + INTERVAL ( days.d ) DAY AS my_date 
                        FROM
                            ( SELECT LAST_DAY( CURRENT_DATE ) + INTERVAL 1 DAY - INTERVAL $total_month MONTH AS start_date, LAST_DAY( CURRENT_DATE ) AS end_date
                    ) AS s
                LEFT JOIN days ON days.d <= DATEDIFF( s.end_date, s.start_date ) ) AS cal
                LEFT JOIN employee_attendances AS t ON DATE_FORMAT(presence_at, '%Y-%m-%d') >= cal.my_date AND DATE_FORMAT(presence_at, '%Y-%m-%d') < cal.my_date + INTERVAL 1 DAY 
                WHERE
                    MONTH ( my_date ) = '$month' AND YEAR(my_date) = '$year'
                GROUP BY
                    tanggal
                ORDER BY
                    LENGTH(tanggal), tanggal
            "));
            @endphp
            @foreach($absen as $a)
            <td>
                <span class="badge badge-circle badge-{{$a->total == 1 ? 'success' : 'danger'}}"></span>
            </td>
            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>
{{-- @else
<div class="text-center px-4 mb-3">
    <img class="mw-100 mh-300px" alt="" src="{{asset('keenthemes/media/illustrations/sigma-1/18.png')}}">
</div>
@endif --}}