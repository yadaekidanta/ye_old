<div class="toolbar" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
        <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
            <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">HRM</h1>
            <span class="h-20px border-gray-300 border-start mx-4"></span>
            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                <li class="breadcrumb-item text-muted">{{__('menu.kpi key')}}</li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-300 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{$data->id ? 'Update '. $data->name : 'Add new'}}</li>
            </ul>
        </div>
        <div class="d-flex align-items-center gap-2 gap-lg-3">
            <a href="javascript:;" onclick="main_content('content_list');" class="btn btn-sm btn-primary">Back</a>
        </div>
    </div>
</div>
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <form id="form_input" class="form d-flex flex-column flex-lg-row">
            <div class="d-flex flex-column gap-7 gap-lg-10 w-100 w-lg-300px mb-7 me-lg-10">
                
                <div class="card card-flush py-4">
                    <div class="card-header">
                        <div class="card-title">
                            <h2>Objectives</h2>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <input type="text" name="kpi_id" class="form-control mb-2" value="{{ $kpiObjective->id }}" hidden>
                        <input type="text" class="form-control mb-2" value="{{ $kpiObjective->objective }}" readonly>
                    </div>
                    <div class="card-header">
                        <div class="card-title">
                            <h2>Department</h2>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <select class="form-select mb-2" id="department" name="department">
                            <option value="">Pilih Department</option>
                            @foreach ($department as $value)
                                <option value="{{$value->id}}" {{$data->status == $value->id ? 'selected' : ''}}>{{$value->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="card-header">
                        <div class="card-title">
                            <h2>Position</h2>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <select class="form-select mb-2" id="position" name="position">
                            <option value="">Pilih Position</option>
                            
                        </select>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="kt_ecommerce_add_product_general" role="tab-panel">
                        <div class="d-flex flex-column gap-7 gap-lg-10">
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Key Result</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div id="key_result" class="min-h-200px mb-2">{!!$kpiObjective->key_result!!}</div>
                                    <textarea class="form-control d-none" name="key_result" value="{{$kpiObjective->key_result}}"></textarea>
                                </div>
                            </div>
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Description</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div id="description" class="min-h-200px mb-2">{!!$kpiObjective->description!!}</div>
                                    <textarea class="form-control d-none" name="description" value="{{$kpiObjective->description}}"></textarea>
                                </div>
                            </div>
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Measure unit</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div id="measure_unit" class="min-h-200px mb-2">{!!$kpiObjective->measure_unit!!}</div>
                                    <textarea class="form-control d-none" name="measure_unit" value="{{$kpiObjective->measure_unit}}"></textarea>
                                </div>
                            </div>
                            <div class="card-header">
                                <div class="card-title">
                                    <h2>Weight</h2>
                                </div>
                            </div>
                            <div class="card-body pt-0">
                                <input type="text" class="form-control mb-2" name="weight" value="{{ $kpiObjective->objective }}" readonly>
                            </div>
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Review</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div id="review" class="min-h-200px mb-2">{!!$kpiObjective->review!!}</div>
                                    <textarea class="form-control d-none" name="review" value="{{$kpiObjective->review}}"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-end">
                    <a href="javascript:;" onclick="main_content('content_list');" class="btn btn-light me-5">Cancel</a>
                    <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{$data->id ? route('office.kpi-objective.update',[$kpi->id, $data->id]) : route('office.kpi-objective.store',$kpi->id)}}','{{$data->id ? 'PATCH' : 'POST'}}');" class="btn btn-primary">
                        <span class="indicator-label">Save Changes</span>
                        <span class="indicator-progress">Please wait...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    obj_quill('key_result');
    @if($data->department_id)
    $('#department').val('{{$data->department_id}}');
    setTimeout(function(){ 
        $('#department').trigger('change');
        setTimeout(function(){ 
            $('#position').val('{{$data->position_id}}');
            $('#position').trigger('change');
        }, 1200);
    }, 500);
    @endif
    
    $("#department").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('office.department.get_position')}}",
            data: {department : $("#department").val()},
            success: function(response){
                $("#position").html(response);
            }
        });
    });
    
</script>