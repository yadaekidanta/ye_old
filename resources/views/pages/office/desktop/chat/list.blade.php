@foreach ($collection as $item)
@php
$name = Str::of($item->name)->explode(' ');
$singkatan ='';
foreach($name as $kata)
{
$singkatan .= substr($kata, 0, 1);
}
$chat = App\Models\Communication\Chat::whereRaw('(from_id = '.$item->id.' AND to_id = '.Auth::guard('office')->user()->id.') OR (from_id = '.Auth::guard('office')->user()->id.' AND to_id = '.$item->id.')')->orderBy('created_at','DESC');
$cchat = App\Models\Communication\Chat::whereNull('read_at')->where('from_id', $item->id)->where('to_id', Auth::guard('office')->user()->id)->orderBy('created_at','DESC')->get();
@endphp
<div class="d-flex flex-stack py-4">
    <!--begin::Details-->
    <div class="d-flex align-items-center">
        <!--begin::Avatar-->
        <div class="symbol symbol-45px symbol-circle">
            <span class="symbol-label bg-light-danger text-danger fs-6 fw-bolder">{{$singkatan}}</span>
            <div class="symbol-badge bg-{{Cache::has('is_employee_online'. $item->id) ? 'success' : 'danger'}} start-100 top-100 border-4 h-15px w-15px ms-n2 mt-n2"></div>
        </div>
        <!--end::Avatar-->
        <!--begin::Details-->
        <div class="ms-5">
            <a href="{{route('office.chat.show',$item->id)}}" class="fs-5 fw-bolder text-gray-900 text-hover-primary mb-2">{{$item->name}}</a>
            <div class="fw-bold text-muted">{{$item->position->name}}</div>
        </div>
        <!--end::Details-->
    </div>
    <!--end::Details-->
    <!--begin::Lat seen-->
    <div class="d-flex flex-column align-items-end ms-2">
        <span class="text-muted fs-7 mb-1">{{$chat->first() ? $chat->first()->created_at->diffForHumans() : ''}}</span>
    </div>
    <!--end::Lat seen-->
</div>
@endforeach