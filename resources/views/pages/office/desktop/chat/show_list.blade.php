
<!--begin::Card header-->
<div class="card-header" id="kt_chat_messenger_header">
    <!--begin::Title-->
    <div class="card-title">
        <!--begin::User-->
        <div class="d-flex justify-content-center flex-column me-3">
            <a href="#" class="fs-4 fw-bolder text-gray-900 text-hover-primary me-1 mb-2 lh-1">{{$employee->name}}</a>
            <!--begin::Info-->
            <div class="mb-0 lh-1">
                <span class="badge badge-{{Cache::has('is_employee_online'. $employee->id) ? 'success' : 'danger'}} badge-circle w-10px h-10px me-1"></span>
                <span class="fs-7 fw-bold text-muted">{{Cache::has('is_employee_online'. $employee->id) ? 'Online' : 'Offline'}}</span>
            </div>
            <!--end::Info-->
        </div>
        <!--end::User-->
    </div>
    <!--end::Title-->
    <!--begin::Card toolbar-->
    <div class="card-toolbar">
        <!--begin::Menu-->
        <div class="me-n3">
            <button class="btn btn-sm btn-icon btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                <i class="bi bi-three-dots fs-2"></i>
            </button>
        </div>
        <!--end::Menu-->
    </div>
    <!--end::Card toolbar-->
</div>
<!--end::Card header-->
<!--begin::Card body-->
<div class="card-body" id="kt_chat_messenger_body">
    <!--begin::Messages-->
    <div class="scroll-y me-n5 pe-5 h-300px h-lg-auto" data-kt-element="messages" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_header, #kt_toolbar, #kt_footer, #kt_chat_messenger_header, #kt_chat_messenger_footer" data-kt-scroll-wrappers="#kt_content, #kt_chat_messenger_body" data-kt-scroll-offset="5px">
        @foreach ($collection as $item)
        <!--begin::Message(in)-->
        <div class="d-flex justify-content-{{$item->to_id == Auth::guard('office')->user()->id ? 'start' : 'end'}} mb-10">
            <!--begin::Wrapper-->
            <div class="d-flex flex-column align-items-{{$item->to_id == Auth::guard('office')->user()->id ? 'start' : 'end'}}">
                <!--begin::User-->
                <div class="d-flex align-items-center mb-2">
                    <!--begin::Avatar-->
                    @if($item->to_id == Auth::guard('office')->user()->id)
                    <div class="symbol symbol-35px symbol-circle">
                        <img alt="Pic" src="{{$item->from->image}}" />
                    </div>
                    <!--end::Avatar-->
                    <!--begin::Details-->
                    <div class="ms-3">
                        <a href="#" class="fs-5 fw-bolder text-gray-900 text-hover-primary me-1">{{$item->from->name}}</a>
                        <span class="text-muted fs-7 mb-1">{{$item->created_at->diffForHumans()}}</span>
                    </div>
                    @else
                    <!--begin::Details-->
                    <div class="ms-3">
                        <span class="text-muted fs-7 mb-1">{{$item->created_at->diffForHumans()}}</span>
                        <a href="#" class="fs-5 fw-bolder text-gray-900 text-hover-primary me-1">You</a>
                    </div>
                    <div class="symbol symbol-35px symbol-circle">
                        <img alt="Pic" src="{{$item->from->image}}" />
                    </div>
                    @endif
                    <!--end::Details-->
                </div>
                <!--end::User-->
                <!--begin::Text-->
                <div class="p-5 rounded bg-light-{{$item->to_id == Auth::guard('office')->user()->id ? 'info' : 'primary'}} text-dark fw-bold mw-lg-400px text-{{$item->to_id == Auth::guard('office')->user()->id ? 'start' : 'end'}}" data-kt-element="message-text">
                    {{$item->body}}
                </div>
                <!--end::Text-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Message(in)-->
        @endforeach
    </div>
    <!--end::Messages-->
</div>
<!--end::Card body-->