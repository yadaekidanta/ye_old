<div class="toolbar" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
        <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
            <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">Categories</h1>
            <span class="h-20px border-gray-300 border-start mx-4"></span>
            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                <li class="breadcrumb-item text-muted">
                    <a href="javascript:;" class="text-muted text-hover-primary">{{$data->name}}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-300 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">Product</li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-300 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{$data->id ? 'Update '. $data->name : 'Add new'}}</li>
            </ul>
        </div>
        <div class="d-flex align-items-center gap-2 gap-lg-3">
            <a href="javascript:;" onclick="load_list(1);" class="btn btn-sm btn-primary">Back</a>
        </div>
    </div>
</div>
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <form id="form_input" class="form d-flex flex-column flex-lg-row" data-kt-redirect="../../demo1/dist/apps/ecommerce/catalog/products.html">
            <div class="d-flex flex-column gap-7 gap-lg-10 w-100 w-lg-300px mb-7 me-lg-10">
                <div class="card card-flush py-4">
                    <div class="card-header">
                        <div class="card-title">
                            <h2>Thumbnail</h2>
                        </div>
                    </div>
                    <div class="card-body text-center pt-0">
                        <div class="image-input image-input-empty image-input-outline mb-3" data-kt-image-input="true" style="">
                            <div class="image-input-wrapper w-150px h-150px" style="background-image: url(assets/media//stock/ecommerce/78.gif)"></div>
                            <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                                <i class="bi bi-pencil-fill fs-7"></i>
                                <input type="file" name="avatar" accept=".png, .jpg, .jpeg" />
                                <input type="hidden" name="avatar_remove" />
                            </label>
                            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                                <i class="bi bi-x fs-2"></i>
                            </span>
                            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                                <i class="bi bi-x fs-2"></i>
                            </span>
                        </div>
                        <div class="text-muted fs-7">Set the product thumbnail image. Only *.png, *.jpg and *.jpeg image files are accepted</div>
                    </div>
                </div>
                <div class="card card-flush py-4">
                    <div class="card-header">
                        <div class="card-title">
                            <h2>Status</h2>
                        </div>
                        <div class="card-toolbar">
                            <div class="rounded-circle w-15px h-15px" id="product_status"></div>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <select class="form-select mb-2" id="status" name="status">
                            <option value="">Pilih Status</option>
                            <option value="Published">Published</option>
                            <option value="Draft">Draft</option>
                            <option value="Scheduled">Scheduled</option>
                            <option value="Inactive">Inactive</option>
                        </select>
                        <div class="text-muted fs-7">Set the product status.</div>
                        <div class="d-none mt-10" id="scheduled_product">
                            <label for="date_publish" class="form-label">Select publishing date and time</label>
                            <input class="form-control" id="date_publish" placeholder="Pick date &amp; time" />
                        </div>
                    </div>
                </div>
                <div class="card card-flush py-4">
                    <div class="card-header">
                        <div class="card-title">
                            <h2>Product Categories</h2>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <label class="form-label">Categories</label>
                        <input type="text" hidden name="product_category_id" value="{{ $productCategory->id }}">
                        <input type="text" readonly class="form-control" value="{{ $productCategory->name }}">
                        <div class="text-muted fs-7 mb-7">Add product to a category.</div>
                        <a href="javascript:;" onclick="handle_open_modal('{{route('office.product-category.create_modal')}}','#customModal','#contentModal');" class="btn btn-light-primary btn-sm mb-10">
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.5" x="11" y="18" width="12" height="2" rx="1" transform="rotate(-90 11 18)" fill="currentColor" />
                                    <rect x="6" y="11" width="12" height="2" rx="1" fill="currentColor" />
                                </svg>
                            </span>
                            Create new category
                        </a>
                        <label class="form-label d-block">Tags</label>
                        <input id="tags" name="tags" class="form-control mb-2" value="{{ $data->tags }}" />
                        <div class="text-muted fs-7">Add tags to a product.</div>
                    </div>
                </div>
                @if($data->id)
                <div class="card card-flush">
                    <div class="card-header pt-5">
                        <div class="card-title d-flex flex-column">
                            <div class="d-flex align-items-center">
                                <span class="fs-4 fw-bold text-gray-400 me-1 align-self-start">$</span>
                                <span class="fs-2hx fw-bolder text-dark me-2 lh-1 ls-n2">2,420</span>
                                <span class="badge badge-success fs-base">
                                    <span class="svg-icon svg-icon-5 svg-icon-white ms-n1">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="currentColor" />
                                            <path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="currentColor" />
                                        </svg>
                                    </span>
                                    2.6%
                                </span>
                            </div>
                            <span class="text-gray-400 pt-1 fw-bold fs-6">Average Daily Sales</span>
                        </div>
                    </div> 
                    <div class="card-body d-flex align-items-end px-0 pb-0">
                        <div id="kt_card_widget_6_chart" class="w-100" style="height: 80px"></div>
                    </div>
                </div>
                @endif
                <div class="card card-flush py-4 d-none">
                    <div class="card-header">
                        <div class="card-title">
                            <h2>Product Template</h2>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <label for="kt_ecommerce_add_product_store_template" class="form-label">Select a product template</label>
                        <select class="form-select mb-2" data-control="select2" data-hide-search="true" data-placeholder="Select an option" id="kt_ecommerce_add_product_store_template">
                            <option></option>
                            <option value="default" selected="selected">Default template</option>
                            <option value="electronics">Electronics</option>
                            <option value="office">Office stationary</option>
                            <option value="fashion">Fashion</option>
                        </select>
                        <div class="text-muted fs-7">Assign a template from your current theme to define how a single product is displayed.</div>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10">
                <ul class="nav nav-custom nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-bold mb-n2">
                    <li class="nav-item">
                        <a class="nav-link text-active-primary pb-4 active" data-bs-toggle="tab" href="#kt_ecommerce_add_product_general">General</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#kt_ecommerce_add_product_advanced">Advanced</a>
                    </li>
                    @if($data->id)
                    <li class="nav-item">
                        <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#kt_ecommerce_add_product_reviews">Reviews</a>
                    </li>
                    @endif
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="kt_ecommerce_add_product_general" role="tab-panel">
                        <div class="d-flex flex-column gap-7 gap-lg-10">
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>General</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="mb-10 fv-row">
                                        <label class="required form-label">Product Name</label>
                                        <input type="text" name="product_name" class="form-control mb-2" placeholder="Sample product" value="{{ $data->title }}" />
                                        <div class="text-muted fs-7">A product name is required and recommended to be unique.</div>
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label class="required form-label">URL</label>
                                        <input type="text" name="demo_url" class="form-control mb-2" placeholder="Demo URL" value="{{ $productCategory->demo_url }}" />
                                        <div class="text-muted fs-7">A product name is required and recommended to be unique.</div>
                                    </div>
                                    <div>
                                        <label class="form-label">Description</label>
                                        <div id="description" class="min-h-200px mb-2">{!!$data->description!!}</div>
                                        <textarea class="form-control d-none" name="description" value="{{$productCategory->description}}"></textarea>
                                        <div class="text-muted fs-7">Set a description to the product for better visibility.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Media</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="fv-row mb-2">
                                        <div class="dropzone" id="product_gallery">
                                            <div class="dz-message needsclick">
                                                <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                                                <div class="ms-4">
                                                    <h3 class="fs-5 fw-bolder text-gray-900 mb-1">Drop files here or click to upload.</h3>
                                                    <span class="fs-7 fw-bold text-gray-400">Upload up to 10 files</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-muted fs-7">Set the product media gallery.</div>
                                </div>
                            </div>
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Files</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="fv-row mb-2">
                                        <div class="dropzone" id="product_file">
                                            <div class="dz-message needsclick">
                                                <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                                                <div class="ms-4">
                                                    <h3 class="fs-5 fw-bolder text-gray-900 mb-1">Drop files here or click to upload.</h3>
                                                    <span class="fs-7 fw-bold text-gray-400">Upload only 1 files</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-muted fs-7">Set the product file.</div>
                                </div>
                            </div>
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Regular Pricing</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="mb-10 fv-row">
                                        <label class="required form-label">Base Price</label>
                                        <input type="text" id="price_reguler" name="price_reguler" class="form-control mb-2" placeholder="1000" value="{{ $productCategory->regular_price }}" />
                                        <div class="text-muted fs-7">Set the product price.</div>
                                    </div>
                                    <div class="fv-row mb-10">
                                        <label class="fs-6 fw-bold mb-2">Discount Type
                                        <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Select a discount type that will be applied to this product"></i></label>
                                        <div class="row row-cols-1 row-cols-md-3 row-cols-lg-1 row-cols-xl-3 g-9">
                                            <div class="col">
                                                <label class="btn btn-outline btn-outline-dashed btn-outline-default {{$data->tax_class == "1" ? 'active' : ''}} d-flex text-start p-6">
                                                    <span class="form-check form-check-custom form-check-solid form-check-sm align-items-start mt-1">
                                                        <input class="form-check-input" id="no-discount" type="radio" name="regular_discount" value="1" {{$data->tax_class == "1" ? 'checked' : ''}} />
                                                    </span>
                                                    <span class="ms-5">
                                                        <span class="fs-4 fw-bolder text-gray-800 d-block">No Discount</span>
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="col">
                                                <label class="btn btn-outline btn-outline-dashed btn-outline-default {{$data->tax_class == "2" ? 'active' : ''}} d-flex text-start p-6">
                                                    <span class="form-check form-check-custom form-check-solid form-check-sm align-items-start mt-1">
                                                        <input class="form-check-input" id="percentage-11" type="radio" name="regular_discount" value="2" {{$data->tax_class == "2" ? 'checked' : ''}} />
                                                    </span>
                                                    <span class="ms-5">
                                                        <span class="fs-4 fw-bolder text-gray-800 d-block">Percentage %</span>
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="fv-row mb-10" id="percentage-0">
                                        <div>
                                            <label class="required form-label">Discount Percentage %</label>
                                            <input type="tel" class="form-control" name="percentage-1" id="percentage-1">
                                        </div>
                                    </div>
                                    <div class="d-none mb-10 fv-row" id="discount_percent">
                                        <label class="form-label">Set Discount Percentage</label>
                                        <div class="d-flex flex-column text-center mb-5">
                                            <div class="d-flex align-items-start justify-content-center mb-7">
                                                <span class="fw-bolder fs-3x" id="kt_ecommerce_add_product_discount_label">0</span>
                                                <span class="fw-bolder fs-4 mt-1 ms-2">%</span>
                                            </div>
                                            <div id="kt_ecommerce_add_product_discount_slider" class="noUi-sm"></div>
                                        </div>
                                        <div class="text-muted fs-7">Set a percentage discount to be applied on this product.</div>
                                    </div>
                                    <div class="d-none mb-10 fv-row" id="discount_price">
                                        <label class="form-label">Fixed Discounted Price</label>
                                        <input type="text" name="dicsounted_price" class="form-control mb-2" placeholder="Discounted price" />
                                        <div class="text-muted fs-7">Set the discounted product price. The product will be reduced at the determined fixed price</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Extend Pricing</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="mb-10 fv-row">
                                        <label class="required form-label">Base Price</label>
                                        <input type="text" id="price_extend" name="price_extend" class="form-control mb-2" placeholder="1000" value="{{ $productCategory->extend_price }}" />
                                        <div class="text-muted fs-7">Set the product price.</div>
                                    </div>
                                    <div class="fv-row mb-10">
                                        <label class="fs-6 fw-bold mb-2">Discount Type
                                        <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Select a discount type that will be applied to this product"></i></label>
                                        <div class="row row-cols-1 row-cols-md-3 row-cols-lg-1 row-cols-xl-3 g-9">
                                            <div class="col">
                                                <label class="btn btn-outline btn-outline-dashed btn-outline-default {{$data->tax_class == "1" ? 'active' : ''}} d-flex text-start p-6">
                                                    <span class="form-check form-check-custom form-check-solid form-check-sm align-items-start mt-1">
                                                        <input class="form-check-input" id="no-discount2" type="radio" name="extend_discount" value="1" {{$data->tax_class == "1" ? 'checked' : ''}} />
                                                    </span>
                                                    <span class="ms-5">
                                                        <span class="fs-4 fw-bolder text-gray-800 d-block">No Discount</span>
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="col">
                                                <label class="btn btn-outline btn-outline-dashed btn-outline-default {{$data->tax_class == "2" ? 'active' : ''}} d-flex text-start p-6">
                                                    <span class="form-check form-check-custom form-check-solid form-check-sm align-items-start mt-1">
                                                        <input class="form-check-input" id="percentage-22" type="radio" name="extend_discount" value="2" {{$data->tax_class == "2" ? 'checked' : ''}} />
                                                    </span>
                                                    <span class="ms-5">
                                                        <span class="fs-4 fw-bolder text-gray-800 d-block">Percentage %</span>
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="fv-row mb-10" id="percentage-2">
                                        <div>
                                            <label class="required form-label">Discount Percentage %</label>
                                            <input type="tel" class="form-control" name="percentage-3" id="percentage-1">
                                        </div>
                                    </div>
                                    <div class="d-none mb-10 fv-row" id="discount_percent">
                                        <label class="form-label">Set Discount Percentage</label>
                                        <div class="d-flex flex-column text-center mb-5">
                                            <div class="d-flex align-items-start justify-content-center mb-7">
                                                <span class="fw-bolder fs-3x" id="kt_ecommerce_add_product_discount_label">0</span>
                                                <span class="fw-bolder fs-4 mt-1 ms-2">%</span>
                                            </div>
                                            <div id="kt_ecommerce_add_product_discount_slider" class="noUi-sm"></div>
                                        </div>
                                        <div class="text-muted fs-7">Set a percentage discount to be applied on this product.</div>
                                    </div>
                                    <div class="d-none mb-10 fv-row" id="discount_price">
                                        <label class="form-label">Fixed Discounted Price</label>
                                        <input type="text" name="dicsounted_price" class="form-control mb-2" placeholder="Discounted price" />
                                        <div class="text-muted fs-7">Set the discounted product price. The product will be reduced at the determined fixed price</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Tax</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="d-flex flex-wrap gap-5">
                                        <div class="fv-row w-100 flex-md-root">
                                            <label class="required form-label">Tax Class</label>
                                            <select class="form-select mb-2" name="tax" data-control="select2" data-hide-search="true" data-placeholder="Select an option">
                                                <option></option>
                                                <option value="0">Tax Free</option>
                                                <option value="1" selected="selected">Taxable Goods</option>
                                                <option value="2">Downloadable Product</option>
                                            </select>
                                            <div class="text-muted fs-7">Set the product tax class.</div>
                                        </div>
                                        <div class="fv-row w-100 flex-md-root">
                                            <label class="form-label">VAT Amount (%)</label>
                                            <input type="text" class="form-control mb-2" value="35" />
                                            <div class="text-muted fs-7">Set the product VAT about.</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Meta Options</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="mb-10">
                                        <label class="form-label">Meta Tag Title</label>
                                        <input type="text" class="form-control mb-2" name="meta_title" placeholder="Meta tag name" />
                                        <div class="text-muted fs-7">Set a meta tag title. Recommended to be simple and precise keywords.</div>
                                    </div>
                                    <div class="mb-10">
                                        <label class="form-label">Meta Tag Description</label>
                                        <input type="text" class="form-control mb-2" name="meta_title" placeholder="Meta tag name" />
                                        <div class="text-muted fs-7">Set a meta tag description to the product for increased SEO ranking.</div>
                                    </div>
                                    <div>
                                        <label class="form-label">Meta Tag Keywords</label>
                                        <input id="kt_ecommerce_add_product_meta_keywords" name="kt_ecommerce_add_product_meta_keywords" class="form-control mb-2" />
                                        <div class="text-muted fs-7">Set a list of keywords that the product is related to. Separate the keywords by adding a comma
                                        <code>,</code>between each keyword.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="kt_ecommerce_add_product_advanced" role="tab-panel">
                        <div class="d-flex flex-column gap-7 gap-lg-10">
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Inventory</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="mb-10 fv-row">
                                        <label class="required form-label">SKU</label>
                                        <input type="text" name="sku" class="form-control mb-2" placeholder="SKU Number" value="011985001" />
                                        <div class="text-muted fs-7">Enter the product SKU.</div>
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label class="required form-label">Barcode</label>
                                        <input type="text" name="sku" class="form-control mb-2" placeholder="Barcode Number" value="45874521458" />
                                        <div class="text-muted fs-7">Enter the product barcode number.</div>
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label class="required form-label">Quantity</label>
                                        <div class="d-flex gap-3">
                                            <input type="number" name="shelf" class="form-control mb-2" placeholder="On shelf" value="24" />
                                            <input type="number" name="warehouse" class="form-control mb-2" placeholder="In warehouse" />
                                        </div>
                                        <div class="text-muted fs-7">Enter the product quantity.</div>
                                    </div>
                                    <div class="fv-row">
                                        <label class="form-label">Allow Backorders</label>
                                        <div class="form-check form-check-custom form-check-solid mb-2">
                                            <input class="form-check-input" type="checkbox" value="" />
                                            <label class="form-check-label">Yes</label>
                                        </div>
                                        <div class="text-muted fs-7">Allow customers to purchase products that are out of stock.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Variations</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="" data-kt-ecommerce-catalog-add-product="auto-options">
                                        <label class="form-label">Add Product Variations</label>
                                        <div id="kt_ecommerce_add_product_options">
                                            <div class="form-group">
                                                <div data-repeater-list="kt_ecommerce_add_product_options" class="d-flex flex-column gap-3">
                                                    <div data-repeater-item="" class="form-group d-flex flex-wrap gap-5">
                                                        <div class="w-100 w-md-200px">
                                                            <select class="form-select" name="product_option" data-placeholder="Select a variation" data-kt-ecommerce-catalog-add-product="product_option">
                                                                <option></option>
                                                                <option value="color">Color</option>
                                                                <option value="size">Size</option>
                                                                <option value="material">Material</option>
                                                                <option value="style">Style</option>
                                                            </select>
                                                        </div>
                                                        <input type="text" class="form-control mw-100 w-200px" name="product_option_value" placeholder="Variation" />
                                                        <button type="button" data-repeater-delete="" class="btn btn-sm btn-icon btn-light-danger">
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <rect opacity="0.5" x="7.05025" y="15.5356" width="12" height="2" rx="1" transform="rotate(-45 7.05025 15.5356)" fill="currentColor" />
                                                                    <rect x="8.46447" y="7.05029" width="12" height="2" rx="1" transform="rotate(45 8.46447 7.05029)" fill="currentColor" />
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mt-5">
                                                <button type="button" data-repeater-create="" class="btn btn-sm btn-light-primary">
                                                    <span class="svg-icon svg-icon-2">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                            <rect opacity="0.5" x="11" y="18" width="12" height="2" rx="1" transform="rotate(-90 11 18)" fill="currentColor" />
                                                            <rect x="6" y="11" width="12" height="2" rx="1" fill="currentColor" />
                                                        </svg>
                                                    </span>
                                                    Add another variation
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Shipping</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="fv-row">
                                        <div class="form-check form-check-custom form-check-solid mb-2">
                                            <input class="form-check-input" type="checkbox" id="kt_ecommerce_add_product_shipping_checkbox" value="1" checked="checked" />
                                            <label class="form-check-label">This is a physical product</label>
                                        </div>
                                        <div class="text-muted fs-7">Set if the product is a physical or digital item. Physical products may require shipping.</div>
                                    </div>
                                    <div id="kt_ecommerce_add_product_shipping" class="mt-10">
                                        <div class="mb-10 fv-row">
                                            <label class="form-label">Weight</label>
                                            <input type="text" name="weight" class="form-control mb-2" placeholder="Product weight" value="4.3" />
                                            <div class="text-muted fs-7">Set a product weight in kilograms (kg).</div>
                                        </div>
                                        <div class="fv-row">
                                            <label class="form-label">Dimension</label>
                                            <div class="d-flex flex-wrap flex-sm-nowrap gap-3">
                                                <input type="number" name="width" class="form-control mb-2" placeholder="Width (w)" value="12" />
                                                <input type="number" name="height" class="form-control mb-2" placeholder="Height (h)" value="4" />
                                                <input type="number" name="length" class="form-control mb-2" placeholder="Lengtn (l)" value="8.5" />
                                            </div>
                                            <div class="text-muted fs-7">Enter the product dimensions in centimeters (cm).</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Meta Options</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="mb-10">
                                        <label class="form-label">Meta Tag Title</label>
                                        <input type="text" class="form-control mb-2" name="meta_title" placeholder="Meta tag name" />
                                        <div class="text-muted fs-7">Set a meta tag title. Recommended to be simple and precise keywords.</div>
                                    </div>
                                    <div class="mb-10">
                                        <label class="form-label">Meta Tag Description</label>
                                        <div id="kt_ecommerce_add_product_meta_description" name="kt_ecommerce_add_product_meta_description" class="min-h-100px mb-2"></div>
                                        <div class="text-muted fs-7">Set a meta tag description to the product for increased SEO ranking.</div>
                                    </div>
                                    <div>
                                        <label class="form-label">Meta Tag Keywords</label>
                                        <input id="kt_ecommerce_add_product_meta_keywords" name="kt_ecommerce_add_product_meta_keywords" class="form-control mb-2" />
                                        <div class="text-muted fs-7">Set a list of keywords that the product is related to. Separate the keywords by adding a comma
                                        <code>,</code>between each keyword.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="kt_ecommerce_add_product_reviews" role="tab-panel">
                        <div class="d-flex flex-column gap-7 gap-lg-10">
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Customer Reviews</h2>
                                    </div>
                                    <div class="card-toolbar">
                                        <span class="fw-bolder me-5">Overall Rating:</span>
                                        <div class="rating">
                                            <div class="rating-label checked">
                                                <span class="svg-icon svg-icon-2">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                    </svg>
                                                </span>
                                            </div>
                                            <div class="rating-label checked">
                                                <span class="svg-icon svg-icon-2">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                    </svg>
                                                </span>
                                            </div>
                                            <div class="rating-label checked">
                                                <span class="svg-icon svg-icon-2">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                    </svg>
                                                </span>
                                            </div>
                                            <div class="rating-label checked">
                                                <span class="svg-icon svg-icon-2">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                    </svg>
                                                </span>
                                            </div>
                                            <div class="rating-label">
                                                <span class="svg-icon svg-icon-2">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                    </svg>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <table class="table table-row-dashed fs-6 gy-5 my-0" id="kt_ecommerce_add_product_reviews">
                                        <thead>
                                            <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                                                <th class="w-10px pe-2">
                                                    <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                                        <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_ecommerce_add_product_reviews .form-check-input" value="1" />
                                                    </div>
                                                </th>
                                                <th class="min-w-125px">Rating</th>
                                                <th class="min-w-175px">Customer</th>
                                                <th class="min-w-175px">Comment</th>
                                                <th class="min-w-100px text-end fs-7">Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="form-check form-check-sm form-check-custom form-check-solid mt-1">
                                                        <input class="form-check-input" type="checkbox" value="1" />
                                                    </div>
                                                </td>
                                                <td data-order="rating-5">
                                                    <div class="rating">
                                                        <div class="rating-label checked">
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                                </svg>
                                                            </span>
                                                        </div>
                                                        <div class="rating-label checked">
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                                </svg>
                                                            </span>
                                                        </div>
                                                        <div class="rating-label checked">
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                                </svg>
                                                            </span>
                                                        </div>
                                                        <div class="rating-label checked">
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                                </svg>
                                                            </span>
                                                        </div>
                                                        <div class="rating-label checked">
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z" fill="currentColor" />
                                                                </svg>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <a href="../../demo1/dist/apps/inbox/reply.html" class="d-flex text-dark text-gray-800 text-hover-primary">
                                                        <div class="symbol symbol-circle symbol-25px me-3">
                                                            <div class="symbol-label bg-light-danger">
                                                                <span class="text-danger">M</span>
                                                            </div>
                                                        </div>
                                                        <span class="fw-bolder">Melody Macy</span>
                                                    </a>
                                                </td>
                                                <td class="text-gray-600 fw-bolder">I like this design</td>
                                                <td class="text-end">
                                                    <span class="fw-bold text-muted">Today</span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-end">
                    <a href="../../demo1/dist/apps/ecommerce/catalog/products.html" id="kt_ecommerce_add_product_cancel" class="btn btn-light me-5">Cancel</a>
                    @if($data->id)
                    <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.product.update',$data->id)}}','PATCH');" class="btn btn-primary">
                        <span class="indicator-label">Save Changes</span>
                        <span class="indicator-progress">Please wait...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                    @else
                    <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.product.store',$productCategory->id)}}','POST');" class="btn btn-primary">
                        <span class="indicator-label">Save Changes</span>
                        <span class="indicator-progress">Please wait...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                    @endif
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    obj_quill('description');
    obj_tagify('#tags');
    ribuan('price_reguler');
    ribuan('price_extend');
    number_only('percentage-1');
    let percentage = $('#percentage-11');
    $('#percentage-0').hide();
    percentage.click(function() {
        $('#percentage-0').show();
    });
    let disct = $('#no-discount');
    disct.click(function() {
        $('#percentage-0').hide();
    });

    number_only('percentage-3');
    let percent = $('#percentage-22');
    $('#percentage-2').hide();
    percent.click(function() {
        $('#percentage-2').show();
    });
    let disct2 = $('#no-discount2');
    disct2.click(function() {
        $('#percentage-2').hide();
    });
    obj_dropzone("product_gallery","{{route('office.product-gallery.store')}}","10");
    obj_dropzone("product_file","{{route('office.product-file.store')}}","1");
    $("input[name='extend_discount']").on('change', function() {
        if ($(this).val() == '1') {
            $('#discount_percentage').addClass('d-none');
            $('#discount_amount').addClass('d-none');
        } else if ($(this).val() == '2'){
            $('#discount_percentage').removeClass('d-none');
            $('#discount_amount').addClass('d-none');
        }else {
            $('#discount_percentage').addClass('d-none');
            $('#discount_amount').removeClass('d-none');
        }
    });
    $("input[name='regular_discount']").on('change', function() {
        if ($(this).val() == '1') {
            $('#discount_percentage').addClass('d-none');
            $('#discount_amount').addClass('d-none');
        } else if ($(this).val() == '2'){
            $('#discount_percentage').removeClass('d-none');
            $('#discount_amount').addClass('d-none');
        }else {
            $('#discount_percentage').addClass('d-none');
            $('#discount_amount').removeClass('d-none');
        }
    });
    $("#status").on('change', function() {
        if(this.value == "Published"){
            $("#scheduled_product").removeClass('d-none');
            $("#scheduled_product").addClass('d-none');
            $("#product_status").removeClass('bg-danger');
            $("#product_status").removeClass('bg-warning');
            $("#product_status").addClass('bg-success');
        }else if(this.value == "Draft"){
            $("#scheduled_product").removeClass('d-none');
            $("#scheduled_product").addClass('d-none');
            $("#product_status").removeClass('bg-success');
            $("#product_status").removeClass('bg-danger');
            $("#product_status").addClass('bg-warning');
        }else if(this.value == "Scheduled"){
            obj_date_time('date_publish');
            $("#scheduled_product").removeClass('d-none');
            $("#product_status").removeClass('bg-warning');
            $("#product_status").removeClass('bg-success');
            $("#product_status").removeClass('bg-danger');
            $("#product_status").addClass('bg-primary');
        }else{
            $("#scheduled_product").removeClass('d-none');
            $("#scheduled_product").addClass('d-none');
            $("#product_status").removeClass('bg-warning');
            $("#product_status").removeClass('bg-success');
            $("#product_status").addClass('bg-danger');
        }
    });
</script>