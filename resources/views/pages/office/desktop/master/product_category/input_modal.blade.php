<div class="modal-header pb-0 border-0 justify-content-end">
    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y mx-5 mx-xl-18 pt-0 pb-15">
    <form id="form_input" class="form d-flex flex-column flex-lg-row">
        <div class="d-flex flex-column gap-7 gap-lg-10 w-100 w-lg-300px mb-7 me-lg-10">
            <div class="card card-flush py-4">
                <div class="card-header">
                    <div class="card-title">
                        <h2>Thumbnail</h2>
                    </div>
                </div>
                <div class="card-body text-center pt-0">
                    <div class="image-input image-input-empty image-input-outline mb-3" style="background-image: url(assets/media/svg/files/blank-image.svg)">
                        <div class="image-input-wrapper w-150px h-150px"></div>
                        <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                            <i class="bi bi-pencil-fill fs-7"></i>
                            <input type="file" name="thumbnail" accept=".png, .jpg, .jpeg, .svg" />
                            <input type="hidden" name="avatar_remove" />
                        </label>
                        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                            <i class="bi bi-x fs-2"></i>
                        </span>
                        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                            <i class="bi bi-x fs-2"></i>
                        </span>
                    </div>
                    <div class="text-muted fs-7">Set the category thumbnail image. Only *.png, *.jpg and *.jpeg image files are accepted</div>
                </div>
            </div>
            <div class="card card-flush py-4">
                <div class="card-header">
                    <div class="card-title">
                        <h2>Status</h2>
                    </div>
                    <div class="card-toolbar">
                        <div class="rounded-circle bg-{{$data->st == "Published" ? 'success' : 'danger'}} w-15px h-15px" id="category_status"></div>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <select class="form-select mb-2" id="status" name="status">
                        <option value="">Pilih Status</option>
                        <option value="Published" {{$data->st == "Published" ? 'selected' : ''}}>Published</option>
                        <option value="Unpublished" {{$data->st == "Unpublished" ? 'selected' : ''}}>Unpublished</option>
                    </select>
                    <div class="text-muted fs-7">Set the category status.</div>
                    <div class="d-none mt-10">
                        <label for="kt_ecommerce_add_category_status_datepicker" class="form-label">Select publishing date and time</label>
                        <input class="form-control" id="kt_ecommerce_add_category_status_datepicker" placeholder="Pick date &amp; time" />
                    </div>
                </div>
            </div>
            <div class="card card-flush py-4">
                <div class="card-header">
                    <div class="card-title">
                        <h2>Product Category</h2>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <select class="form-select mb-2" id="status" name="product_category_id">
                        <option value="0" selected="selected">Pilih Kategori Produk</option>
                        @foreach ( $category as $item )
                            <option value="{{ $item->id }}" {{$data->product_category_id == $item->id ? 'selected' : ''}}>{{ $item->name }}</option>
                        @endforeach
                    </select>
                    <div class="text-muted fs-7">Set the Product category.</div>
                </div>
            </div>
            <div class="card card-flush py-4 d-none">
                <div class="card-header">
                    <div class="card-title">
                        <h2>Store Template</h2>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <label for="kt_ecommerce_add_category_store_template" class="form-label">Select a store template</label>
                    <select class="form-select mb-2" data-control="select2" data-hide-search="true" data-placeholder="Select an option" id="kt_ecommerce_add_category_store_template">
                        <option></option>
                        <option value="default" selected="selected">Default template</option>
                        <option value="electronics">Electronics</option>
                        <option value="office">Office stationary</option>
                        <option value="fashion">Fashion</option>
                    </select>
                    <div class="text-muted fs-7">Assign a template from your current theme to define how the category products are displayed.</div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10">
            <div class="card card-flush py-4">
                <div class="card-header">
                    <div class="card-title">
                        <h2>General</h2>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Category Name</label>
                        <input type="text" name="category_name" class="form-control mb-2" placeholder="Product name" value="{{ $data->name }}" />
                        <div class="text-muted fs-7">A category name is required and recommended to be unique.</div>
                    </div>
                    <div>
                        <label class="form-label">Description</label>
                        <div id="description" class="min-h-200px mb-2">{!!$data->description!!}</div>
                        <textarea class="form-control d-none" name="description" value="{{$data->description}}"></textarea>
                        <div class="text-muted fs-7">Set a description to the category for better visibility.</div>
                    </div>
                </div>
            </div>
            <div class="card card-flush py-4">
                <div class="card-header">
                    <div class="card-title">
                        <h2>Meta Options</h2>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="mb-10">
                        <label class="form-label">Meta Tag Title</label>
                        <input type="text" class="form-control mb-2" name="meta_title" placeholder="Meta tag name" value="{{ $data->tag_title }}" />
                        <div class="text-muted fs-7">Set a meta tag title. Recommended to be simple and precise keywords.</div>
                    </div>
                    <div class="mb-10">
                        <label class="form-label">Meta Tag Description</label>
                        <div id="meta_description" class="min-h-100px mb-2">{!!$data->tag_description!!}</div>
                        <textarea class="form-control d-none" name="meta_description" value="{{$data->tag_description}}"></textarea>
                        <div class="text-muted fs-7">Set a meta tag description to the category for increased SEO ranking.</div>
                    </div>
                    <div>
                        <label class="form-label">Meta Tag Keywords</label>
                        <input id="tag_keyword" name="tag_keyword" class="form-control mb-2" value="{{ $data->tag_keyword }}" />
                        <div class="text-muted fs-7">Set a list of keywords that the category is related to. Separate the keywords by adding a comma
                        <code>,</code>between each keyword.</div>
                    </div>
                </div>
            </div>
            <div class="d-none card card-flush py-4">
                <div class="card-header">
                    <div class="card-title">
                        <h2>Automation</h2>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div>
                        <label class="form-label mb-5">Product assignment method</label>
                        <div class="d-flex fv-row">
                            <div class="form-check form-check-custom form-check-solid">
                                <input class="form-check-input me-3" name="method" type="radio" value="0" id="kt_ecommerce_add_category_automation_0" checked='checked' />
                                <label class="form-check-label" for="kt_ecommerce_add_category_automation_0">
                                    <div class="fw-bolder text-gray-800">Manual</div>
                                    <div class="text-gray-600">Add products to this category one by one by manually selecting this category during product creation or update.</div>
                                </label>
                            </div>
                        </div>
                        <div class='separator separator-dashed my-5'></div>
                        <div class="d-flex fv-row">
                            <div class="form-check form-check-custom form-check-solid">
                                <input class="form-check-input me-3" name="method" type="radio" value="1" id="kt_ecommerce_add_category_automation_1" />
                                <label class="form-check-label" for="kt_ecommerce_add_category_automation_1">
                                    <div class="fw-bolder text-gray-800">Automatic</div>
                                    <div class="text-gray-600">Products matched with the following conditions will be automatically assigned to this category.</div>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="mt-10" data-kt-ecommerce-catalog-add-category="auto-options">
                        <label class="form-label">Conditions</label>
                        <div class="d-flex flex-wrap align-items-center text-gray-600 gap-5 mb-7">
                            <span>Products must match:</span>
                            <div class="form-check form-check-custom form-check-solid">
                                <input class="form-check-input" type="radio" name="conditions" value="" id="all_conditions" checked="checked" />
                                <label class="form-check-label" for="all_conditions">All conditions</label>
                            </div>
                            <div class="form-check form-check-custom form-check-solid">
                                <input class="form-check-input" type="radio" name="conditions" value="" id="any_conditions" />
                                <label class="form-check-label" for="any_conditions">Any conditions</label>
                            </div>
                        </div>
                        <div id="kt_ecommerce_add_category_conditions">
                            <div class="form-group">
                                <div data-repeater-list="kt_ecommerce_add_category_conditions" class="d-flex flex-column gap-3">
                                    <div data-repeater-item="" class="form-group d-flex flex-wrap gap-5">
                                        <div class="w-100 w-md-200px">
                                            <select class="form-select" name="condition_type" data-placeholder="Select an option" data-kt-ecommerce-catalog-add-category="condition_type">
                                                <option></option>
                                                <option value="title">Product Title</option>
                                                <option value="tag" selected="selected">Product Tag</option>
                                                <option value="price">Prodict Price</option>
                                            </select>
                                        </div>
                                        <div class="w-100 w-md-200px">
                                            <select class="form-select" name="condition_equals" data-placeholder="Select an option" data-kt-ecommerce-catalog-add-category="condition_equals">
                                                <option></option>
                                                <option value="equal" selected="selected">is equal to</option>
                                                <option value="notequal">is not equal to</option>
                                                <option value="greater">is greater than</option>
                                                <option value="less">is less than</option>
                                                <option value="starts">starts with</option>
                                                <option value="ends">ends with</option>
                                            </select>
                                        </div>
                                        <input type="text" class="form-control mw-100 w-200px" name="condition_label" placeholder="" />
                                        <button type="button" data-repeater-delete="" class="btn btn-sm btn-icon btn-light-danger">
                                            <span class="svg-icon svg-icon-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                    <rect opacity="0.5" x="7.05025" y="15.5356" width="12" height="2" rx="1" transform="rotate(-45 7.05025 15.5356)" fill="currentColor" />
                                                    <rect x="8.46447" y="7.05029" width="12" height="2" rx="1" transform="rotate(45 8.46447 7.05029)" fill="currentColor" />
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mt-5">
                                <button type="button" data-repeater-create="" class="btn btn-sm btn-light-primary">
                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="11" y="18" width="12" height="2" rx="1" transform="rotate(-90 11 18)" fill="currentColor" />
                                            <rect x="6" y="11" width="12" height="2" rx="1" fill="currentColor" />
                                        </svg>
                                    </span>
                                    Add another condition
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-end">
                <a href="javascript:;" onclick="load_list(1);" class="btn btn-light me-5">Cancel</a>
                <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.product-category.store')}}','POST');" class="btn btn-primary">
                    <span class="indicator-label">Save Changes</span>
                    <span class="indicator-progress">Please wait...
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </div>
    </form>
</div>
<script>
    obj_quill('description');
    obj_quill('meta_description');
    obj_tagify('#tag_keyword');
    $("#status").on('change', function() {
        if(this.value == "Published"){
            $("#category_status").removeClass('bg-danger');
            $("#category_status").addClass('bg-success');
        }else{
            $("#category_status").removeClass('bg-success');
            $("#category_status").addClass('bg-danger');
        }
    });
</script>