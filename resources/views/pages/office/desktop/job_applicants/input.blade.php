<div class="toolbar" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
        <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
            <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">Job Applicants</h1>
            <span class="h-20px border-gray-300 border-start mx-4"></span>
            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                <li class="breadcrumb-item text-muted">Job Applicants</li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-300 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{Auth::guard('office')->user()->id ? 'Complete Your Job Applicants '. Auth::guard('office')->user()->name : 'Add new'}}</li>
            </ul>
        </div>
        <div class="d-flex align-items-center gap-2 gap-lg-3 d-none">
            <a href="javascript:;" onclick="main_content('content_list');" class="btn btn-sm btn-primary">Back</a>
        </div>
    </div>
</div>
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <form id="form_input" class="form d-flex flex-column flex-lg-row">
            <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10">
                <ul class="nav nav-custom nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-bold mb-n2 d-none">
                    <li class="nav-item">
                        <a class="nav-link text-active-primary pb-4 active" data-bs-toggle="tab" href="#kt_ecommerce_add_product_general">General</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="kt_ecommerce_add_product_general" role="tab-panel">
                        <div class="d-flex flex-column gap-7 gap-lg-10">
                            <div class="card card-flush py-4 d-none">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>General</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="mb-10 fv-row d-none">
                                        <label class="required form-label">Name</label>
                                        <input type="text" name="id" class="form-control mb-2" placeholder="Eg : Rizky Ramadhan" value="{{ $data->id }}" />
                                        <div class="text-muted fs-7">A Employee name is required.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Information</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="mb-10 fv-row">
                                        <label class="required form-label">Expected Salary</label>
                                        <input type="text" name="expected_salary" id="expected_salary" class="form-control mb-2" placeholder="Expected Salary Eg : 1,000,000" value="" />
                                        <div class="text-muted fs-7">A Employee Expected Salary is required.</div>
                                    </div>
                                    <div>
                                        <label class="form-label required">Message</label>
                                        <div id="message" class="min-h-200px mb-2"></div>
                                        <textarea class="form-control d-none" name="message"></textarea>
                                        <div class="text-muted fs-7">Set a message to the yadaekidanta for better visibility.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-end">
                    <a href="javascript:;" onclick="main_content('content_list');" class="btn btn-light me-5">Cancel</a>
                    <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('office.my-applicants.store')}}','POST');" class="btn btn-primary">
                        <span class="indicator-label">Save Changes</span>
                        <span class="indicator-progress">Please wait...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    // obj_dropzone_files("cv","","1");
    obj_enddatenow('date_birth');
    obj_quill('message');
    ribuan('expected_salary');
    @if($data->province_id)
    $('#province').val('{{$data->province_id}}');
    setTimeout(function(){ 
        $('#province').trigger('change');
        setTimeout(function(){ 
            $('#city').val('{{$data->city_id}}');
            $('#city').trigger('change');
            setTimeout(function(){ 
                $('#subdistrict').val('{{$data->subdistrict_id}}');
                $('#subdistrict').trigger('change');
            }, 1200);
            setTimeout(function(){ 
                $('#postcode').val('{{$data->postcode}}');
            }, 1200);
        }, 1200);
    }, 500);
    @endif
    $("#department").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('office.department.get_position')}}",
            data: {department : $("#department").val()},
            success: function(response){
                $("#position").html(response);
            }
        });
    });
    $("#province").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('office.province.get_city')}}",
            data: {province : $("#province").val()},
            success: function(response){
                $("#city").html(response);
            }
        });
    });
    $("#city").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('office.city.get_subdistrict')}}",
            data: {city : $("#city").val()},
            success: function(response){
                $("#subdistrict").html(response);
            }
        });
    });
    $("#subdistrict").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('office.subdistrict.get_postcode')}}",
            data: {subdistrict : $("#subdistrict").val()},
            success: function(response){
                $("#postcode").html(response);
            }
        });
    });
</script>