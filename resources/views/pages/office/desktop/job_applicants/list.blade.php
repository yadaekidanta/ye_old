{{-- @if ($collection->count() > 0) --}}
<table class="table align-middle table-row-dashed fs-6 gy-5">
    <thead>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            @if(Auth::guard('office')->user()->department_id != 0)
            <th class="w-10px pe-2">
                <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                    <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target=".collection-check" />
                </div>
            </th>
            @endif
            <th class="min-w-250px">Department & Position</th>
            <th class="min-w-250px">Title</th>
            <th class="min-w-150px">Requirements</th>
            <th class="min-w-150px">Facilities</th>
            <th class="min-w-150px">Rates</th>
            <th class="min-w-150px">Status</th>
        </tr>
    </thead>
    <tbody class="fw-bold text-gray-600">
        @foreach ($collection as $key => $item)
        @php
        $name = Str::of($item->vacancy->title)->explode(' ');
        $singkatan ='';
        foreach($name as $kata)
        {
        $singkatan .= substr($kata, 0, 1);
        }
        if($item->st == "a"){
            $text = "accepted";
            $color = "success";
        }elseif($item->st=="p"){
            $text = "pending";
            $color = "warning";
        }else{
            $text = "rejected";
            $color = "danger";
        }
        @endphp
        <tr>
            @if(Auth::guard('office')->user()->department_id != 0)
            <td>
                <div class="form-check form-check-sm form-check-custom form-check-solid">
                    <input class="form-check-input collection-check" name="list_id" type="checkbox" value="{{$item->id}}" />
                </div>
            </td>
            @endif
            <td>
                <div class="d-flex">
                    <a href="javascript:;" class="symbol symbol-50px">
                        <div class="symbol-label fs-2 fw-bold text-success">{{$singkatan}}</div>
                    </a>
                    <div class="ms-5">
                        <a href="javascript:;" class="text-gray-800 text-hover-primary fs-5 fw-bolder mb-1">{{$item->vacancy->position_id ? $item->vacancy->position->name : ''}}</a>
                        <div class="text-muted fs-7 fw-bolder">{{$item->vacancy->department_id ? $item->vacancy->department->name : ''}}</div>
                    </div>
                </div>
            </td>
            <td>
                <div class="d-flex">
                    <div class="ms-5">
                        <a href="javascript:;" class="text-gray-800 text-hover-primary fs-5 fw-bolder mb-1">{{$item->vacancy->title}}</a>
                        <div class="text-muted fs-7 fw-bolder">{!!$item->vacancy->description!!}</div>
                    </div>
                </div>
            </td>
            <td>
                <div class="d-flex">
                    <div class="ms-5">
                        <div class="text-muted fs-7 fw-bolder">{!!$item->vacancy->requirement!!}</div>
                    </div>
                </div>
            </td>
            <td>
                <div class="d-flex">
                    <div class="ms-5">
                        <div class="text-muted fs-7 fw-bolder">{!!$item->vacancy->facilities!!}</div>
                    </div>
                </div>
            </td>
            <td>
                <div class="d-flex">
                    <div class="ms-5">
                        <div class="text-muted fs-7 fw-bolder">{{number_format($item->vacancy->rates)}}</div>
                    </div>
                </div>
            </td>
            <td>
                <div class="d-flex">
                    <div class="ms-5">
                        <div class="badge badge-light-{{$color}}">{{$text}}</div>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('themes.office.desktop.pagination')}}
{{-- @else
<div class="text-center px-4 mb-3">
    <img class="mw-100 mh-300px" alt="" src="{{asset('keenthemes/media/illustrations/sigma-1/18.png')}}">
</div>
@endif --}}