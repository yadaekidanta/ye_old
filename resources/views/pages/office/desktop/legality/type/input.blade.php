<div class="toolbar" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
        <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
            <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">HRM</h1>
            <span class="h-20px border-gray-300 border-start mx-4"></span>
            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                <li class="breadcrumb-item text-muted">Document Type</li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-300 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{$data->id ? 'Update '. $data->name : 'Add new'}}</li>
            </ul>
        </div>
        <div class="d-flex align-items-center gap-2 gap-lg-3">
            <a href="javascript:;" onclick="main_content('content_list');" class="btn btn-sm btn-primary">Back</a>
        </div>
    </div>
</div>
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <form id="form_input" class="form d-flex flex-column flex-lg-row">
            <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="tab_general" role="tab-panel">
                        <div class="d-flex flex-column gap-7 gap-lg-10">
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>General</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="mb-10 fv-row">
                                        <label class="required form-label">Name</label>
                                        <input type="text" name="name" class="form-control mb-2" placeholder="Name Type" value="{{ $data->name }}" />
                                        <div class="text-muted fs-7">A Document Type Name is required.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-end">
                    <a href="javascript:;" onclick="main_content('content_list');" class="btn btn-light me-5">Cancel</a>
                    <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{$data->id ? route('office.legal-doc-type.update',$data->id) : route('office.legal-doc-type.store')}}','{{$data->id ? 'PATCH' : 'POST'}}');" class="btn btn-primary">
                        <span class="indicator-label">Save Changes</span>
                        <span class="indicator-progress">Please wait...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    obj_quill('description');
    obj_quill('requirement');
    obj_quill('facilities');
    ribuan('rates');
    $("#status").on('change', function() {
        if(this.value == "Open"){
            $("#vacancy_status").removeClass('bg-danger');
            $("#vacancy_status").addClass('bg-success');
        }else{
            $("#vacancy_status").removeClass('bg-success');
            $("#vacancy_status").addClass('bg-danger');
        }
    });
    @if($data->department_id)
    $('#department').val('{{$data->department_id}}');
    setTimeout(function(){ 
        $('#department').trigger('change');
        setTimeout(function(){ 
            $('#position').val('{{$data->position_id}}');
            $('#position').trigger('change');
        }, 1200);
    }, 500);
    @endif
    $("#department").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('office.department.get_position')}}",
            data: {department : $("#department").val()},
            success: function(response){
                $("#position").html(response);
            }
        });
    });
</script>