<div class="toolbar" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
        <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
            <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">Account Overview</h1>
            <span class="h-20px border-gray-300 border-start mx-4"></span>
            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                <li class="breadcrumb-item text-muted">Profile</li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-300 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{Auth::guard('office')->user()->id ? 'Complete Your Profile '. Auth::guard('office')->user()->name : 'Add new'}}</li>
            </ul>
        </div>
        <div class="d-flex align-items-center gap-2 gap-lg-3">
            <a href="javascript:;" onclick="main_content('content_list');" class="btn btn-sm btn-primary">Back</a>
        </div>
    </div>
</div>
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <form id="form_input" class="form d-flex flex-column flex-lg-row">
            <div class="d-flex flex-column gap-7 gap-lg-10 w-100 w-lg-300px mb-7 me-lg-10">
                <div class="card card-flush py-4">
                    <div class="card-header">
                        <div class="card-title">
                            <h2>Avatar</h2>
                        </div>
                    </div>
                    <div class="card-body text-center pt-0">
                        <div class="image-input image-input-empty image-input-outline mb-3" data-kt-image-input="true" style="">
                            <div class="image-input-wrapper w-150px h-150px" style="background-image: url(assets/media//stock/ecommerce/78.gif)"></div>
                            <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                                <i class="bi bi-pencil-fill fs-7"></i>
                                <input type="file" name="avatar" accept=".png, .jpg, .jpeg" />
                                <input type="hidden" name="avatar_remove" />
                            </label>
                            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                                <i class="bi bi-x fs-2"></i>
                            </span>
                            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                                <i class="bi bi-x fs-2"></i>
                            </span>
                        </div>
                        <div class="text-muted fs-7">Set the client avatar image. Only *.png, *.jpg and *.jpeg image files are accepted</div>
                    </div>
                </div>
                <div class="card card-flush py-4">
                    <div class="card-header">
                        <div class="card-title">
                            <h2>Upload CV</h2>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <input type="file" name="cv" class="form-control">
                        @if(Auth::guard('office')->user()->cv)
                        <a download target="_blank" href="{{asset('storage/'.Auth::guard('office')->user()->cv)}}" class="btn mt-3 btn-small btn-bg-light btn-active-color-primary btn-sm me-1">
                            <span class="svg-icon svg-icon-3">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M19 15C20.7 15 22 13.7 22 12C22 10.3 20.7 9 19 9C18.9 9 18.9 9 18.8 9C18.9 8.7 19 8.3 19 8C19 6.3 17.7 5 16 5C15.4 5 14.8 5.2 14.3 5.5C13.4 4 11.8 3 10 3C7.2 3 5 5.2 5 8C5 8.3 5 8.7 5.1 9H5C3.3 9 2 10.3 2 12C2 13.7 3.3 15 5 15H19Z" fill="currentColor"/>
                                    <path d="M13 17.4V12C13 11.4 12.6 11 12 11C11.4 11 11 11.4 11 12V17.4H13Z" fill="currentColor"/>
                                    <path opacity="0.3" d="M8 17.4H16L12.7 20.7C12.3 21.1 11.7 21.1 11.3 20.7L8 17.4Z" fill="currentColor"/>
                                </svg>
                            </span>
                            Download
                        </a>
                        @endif
                        <div class="dropzone d-none">
                            <div class="dz-message needsclick">
                                <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                                <div class="ms-4">
                                    <h3 class="fs-5 fw-bolder text-gray-900 mb-1">Drop files here or click to upload.</h3>
                                    <span class="fs-7 fw-bold text-gray-400">Upload up only 1 files</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10">
                <ul class="nav nav-custom nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-bold mb-n2">
                    <li class="nav-item">
                        <a class="nav-link text-active-primary pb-4 active" data-bs-toggle="tab" href="#kt_ecommerce_add_product_general">General</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="kt_ecommerce_add_product_general" role="tab-panel">
                        <div class="d-flex flex-column gap-7 gap-lg-10">
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>General</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="mb-10 fv-row">
                                        <label class="required form-label">Name</label>
                                        <input type="text" name="name" class="form-control mb-2" placeholder="Eg : Rizky Ramadhan" value="{{ Auth::guard('office')->user()->name }}" />
                                        <div class="text-muted fs-7">A Employee name is required.</div>
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label class="required form-label">Date Birth</label>
                                        <input type="text" id="date_birth" name="date_birth" class="form-control mb-2" placeholder="Eg : {{date('Y-m-d')}}" value="{{ Auth::guard('office')->user()->date_birth }}" />
                                        <div class="text-muted fs-7">A Employee date birth is required.</div>
                                    </div>
                                    <div>
                                        <label class="form-label required">Bio</label>
                                        <div id="bio" class="min-h-200px mb-2">{!!Auth::guard('office')->user()->bio!!}</div>
                                        <textarea class="form-control d-none" name="bio">{!!Auth::guard('office')->user()->bio!!}</textarea>
                                        <div class="text-muted fs-7">Set a bio to the product for better visibility.</div>
                                    </div>
                                    <div>
                                        <label class="form-label required">Impression</label>
                                        <div id="impression" class="min-h-200px mb-2">{!!Auth::guard('office')->user()->impression!!}</div>
                                        <textarea class="form-control d-none" name="impression">{!!Auth::guard('office')->user()->impression!!}</textarea>
                                        <div class="text-muted fs-7">Set a impression to the product for better visibility.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Contact Info</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="mb-10 fv-row">
                                        <label class="required form-label">Email</label>
                                        <input type="text" name="email" class="form-control mb-2" placeholder="Eg : Rizky Ramadhan" value="{{ Auth::guard('office')->user()->email }}" />
                                        <div class="text-muted fs-7">A Employee email is required.</div>
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label class="required form-label">Phone</label>
                                        <input type="text" name="phone" class="form-control mb-2" placeholder="Eg : Rizky Ramadhan" value="{{ Auth::guard('office')->user()->phone }}" />
                                        <div class="text-muted fs-7">A Employee phone is required.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Address Info</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="mb-10 fv-row">
                                        <label class="form-label required">Address</label>
                                        <textarea class="form-control" name="address">{{Auth::guard('office')->user()->address}}</textarea>
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label class="form-label required">Province</label>
                                        <select class="form-control" name="province" id="province">
                                            <option value="">Choose Province</option>
                                            @foreach($province as $p)
                                                <option value="{{$p->id}}">{{$p->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label class="form-label required">City</label>
                                        <select class="form-control" name="city" id="city">
                                            <option value="">Choose Province First</option>
                                        </select>
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label class="form-label required">Subdistrict</label>
                                        <select class="form-control" name="subdistrict" id="subdistrict">
                                            <option value="">Choose City First</option>
                                        </select>
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label class="form-label required">Postcode</label>
                                        <input type="text" class="form-control" name="postcode" id="postcode" value="{{Auth::guard('office')->user()->postcode}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-end">
                    <a href="javascript:;" onclick="main_content('content_list');" class="btn btn-light me-5">Cancel</a>
                    <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.profile.update',Auth::guard('office')->user()->id)}}','PATCH');" class="btn btn-primary">
                        <span class="indicator-label">Save Changes</span>
                        <span class="indicator-progress">Please wait...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    // obj_dropzone_files("cv","","1");
    obj_enddatenow('date_birth');
    obj_quill('bio');
    obj_quill('impression');
    @if(Auth::guard('office')->user()->province_id)
    $('#province').val('{{Auth::guard('office')->user()->province_id}}');
    setTimeout(function(){ 
        $('#province').trigger('change');
        setTimeout(function(){ 
            $('#city').val('{{Auth::guard('office')->user()->city_id}}');
            $('#city').trigger('change');
            setTimeout(function(){ 
                $('#subdistrict').val('{{Auth::guard('office')->user()->subdistrict_id}}');
                $('#subdistrict').trigger('change');
            }, 1200);
            setTimeout(function(){ 
                $('#postcode').val('{{Auth::guard('office')->user()->postcode}}');
            }, 1200);
        }, 1200);
    }, 500);
    @endif
    $("#department").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('office.department.get_position')}}",
            data: {department : $("#department").val()},
            success: function(response){
                $("#position").html(response);
            }
        });
    });
    $("#province").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('office.province.get_city')}}",
            data: {province : $("#province").val()},
            success: function(response){
                $("#city").html(response);
            }
        });
    });
    $("#city").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('office.city.get_subdistrict')}}",
            data: {city : $("#city").val()},
            success: function(response){
                $("#subdistrict").html(response);
            }
        });
    });
    $("#subdistrict").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('office.subdistrict.get_postcode')}}",
            data: {subdistrict : $("#subdistrict").val()},
            success: function(response){
                $("#postcode").html(response);
            }
        });
    });
</script>