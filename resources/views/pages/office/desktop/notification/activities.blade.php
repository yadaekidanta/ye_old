<div class="d-flex flex-stack py-4">
    <!--begin::Section-->
    <div class="d-flex align-items-center me-2">
        <!--begin::Code-->
        <span class="w-70px badge badge-light-success me-4">200 OK</span>
        <!--end::Code-->
        <!--begin::Title-->
        <a href="#" class="text-gray-800 text-hover-primary fw-bold">New order</a>
        <!--end::Title-->
    </div>
    <!--end::Section-->
    <!--begin::Label-->
    <span class="badge badge-light fs-8">Just now</span>
    <!--end::Label-->
</div>