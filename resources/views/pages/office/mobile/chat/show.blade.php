<x-mobile-office-layout title="Selamat datang">
    <div class="page-title page-title-small">
        <h2><a href="#" data-back-button><i class="fa fa-arrow-left"></i></a>{{$employee->name}}</h2>
        <a href="#" data-menu="menu-main" class="bg-fade-highlight-light shadow-xl preload-img" data-src="{{$employee->image}}"></a>
    </div>
    <div class="card header-card " data-card-height="85">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{asset('img/icon.png')}}"></div>
    </div>
    <div id="content_list">
        <div class="content mt-5 pt-3">
            <div id="list_result"></div>
        </div>
    </div>
    @section('custom_js')
        <script>
            setInterval(() => {
                load_list(1);
            }, 1000);
            $("#to_id_chat").val('{{$employee->id}}');
        </script>
    @endsection
</x-mobile-office-layout>