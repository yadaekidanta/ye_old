@foreach($collection as $item)
    @if($item->to_id == Auth::guard('office')->user()->id)
    <div class="d-flex">
        <div class="align-self-end">
            <img src="{{$item->from->image}}" class="bg-fade-red-dark rounded-l me-2" width="30">
        </div>
        <div>
            <div class="speech-bubble speech-right color-black">
                {{$item->body}}
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    @else
    <div class="d-flex">
        <div class="ms-auto">
            <div class="speech-bubble speech-left gradient-highlight">
                {{$item->body}}
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="align-self-end">
            <img src="{{$item->from->image}}" class="bg-fade-blue-dark rounded-l ms-2" width="30">
        </div>
    </div>
    @endif
@endforeach