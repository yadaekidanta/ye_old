<x-mobile-office-layout title="Selamat datang">
    <div class="page-title page-title-large">
        <h2 data-username="{{Auth::guard('office')->user()->name}}" class="greeting-text"></h2>
        <a href="#" data-menu="menu-main" class="bg-fade-highlight-light shadow-xl preload-img" data-src="{{Auth::guard('office')->user()->image}}"></a>
    </div>
    <div class="card header-card shape-rounded" data-card-height="210">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{asset('img/icon.png')}}"></div>
    </div>
    <div id="content_list">
        <div id="list_result"></div>
    </div>
    <div id="content_input"></div>
    @section('custom_js')
        <script>
            setInterval(() => {
                load_list(1);
            }, 1000);
        </script>
    @endsection
</x-mobile-office-layout>