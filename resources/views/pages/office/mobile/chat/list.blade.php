@php
$cachat = App\Models\Communication\Chat::select('from_id')->whereNull('read_at')->where('to_id', Auth::guard('office')->user()->id)->groupBy('from_id')->get();
@endphp
@if($cachat->count() > 0)
<div class="card card-style mb-3 mt-n2">
    <div class="content my-3">
        <div class="d-flex">
            <div class="align-self-center me-auto">
                <span class="icon icon-xxs bg-blue-dark rounded-m scale-switch"><i class="fa fa-envelope"></i></span>
                <span class="ps-2">{{$cachat->count()}} New Chats</span>
            </div>
        </div>
    </div>
</div>
@endif
<div class="card card-style">
    <div class="content mb-n2 pb-2">
        @foreach ($collection as $item)
        @php
        $chat = App\Models\Communication\Chat::whereRaw('(from_id = '.$item->id.' AND to_id = '.Auth::guard('office')->user()->id.') OR (from_id = '.Auth::guard('office')->user()->id.' AND to_id = '.$item->id.')')->orderBy('created_at','DESC');
        $cchat = App\Models\Communication\Chat::whereNull('read_at')->where('from_id', $item->id)->where('to_id', Auth::guard('office')->user()->id)->orderBy('created_at','DESC')->get();
        @endphp
            <div class="d-flex position-relative mb-4">
                <div class="align-self-center pe-3">
                    <img src="{{$item->image}}" width="45" class="rounded-xl bg-fade-blue-dark">
                    <i class="fa fa-circle color-{{Cache::has('is_employee_online' . $item->id) ? 'green' : 'red'}}-dark position-absolute pt-4 mt-3 start-0 font-8"></i>
                </div>
                <a href="{{route('office.chat.show',$item->id)}}" class="align-self-center me-auto">
                    <h5 class="mb-n1 pt-1 font-600 font-16">{{$item->name}}</h5>
                    <p class="mb-0 font-12">{{$chat->first() ? $chat->first()->body : ''}}</p>
                </a>
                <div class="align-self-start ms-auto text-end">
                    <span class="opacity-40 font-11">{{$chat->first() ? $chat->first()->created_at->diffForHumans() : ''}}</span><br>
                    @if($cchat->count() > 0)
                    <span class="bg-green-dark font-10 badge float-end rounded-xl">
                        {{$cchat->count()}} New
                    </span>
                    @else
                    @endif
                </div>
            </div>
        @endforeach
    </div>
</div>