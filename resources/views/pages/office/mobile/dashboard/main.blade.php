<x-mobile-office-layout title="Selamat datang">
    <div class="page-title page-title-large">
        <h2 data-username="{{Auth::guard('office')->user()->name}}" class="greeting-text"></h2>
        <a href="#" data-menu="menu-main" class="bg-fade-highlight-light shadow-xl preload-img" data-src="{{asset('azures/images/avatars/5s.png')}}"></a>
    </div>
    <div class="card header-card shape-rounded" data-card-height="210">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{asset('azures/images/pictures/20s.jpg')}}"></div>
    </div>
    
    <!-- Homepage Slider-->
    <div class="splide single-slider slider-no-arrows slider-no-dots visible-slider" id="single-slider-1">
        <div class="splide__track">
            <div class="splide__list">
                <div class="splide__slide">
                    <div class="card card-style shadow-l bg-18" data-card-height="200">
                        <div class="card-top p-3"><h5 class="color-white font-600">Today</h5></div>
                        <div class="card-top p-3"><a href="#" class="btn btn-xxs bg-white color-black float-end font-600 rounded-s">View</a></div>
                        <div class="card-bottom p-3">
                            <h1 class="font-24 font-700 color-white">2/10 Tasks</h1>
                            <p class="color-white opacity-70 mb-0 mt-n2">You have 8 tasks remaining today.</p>
                        </div>
                        <div class="card-overlay bg-gradient"></div>
                    </div>
                </div>
                <div class="splide__slide">
                    <div class="card card-style shadow-l bg-13" data-card-height="200">
                        <div class="card-top p-3"><h5 class="color-white font-600">Tomorrow</h5></div>
                        <div class="card-top p-3"><a href="#" class="btn btn-xxs bg-white color-black float-end font-600 rounded-s">View</a></div>
                        <div class="card-bottom p-3">
                            <h1 class="font-24 font-700 color-white">6/10 Tasks</h1>
                            <p class="color-white opacity-70 mb-0 mt-n2">You have 4 tasks remaining in this project.</p>
                        </div>
                        <div class="card-overlay bg-gradient"></div>
                    </div>
                </div>
                <div class="splide__slide">
                    <div class="card card-style shadow-l bg-6" data-card-height="200">
                        <div class="card-top p-3"><h5 class="color-white font-600">15th August</h5></div>
                        <div class="card-top p-3"><a href="#" class="btn btn-xxs bg-white color-black float-end font-600 rounded-s">View</a></div>
                        <div class="card-bottom p-3">
                            <h1 class="font-24 font-700 color-white">0/10 Tasks</h1>
                            <p class="color-white opacity-70 mb-0 mt-n2">You've not started this project.</p>
                        </div>
                        <div class="card-overlay bg-gradient"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
          
    <div class="content mb-2 mt-0">
        <h5 class="float-start font-16 font-500">To Do List</h5>
        <a class="float-end font-12 color-highlight mt-n1" href="#">View All</a>
        <div class="clearfix"></div>
    </div>

    <div class="splide double-slider visible-slider slider-no-arrows slider-no-dots mb-4 pb-2" id="double-slider-1">
        <div class="splide__track">
            <div class="splide__list">
                <div class="splide__slide ps-3">
                    <div class="bg-theme rounded-m shadow-m px-3">
                    <i class="mt-4 mb-4" data-feather="home" data-feather-line="1" data-feather-size="45" data-feather-color="blue-dark" data-feather-bg="blue-fade-light"></i>
                    <h5 class="font-16">Design Task Homepage</h5>
                    <p class="line-height-s font-10 mb-0 pb-3">
                        <span><i class="fa fa-flag color-red-dark pe-2 pt-2 font-9"></i>High Priority</span>
                    </p>
                    </div>
                </div>
                <div class="splide__slide ps-3">
                    <div class="bg-theme rounded-m shadow-m px-3">
                    <i class="mt-4 mb-4" data-feather="smartphone" data-feather-line="1" data-feather-size="45" data-feather-color="green-dark" data-feather-bg="green-fade-light"></i>
                    <h5 class="font-16">Create Tasklists</h5>
                    <p class="line-height-s font-10 mb-0 pb-3">
                        <span><i class="fa fa-flag color-blue-dark pe-2 pt-2 font-9"></i>Medium Priority</span>
                    </p>
                    </div>
                </div>
                <div class="splide__slide ps-3">
                    <div class="bg-theme rounded-m shadow-m px-3">
                    <i class="mt-4 mb-4" data-feather="coffee" data-feather-line="1" data-feather-size="45" data-feather-color="brown-dark"  data-feather-bg="brown-fade-light"></i>
                    <h5 class="font-16">Have a Coffee</h5>
                    <p class="line-height-s font-10 mb-0 pb-3">
                        <span><i class="fa fa-flag color-yellow-dark pe-2 pt-2 font-9"></i>Low Priority</span>
                    </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="content mb-2 mt-0">
        <h5 class="float-start font-16 font-500">In Progress</h5>
        <a class="float-end font-12 color-highlight mt-n1" href="#">View All</a>
        <div class="clearfix"></div>
    </div>
        
    <a href="#" class="card card-style mb-2">
        <div class="d-flex pt-2 pb-2">
            <div class="pt-2 mt-1 ps-4">
                <h4 class="color-theme font-600 font-15">Releaes Update</h4>
                <p class="mt-n2 font-11 opacity-70 mb-2">
                    <span><i class="fa fa-clock color-theme opacity-70 pe-2 font-9"></i>2 Hours</span>
                    <span><i class="fa fa-flag color-theme opacity-70 px-2 font-9"></i>High Priority</span>
                </p>
            </div>
            <div class="ms-auto align-self-center me-3">
                <span class="d-block font-9 mb-n2 mt-n1">Progress</span>
                <span class="badge bg-green-dark color-white font-10 py-1 px-2">90%</span>
            </div>
        </div>
    </a>
    <a href="#" class="card card-style mb-2">
        <div class="d-flex pt-2 pb-2">
            <div class="pt-2 mt-1 ps-4">
                <h4 class="color-theme font-600 font-15">Create Task Package</h4>
                <p class="mt-n2 font-11 opacity-70 mb-2">
                    <span><i class="fa fa-clock color-theme opacity-70 pe-2 font-9"></i>2 Hours</span>
                    <span><i class="fa fa-flag color-theme opacity-70 px-2 font-9"></i>Medium Priority</span>
                </p>
            </div>
            <div class="ms-auto align-self-center me-3">
                <span class="d-block font-9 mb-n2 mt-n1">Progress</span>
                <span class="badge bg-highlight color-white font-10 py-1 px-2">70%</span>
            </div>
        </div>
    </a>
    <a href="#" class="card card-style mb-2">
        <div class="d-flex pt-2 pb-2">
            <div class="pt-2 mt-1 ps-4">
                <h4 class="color-theme font-600 font-15">Release Update</h4>
                <p class="mt-n2 font-11 opacity-70 mb-2">
                    <span><i class="fa fa-clock color-theme opacity-70 pe-2 font-9"></i>2 Hours</span>
                    <span><i class="fa fa-flag color-theme opacity-70 px-2 font-9"></i>Low Priority</span>
                </p>
            </div>
            <div class="ms-auto align-self-center me-3">
                <span class="d-block font-9 mb-n2 mt-n1">Progress</span>
                <span class="badge bg-yellow-dark color-white font-10 py-1 px-2">30%</span>
            </div>
        </div>
    </a>
    
    <div class="mt-4"></div>
        
    <div class="card preload-img" data-src="{{asset('azures/images/pictures/20s.jpg')}}">
        <div class="card-body pt-4">
            <h4 class="color-white mb-0">Your Team</h4>
            <p class="color-white mb-3 font-12">
                Team Members you assigned to your projects.
            </p>
            <div class="card card-style ms-0 me-0 bg-theme">
                <div class="row mt-3 mb-3 px-2">
                    <div class="col-6">
                        <div class="d-flex">
                            <div class="align-self-center">
                                <img src="{{asset('azures/images/avatars/1s.png')}}" width="40" class="rounded-m me-2 bg-red-dark" alt="img">
                            </div>
                            <div class="align-self-center">
                                <h5 class="color-theme font-13 font-500 mb-0">Jane Doe</h5>
                                <p class="mb-0 font-10 mt-n2">Front End Developer</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="d-flex">
                            <div class="align-self-center">
                                <img src="{{asset('azures/images/avatars/4s.png')}}" width="40" class="rounded-m bg-blue-dark me-2" alt="img">
                            </div>
                            <div class="align-self-center">
                                <h5 class="color-theme font-13 font-500 mb-0">Max Doeson</h5>
                                <p class="mb-0 font-10 mt-n2">Back End Developer</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mb-3"></div>
                    <div class="col-6">
                        <div class="d-flex">
                            <div class="align-self-center">
                                <img src="{{asset('azures/images/avatars/5s.png')}}" width="40" class="rounded-m bg-brown-dark me-2" alt="img">
                            </div>
                            <div class="align-self-center">
                                <h5 class="color-theme font-13 font-500 mb-0">Sir Jack</h5>
                                <p class="mb-0 font-10 mt-n2">Front End Developer</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="d-flex">
                            <div class="align-self-center">
                                <img src="{{asset('azures/images/avatars/8s.png')}}" width="40" class="rounded-m bg-magenta-dark me-2" alt="img">
                            </div>
                            <div class="align-self-center">
                                <h5 class="color-theme font-13 font-500 mb-0">Dana Coder</h5>
                                <p class="mb-0 font-10 mt-n2">Back End Developer</p>
                            </div>
                        </div>
                    </div>				
                </div>
            </div>
            <a href="#" class="btn btn-full btn-sm text-uppercase font-700 bg-white color-black rounded-sm mt-n3">View Team</a>
        </div>
        <div class="card-overlay bg-highlight opacity-90"></div>
        <div class="card-overlay dark-mode-tint"></div>
    </div>
    
    <!-- Homepage Card-->
    <div class="content mb-2 mt-0">
        <h5 class="float-start font-16 font-500">Assigned to You</h5>
        <a class="float-end font-12 color-highlight mt-n1" href="#">View All</a>
        <div class="clearfix"></div>
    </div>
    <div class="card card-style shadow-l bg-18" data-card-height="200">
        <div class="card-top p-3"><a href="#" class="btn btn-xxs bg-white color-black float-end font-600 rounded-s">View</a></div>
        <div class="card-bottom p-3">
            <h1 class="font-24 font-700 color-white">2/10 Tasks</h1>
            <p class="color-white opacity-70 mb-0 mt-n2">You have 8 tasks remaining today.</p>
        </div>
        <div class="card-overlay bg-gradient"></div>
    </div>
</x-mobile-office-layout>