<div class="card header-card shape-rounded" data-card-height="210">
    <div class="card-overlay bg-highlight opacity-95"></div>
    <div class="card-overlay dark-mode-tint"></div>
    <div class="card-bg preload-img" data-src="{{asset('img/icon.png')}}"></div>
</div>
<div id="content_list">
    <div class="card card-style">
        <div class="content">
            <div class="d-flex">
                <div class="ps-3">
                    <h2 class="mb-0 pt-1">Create Objective KPI</h2>
                </div>
                <div class="ms-auto">
                    <a href="#" onclick="main_content('content_list');" class="btn btn-sm btn-full mb-3 rounded-xl text-uppercase font-900 shadow-s bg-blue-dark">Back</a>
                </div>
            </div>
            <form id="form_input">
                <input type="text" name="kpi_id" class="form-control mb-2" value="{{ $kpi->id }}" hidden>
                <div class="input-style has-borders input-style-always-active no-icon mt-3 mb-4">
                    <label for="department" class="color-highlight font-500">Department</label>
                    <select class="form-select mb-2" id="department" name="department">
                        <option value="">Pilih Department</option>
                        @foreach ($department as $value)
                            <option value="{{$value->id}}" {{$data->status == $value->id ? 'selected' : ''}}>{{$value->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input-style has-borders input-style-always-active no-icon mt-3 mb-4">
                    <label for="position" class="color-highlight font-500">Position</label>
                    <select class="form-select mb-2" id="position" name="position">
                        <option value="">Pilih Position</option>
                    </select>
                </div>
                <div class="input-style has-borders input-style-always-active no-icon mt-3 mb-4">
                    <label for="objective" class="color-highlight font-500">Objective</label>
                    <input type="text" name="objective" class="form-control" value="{{$data->objective}}">
                </div>
                <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{$data->id ? route('office.kpi-objective.update',[$kpi->id, $data->id]) : route('office.kpi-objective.store',$kpi->id)}}','{{$data->id ? 'PATCH' : 'POST'}}');" class="btn btn-border btn-m btn-full mb-3 rounded-xl text-uppercase font-900 border-blue-dark color-blue-dark bg-theme">Save</button>
            </form>
        </div>
    </div>
    <div class="card card-style d-none">
        <div class="content">
        </div>
    </div>
</div>
<script>
    @if($data->department_id)
    $('#department').val('{{$data->department_id}}');
    setTimeout(function(){ 
        $('#department').trigger('change');
        setTimeout(function(){ 
            $('#position').val('{{$data->position_id}}');
            $('#position').trigger('change');
        }, 1200);
    }, 500);
    @endif
    
    $("#department").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('office.department.get_position')}}",
            data: {department : $("#department").val()},
            success: function(response){
                $("#position").html(response);
            }
        });
    });
</script>