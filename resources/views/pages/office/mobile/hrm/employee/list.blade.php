@foreach ($collection as $item)
<div data-splide='{"autoplay":false}' class="single-slider slider-no-arrows slider-no-dots" id="user-slider-{{$item->id}}">
    <div class="splide__track">
        <div class="splide__list">
            <div class="mx-3">
                <div class="d-flex">
                    <div><img src="{{$item->image}}" class="me-3 rounded-circle bg-fade-red-light shadow-l" width="50"></div>
                    <div>
                        <h5 class="mt-1 mb-0">{{$item->name}}</h5>
                        <p class="font-10 mt-n1 color-red-dark">{{$item->position->name}}</p>
                    </div>
                    <div class="ms-auto"><span class="badge bg-red-dark mt-2 p-2 font-8 ml-5">TAP FOR MORE</span></div>
                </div>
            </div>
        </div>      
    </div>
</div>
<div class="divider mt-3"></div>
@endforeach
{{$collection->links('themes.office.mobile.pagination')}}