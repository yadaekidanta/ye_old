<div class="card header-card shape-rounded" data-card-height="210">
    <div class="card-overlay bg-highlight opacity-95"></div>
    <div class="card-overlay dark-mode-tint"></div>
    <div class="card-bg preload-img" data-src="{{asset('img/icon.png')}}"></div>
</div>
<div id="content_list">
    <div class="card card-style">
        <div class="content">
            <div class="d-flex">
                <div class="ps-3">
                    <h2 class="mb-0 pt-1">Create KPI</h2>
                </div>
                <div class="ms-auto">
                    <a href="#" onclick="main_content('content_list');" class="btn btn-sm btn-full mb-3 rounded-xl text-uppercase font-900 shadow-s bg-blue-dark">Back</a>
                </div>
            </div>
            <form id="form_input">
                <div class="input-style has-borders input-style-always-active no-icon mt-3 mb-4">
                    <label for="form5" class="color-highlight font-500">Function</label>
                    <input type="text" name="function" class="form-control" value="{{$data->function}}">
                </div>
                <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{$data->id ? route('office.kpi.update',$data->id) : route('office.kpi.store')}}','{{$data->id ? 'PATCH' : 'POST'}}');" class="btn btn-border btn-m btn-full mb-3 rounded-xl text-uppercase font-900 border-blue-dark color-blue-dark bg-theme">Save</button>
            </form>
        </div>
    </div>
    <div class="card card-style d-none">
        <div class="content">
        </div>
    </div>
</div>