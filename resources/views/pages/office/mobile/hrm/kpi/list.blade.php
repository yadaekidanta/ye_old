@if($collection->count() > 0)
    @foreach ($collection as $item)
    <div class="d-flex">
        <div class='d-none'>
            <img src="images/grocery/1t.jpg" class="rounded-sm" width="55">
        </div>
        <div class="ps-3">
            <a href="{{route('office.kpi.show',$item->id)}}"><h4>{{$item->function}}</h4></a>
        </div>
        <div class="ms-auto">
            <span onclick="load_input('{{route('office.kpi.edit',$item->id)}}');" class="badge bg-green-dark font-700 font-11 text-uppercase">Edit</span>
            <span onclick="handle_confirm('Are you sure ?', 'Yes','No','{{route('office.kpi.destroy',$item->id)}}','DELETE');" class="badge bg-red-dark font-700 font-11 text-uppercase">Delete</span>
            {{-- <h1 class="font-20">$15.25</h1> --}}
        </div>
    </div>
    <div class="divider mt-1"></div>
    @endforeach
@else
<div class="d-flex">
    <div class='d-none'>
        <img src="images/grocery/1t.jpg" class="rounded-sm" width="55">
    </div>
    <div class="ps-3">
        Tidak ada Data
    </div>
</div>
@endif
{{$collection->links('themes.office.mobile.pagination')}}