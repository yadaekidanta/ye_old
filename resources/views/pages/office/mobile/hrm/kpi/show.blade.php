<x-mobile-office-layout title="Selamat datang">
    <div class="page-title page-title-large">
        <h2 data-username="{{Auth::guard('office')->user()->name}}" class="greeting-text"></h2>
        <a href="#" data-menu="menu-main" class="bg-fade-highlight-light shadow-xl preload-img" data-src="{{asset('azures/images/avatars/5s.png')}}"></a>
    </div>
    <div class="card header-card shape-rounded" data-card-height="210">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{asset('img/icon.png')}}"></div>
    </div>
    <div id="content_list">
        <div class="card card-style">
            <div class="content">
                <div class="d-flex">
                    <div class="ps-3">
                        <h2 class="mb-0 pt-1">List Objective KPI</h2>
                    </div>
                    <div class="ms-auto">
                        <a href="#" onclick="load_input('{{route('office.kpi-objective.create',$data->id)}}');" class="btn btn-sm btn-full mb-3 rounded-xl text-uppercase font-900 shadow-s bg-blue-dark">Add Data</a>
                    </div>
                </div>
                <form id="content_filter">
                    <div class="input-style has-borders input-style-always-active no-icon mt-3 mb-4">
                        <label for="form5" class="color-highlight font-500">Search</label>
                        <input type="text" name="keyword" onkeyup="load_list(1);" class="form-control">
                    </div>
                </form>
            </div>
        </div>
        
        <div class="card card-style">
            <div class="content">
                <div id="list_result"></div>
            </div>
        </div>
    </div>
    <div id="content_input"></div>
    @section('custom_js')
        <script>
            load_list(1);
        </script>
    @endsection
</x-mobile-office-layout>