<x-mobile-office-layout title="Selamat datang">
    <div class="page-title page-title-small">
        <h2>
            {{-- <a href="#" data-back-button><i class="fa fa-arrow-left"></i></a> --}}
            Welcome to {{config('app.name')}}
        </h2>
        <a href="#" data-menu="" class="bg-fade-highlight-light shadow-xl preload-img" data-src="{{asset('img/icon.png')}}"></a>
    </div>
    <div class="card header-card shape-rounded" data-card-height="150">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{asset('img/icon.png')}}"></div>
    </div>
    
    <div class="card card-style" style="margin-top:30%;">
        <div id="login_page">
            <div class="content mt-2 mb-0">
                <form id="form_login">
                    <div class="input-style no-borders has-icon validate-field mb-4">
                        <i class="fa fa-envelope"></i>
                        <input type="email" class="form-control validate-email" name="email" id="email_login" placeholder="Your email" data-login="1">
                        <label for="email_login" class="color-blue-dark font-10 mt-1">Email</label>
                        <i class="fa fa-times disabled invalid color-red-dark"></i>
                        <i class="fa fa-check disabled valid color-green-dark"></i>
                        <em>(required)</em>
                    </div>
                    
                    <div class="input-style no-borders has-icon validate-field mb-4">
                        <i class="fa fa-lock"></i>
                        <input type="password" class="form-control validate-password" name="password" placeholder="Password" data-login="2">
                        <label for="password" class="color-blue-dark font-10 mt-1">Password</label>
                        <i class="fa fa-times disabled invalid color-red-dark"></i>
                        <i class="fa fa-check disabled valid color-green-dark"></i>
                        <em>(required)</em>
                    </div>
    
                    <a href="#" onclick="handle_post('#tombol_login','#form_login','{{route('office.auth.login')}}','POST');" class="btn btn-m btn-full mb-3 rounded-xl text-uppercase font-900 shadow-s bg-blue-dark">Login</a>
                </form>
                <div class="divider"></div>
    
                <a href="#" class="btn btn-icon btn-m btn-full rounded-xl shadow-l bg-google text-uppercase font-900 text-start"><i class="fab fa-google text-center"></i>Login with Google</a>
    
                <div class="divider mt-4 mb-3"></div>
    
                <div class="d-flex">
                    <div class="w-50 font-11 pb-2 color-theme opacity-60 pb-3 text-start"><a href="#" onclick="auth_content('register_page');" class="color-theme">Create Account</a></div>
                    <div class="w-50 font-11 pb-2 color-theme opacity-60 pb-3 text-end"><a href="#" onclick="auth_content('forgot_page');" class="color-theme">Forgot Credentials</a></div>
                </div>
            </div>
        </div>
        <div id="register_page">
            <div class="content mt-2 mb-0">
                <form id="form_register">
                    <a href="#" class="btn btn-icon btn-m btn-full rounded-xl shadow-l mt-3 bg-google text-uppercase font-900 text-start"><i class="fab fa-google text-center"></i>Login with Google</a>
                    <div class="divider"></div>
                    <div class="input-style no-borders has-icon validate-field mb-4">
                        <i class="fa fa-user"></i>
                        <input type="text" class="form-control validate-name" name="name" id="name_register" placeholder="Your Name" data-register="1">
                        <label for="name_register" class="color-blue-dark font-10 mt-1">Name</label>
                        <i class="fa fa-times disabled invalid color-red-dark"></i>
                        <i class="fa fa-check disabled valid color-green-dark"></i>
                        <em>(required)</em>
                    </div>
                    <div class="input-style no-borders has-icon validate-field mb-4">
                        <i class="fa fa-phone"></i>
                        <input type="tel" class="form-control validate-email" name="phone" id="phone_register" placeholder="Your phone" data-register="2">
                        <label for="phone_register" class="color-blue-dark font-10 mt-1">Phone</label>
                        <i class="fa fa-times disabled invalid color-red-dark"></i>
                        <i class="fa fa-check disabled valid color-green-dark"></i>
                        <em>(required)</em>
                    </div>
                    <div class="input-style no-borders has-icon validate-field mb-4">
                        <i class="fa fa-envelope"></i>
                        <input type="email" class="form-control validate-email" name="email" id="email_register" placeholder="Your email" data-register="3">
                        <label for="email_register" class="color-blue-dark font-10 mt-1">Email</label>
                        <i class="fa fa-times disabled invalid color-red-dark"></i>
                        <i class="fa fa-check disabled valid color-green-dark"></i>
                        <em>(required)</em>
                    </div>
                    
                    <div class="input-style no-borders has-icon validate-field mb-4">
                        <i class="fa fa-lock"></i>
                        <input type="password" class="form-control validate-password" name="password" placeholder="Password" data-login="4">
                        <label for="password" class="color-blue-dark font-10 mt-1">Password</label>
                        <i class="fa fa-times disabled invalid color-red-dark"></i>
                        <i class="fa fa-check disabled valid color-green-dark"></i>
                        <em>(required)</em>
                    </div>
                    <div class="input-style no-borders has-icon validate-field mb-4">
                        <i class="fa fa-lock"></i>
                        <input type="password" class="form-control validate-password" name="password_confirmation" placeholder="Password" data-login="5">
                        <label for="password" class="color-blue-dark font-10 mt-1">Re-enter Password</label>
                        <i class="fa fa-times disabled invalid color-red-dark"></i>
                        <i class="fa fa-check disabled valid color-green-dark"></i>
                        <em>(required)</em>
                    </div>
    
                    <a href="#" onclick="handle_post('#tombol_register','#form_register','{{route('office.auth.register')}}','POST');" class="btn btn-m mt-2 mb-4 btn-full rounded-xl bg-green-dark rounded-sm text-uppercase font-900">Register</a>
                </form>
                <div class="divider mt-4 mb-3"></div>
    
                <div class="d-flex">
                    <div class="w-50 font-11 pb-2 color-theme opacity-60 pb-3 text-start"><a href="#" class="color-theme">Already have an account ?</a></div>
                    <div class="w-50 font-11 pb-2 color-theme opacity-60 pb-3 text-end"><a href="#" onclick="auth_content('login_page');" class="color-theme">Sign in here</a></div>
                </div>
            </div>
        </div>
        <div id="forgot_page">
            <div class="content mt-2 mb-0">
                <form id="form_forgot">
                    <div class="input-style no-borders has-icon validate-field mb-4">
                        <i class="fa fa-envelope"></i>
                        <input type="email" class="form-control validate-email" name="email" id="email_forgot" placeholder="Your email" data-forgot="1">
                        <label for="email_forgot" class="color-blue-dark font-10 mt-1">Email</label>
                        <i class="fa fa-times disabled invalid color-red-dark"></i>
                        <i class="fa fa-check disabled valid color-green-dark"></i>
                        <em>(required)</em>
                    </div>
                    <a href="#" onclick="handle_post('#tombol_forgot','#form_forgot','{{route('office.auth.forgot')}}','POST');" class="btn btn-m mt-2 mb-4 btn-full rounded-xl bg-green-dark rounded-sm text-uppercase font-900">Submit</a>
                    <a href="#" onclick="auth_content('login_page');" class="btn btn-border btn-m btn-full mb-3 rounded-xl text-uppercase font-900 border-blue-dark color-blue-dark bg-theme">Cancel</a>
                </form>
            </div>
        </div>
    </div>
    @section('custom_js')
        <script>
            format_email('email_login');
            number_only('phone_register');
            auth_content('login_page');
        </script>
    @endsection
</x-mobile-office-layout>