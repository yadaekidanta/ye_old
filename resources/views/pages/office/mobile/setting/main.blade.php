<x-mobile-office-layout title="Selamat datang">
    <div class="page-title page-title-small">
        <h2><a href="#" data-back-button><i class="fa fa-arrow-left"></i></a>My Profile</h2>
    </div>
    <div class="card header-card shape-rounded" data-card-height="150">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{asset('img/icon.png')}}"></div>
    </div>
    
    <div class="card card-style">
        <div class="d-flex content mb-1">
            <!-- left side of profile -->
            <div class="flex-grow-1">
                <h1 class="font-700">{{Auth::guard('office')->user()->name}}<i class="fa fa-check-circle color-blue-dark float-end font-13 mt-2 me-3"></i></h1>
                <p class="mb-2">
                    {{Auth::guard('office')->user()->jobdesc}}
                </p>
                <p class="font-10">
                    <strong class="color-theme pe-1">1k</strong>Followers
                    <strong class="color-theme ps-3 pe-1">342</strong>Following 
                </p>
            </div>
            <!-- right side of profile. increase image width to increase column size-->
            <img src="{{asset('azures/images/empty.png')}}" data-src="{{Auth::guard('office')->user()->image}}" width="115" class="bg-highlight rounded-circle mt-3 shadow-xl preload-img">
        </div>
        <!-- follow buttons-->
        <div class="content">
            <div class="row mb-0">
                <div class="col-6">
                    <a href="#" class="btn btn-full btn-sm rounded-s text-uppercase font-900 bg-blue-dark">Edit Profile</a>
                </div>
                <div class="col-6">
                    <a href="#" class="btn btn-full btn-sm btn-border rounded-s text-uppercase font-900 color-highlight border-blue-dark">Edit Apa</a>
                </div>
            </div>
        </div>
        <div class="divider d-none mb-3 mt-1"></div>
        <div class="p-3 d-none">
            <div class="row text-center row-cols-3 mb-n4">
                <a class="col mb-4" data-gallery="gallery-1" href="images/pictures/30t.jpg" title="Vynil and Typerwritter">
                    <img data-src="images/pictures/30s.jpg" class="img-fluid rounded-xs preload-img" alt="img">
                </a>
                <a class="col mb-4" data-gallery="gallery-1" href="images/pictures/22t.jpg" title="Cream Cookie">
                    <img data-src="images/pictures/22s.jpg" class="img-fluid rounded-xs preload-img" alt="img">
                </a>
                <a class="col mb-4" data-gallery="gallery-1" href="images/pictures/23t.jpg" title="Cookies and Flowers">
                    <img data-src="images/pictures/23s.jpg" class="img-fluid rounded-xs preload-img" alt="img">
                </a>
                <a class="col mb-4" data-gallery="gallery-1" href="images/pictures/24t.jpg" title="Pots and Pans">
                    <img data-src="images/pictures/24s.jpg" class="img-fluid rounded-xs preload-img" alt="img">
                </a>
                <a class="col mb-4" data-gallery="gallery-1" href="images/pictures/25t.jpg" title="Berries are Packed with Fiber">
                    <img data-src="images/pictures/25s.jpg" class="img-fluid rounded-xs preload-img" alt="img">
                </a>
                <a class="col mb-4" data-gallery="gallery-1" href="images/pictures/26t.jpg" title="A beautiful Retro Camera">
                    <img data-src="images/pictures/26s.jpg" class="img-fluid rounded-xs preload-img" alt="img">
                </a>
                <a class="col mb-4" data-gallery="gallery-1" href="images/pictures/27t.jpg" title="Pots and Pans">
                    <img data-src="images/pictures/27s.jpg" class="img-fluid rounded-xs preload-img" alt="img">
                </a>
                <a class="col mb-4" data-gallery="gallery-1" href="images/pictures/28t.jpg" title="Berries are Packed with Fiber">
                    <img data-src="images/pictures/28s.jpg" class="img-fluid rounded-xs preload-img" alt="img">
                </a>
                <a class="col mb-4" data-gallery="gallery-1" href="images/pictures/31t.jpg" title="A beautiful Retro Camera">
                    <img data-src="images/pictures/31s.jpg" class="img-fluid rounded-xs preload-img" alt="img">
                </a>
            </div>
        </div>
    </div>
    <div class="card card-style">
        <div class="content mt-0 mb-2">
            <div class="list-group list-custom-large mb-4">
                <a href="#">
                    <i class="fa font-14 fa-wallet bg-pink-dark rounded-sm"></i>
                    <span>Bank Accounts</span>
                    <strong>Manage your bank accounts</strong>
                    <i class="fa fa-angle-right me-2"></i>
                </a>
                <a href="#" data-toggle-theme onclick="checkDarkMode();" class="show-on-theme-light">
                    <i class="fa font-14 fa-moon bg-brown-dark rounded-sm"></i>
                    <span>Dark Mode</span>
                    <strong>Auto Dark Mode Available Too</strong>
                    <i class="fa fa-angle-right me-2"></i>
                </a>
                <a href="#" data-toggle-theme onclick="checkDarkMode();" data-toggle-theme class="show-on-theme-dark">
                    <i class="fa font-14 fa-sun bg-yellow-dark rounded-sm"></i>
                    <span>Light Mode</span>
                    <strong>Auto Dark Mode Available Too</strong>
                    <i class="fa fa-angle-right me-2"></i>
                </a>     
                <a href="#" data-menu="menu-highlights">
                    <i class="fa font-14 fa-brush bg-highlight color-white rounded-sm"></i>
                    <span>Color Scheme</span>
                    <strong>A tone of Colors, Just for You</strong>
                    <i class="fa fa-angle-right me-2"></i>
                </a>     
                <a href="#" data-menu="menu-share">
                    <i class="fa font-14 fa-share-alt bg-red-dark rounded-sm"></i>
                    <span>Share {{config('app.name')}}</span>
                    <strong>Just one tap! We'll do the rest!</strong>
                    <i class="fa fa-angle-right me-2"></i>
                </a>     
                <a href="#" data-menu="menu-language">
                    <i class="fa font-14 fa-globe bg-green-dark rounded-sm"></i>
                    <span>Language Picker</span>
                    <strong>A Sample for Demo Purposes</strong>
                    <i class="fa fa-angle-right me-2"></i>
                </a>
            </div>
            
            <h5>Did you know?</h5>
            <p>
                Fast loading, great support, premium quality. We have a tone of awesome features, that make us stand out from our competitors.
            </p>
            <div class="divider mb-1"></div>
            <div class="list-group list-custom-large">
                <a href="{{route('office.auth.logout')}}" data-menu="menu-tips-1" class="border-0">
                    <i class="fa font-14 fa-door-open bg-magenta-light rounded-sm"></i>
                    <span>Logout</span>
                    <strong>A few Tips About Azures</strong>
                    <i class="fa fa-angle-right me-2"></i>
                </a>     
            </div>
        </div>  
    </div>
</x-mobile-office-layout>