<x-web-layout title="{{__('menu.service')}}">
    <section class="wrapper image-wrapper bg-image bg-overlay text-white" data-image-src="{{asset('sandbox/img/photos/bg1.jpg')}}">
        <div class="container pt-19 pt-md-21 pb-18 pb-md-20 text-center">
            <div class="row">
                <div class="col-md-10 col-lg-8 col-xl-7 col-xxl-6 mx-auto">
                    <h1 class="display-1 text-white mb-3">YE CAPACITIES</h1>
                    <p class="lead fs-lg px-md-3 px-lg-7 px-xl-9 px-xxl-10">Project, Agile, Managed Services</p>
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-light">
        <div class="container pt-14 pb-12 pt-md-16 pb-md-14">
            <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
                <div class="col-lg-5">
                    <h2 class="fs-15 text-uppercase text-muted mb-3">YE Services</h2>
                    <h3 class="display-4 mb-5">Services That We Offer</h3>
                    <p style="text-align: justify;">
                        From IT strategy consulting and comprehensive technology roadmaps to the end-to-end development of scalable solutions, Intellectsoft delivers a full-cycle software development services that adapt seamlessly to your project requirements and business needs.
                    </p>
                    <a href="#" class="btn btn-navy rounded-pill mt-3 d-none">More Details</a>
                </div>
                <div class="col-lg-7 order-lg-2">
                    <div class="row gx-md-5 gy-5">
                        <div class="col-md-6">
                            <div class="card bg-pale-purple">
                                <div class="card-body">
                                    <img src="{{asset('sandbox/img/icons/lineal/profits.svg')}}" class="svg-inject icon-svg icon-svg-md text-yellow mb-3" alt="" />
                                    <h4 style="line-height: 1.3;font-weight: bold;">Agile Development</h4>
                                    <h6 class="card-text" style="line-height: 1.3;text-align: justify;text-justify:distribute-all-lines">
                                        Software development methodologies centered round the idea of iterative
                                        development
                                    </h6>
                                    <p class="mb-0" style="line-height: 1.3;text-align: justify;text-justify:distribute-all-lines">
                                        On Extended Team Service, customer get the help from our experts as
                                        additional resource and has free reign over the project management with flexible spec.
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                        <div class="col-md-6">
                            <div class="card bg-pale-orange">
                                <div class="card-body">
                                    <img src="{{asset('sandbox/img/icons/lineal/touch-screen.svg')}}" class="svg-inject icon-svg icon-svg-md text-red mb-3" alt="" />
                                    <h4 style="line-height: 1.3; font-weight: bold;">Project Based</h4>
                                    <h6 class="card-text" style="line-height: 1.3;text-align: justify;text-justify:distribute-all-lines">
                                        With YE fixed-budget delivery,
                                        client does not pay for delays
                                    </h6>
                                    <p class="mb-0" style="line-height: 1.3;text-align: justify;text-justify:distribute-all-lines">
                                        Consult  build all kind of custom made apps. For your company's
                                        promotional, operational, and human resource needs.
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                        <div class="col-md-6">
                            <div class="card bg-pale-violet">
                                <div class="card-body">
                                    <img src="{{asset('sandbox/img/icons/lineal/puzzle.svg')}}" class="svg-inject icon-svg icon-svg-md text-leaf mb-3" alt="" />
                                    <h4 style="line-height: 1.3;font-weight: bold;">Managed Services</h4>
                                    <h6 class="card-text" style="line-height: 1.3;text-align: justify;text-justify:distribute-all-lines">
                                        Our Developer will maintain your application professionally
                                    </h6>
                                    <p class="mb-0" style="line-height: 1.3;text-align: justify;text-justify:distribute-all-lines">
                                        Our team consists of experienced and tech-savvy developers. We can help you
                                        to maintain your application professionally
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                        <div class="col-md-6">
                            <div class="card bg-pale-sky">
                                <div class="card-body">
                                    <img src="{{asset('sandbox/img/icons/lineal/watercolor.svg')}}" class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt="" />
                                    <h4 style="line-height: 1.3;font-weight: bold;">Design Services</h4>
                                    <h6 class="card-text" style="line-height: 1.3;text-align: justify;text-justify:distribute-all-lines">
                                        Build product with a clear design process and delivers a spot-on end
                                        result
                                    </h6>
                                    <p class="mb-0" style="line-height: 1.3;text-align: justify;text-justify:distribute-all-lines">
                                        Our design team is design studio within a large software company that will
                                        help you build an engaging product easily and quickly.
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                        <div class="col-md-6">
                            <div class="card bg-pale-leaf">
                                <div class="card-body">
                                    <img src="{{asset('sandbox/img/icons/lineal/paper.svg')}}" class="svg-inject icon-svg icon-svg-md text-leaf mb-3" alt="" />
                                    <h4 style="line-height: 1.3;font-weight: bold;">Technical Writer</h4>
                                    <h6 class="card-text" style="line-height: 1.3;text-center: inter-word;">
                                        Improve your business or operation with software blueprint tailored for your company as a basis of system development
                                    </h6>
                                    <p class="mb-0" style="line-height: 1.3;text-align: justify;text-justify:distribute-all-lines">
                                        Improve the apps quality through professional testing services
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                        <div class="col-md-6">
                            <div class="card bg-pale-navy">
                                <div class="card-body">
                                    <img src="{{asset('sandbox/img/icons/lineal/check-list.svg')}}" class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt="" />
                                    <h4 style="line-height: 1.3;font-weight: bold;">Quality Assurance</h4>
                                    <h6 class="card-text" style="line-height: 1.3;text-align: justify;text-justify:distribute-all-lines">
                                        Improve the apps quality through professional testing services
                                    </h6>
                                    <p class="mb-0" style="line-height: 1.3;text-align: justify;text-justify:distribute-all-lines">
                                        Testing is the process to ensure applications or websites meet general or
                                        specific quality standards.
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                    </div>
                    <!--/.row -->
                </div>
                <!--/column -->
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <div class="overflow-hidden">
            <div class="divider text-soft-primary mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100">
                    <path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z" />
                </svg>
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-soft-primary">
        <div class="container py-14 pt-lg-0">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="display-4 mb-0 text-center mb-3 mt-12">How to get started with the Service</h3>
                    <h2 class="fs-15 text-muted px-xl-10 px-xxl-15">Our core competence is in web-based software and mobility. Complemented with additional expertise on A.I., Big Data, and IoT, we can help you succeed. You can engage us on a project-based or extended team manner to ensure fast innovation.</h2>
                    <div class="row gx-lg-8 gx-xl-12 process-wrapper mt-9">
                        <div class="col-md-6">
                            <div class="card bg-pale-light">
                                <div class="card-body">
                                    <img src="{{asset('img/ilustrasi/svg/future.svg')}}" class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt="" />
                                    <h4 class="text-left" style="line-height: 1.3;">1. Aware of your Goals</h4>
                                    <p class="mb-0" style="line-height: 1.3;text-align: justify;text-justify:distribute-all-lines">
                                        We are eager to hear your story, what's keeping you up at night, your ideas and vision, and how we can help. We will dig the requirements.
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                        <div class="col-md-6 d-none d-md-block">
                            <img class="img-fluid" src="{{asset('sandbox/img/illustrations/i6.png')}}" srcset="{{asset('sandbox/img/illustrations/i6@2x.png 2x')}}" alt="" />
                        </div>
                        <div class="col-md-6 d-none d-md-block">
                            <img class="img-fluid" src="{{asset('sandbox/img/illustrations/i1.png')}}" srcset="{{asset('sandbox/img/illustrations/i1@2x.png 2x')}}" alt="" />
                        </div>
                        <div class="col-md-6 mt-5">
                            <div class="card bg-pale-light">
                                <div class="card-body">
                                    <img src="{{asset('img/ilustrasi/svg/startup.svg')}}" class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt="" />
                                    <h4 style="line-height: 1.3;">2. Expect a Proposal from Us</h4>
                                    <p class="mb-0" style="line-height: 1.3;text-align: justify;">
                                        For development request, once the scope of work is understood by both parties, we will estimate the effort and provide you with a solution proposal with a fixed budget and deadline. Or, if you prefer, we can do agile enhancement service with more flexibility in the scope of work.
                                        If you're looking for a managed service support for your application, even if it's built by another software house we can help you too. Once we inquire further and get a clearer picture of your application, with the prerequisite preparations completed we will send the service proposal too.
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                        <div class="col-md-6 mt-5">
                            <div class="card bg-pale-light">
                                <div class="card-body">
                                    <img src="{{asset('img/ilustrasi/svg/personal.svg')}}" class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt="" />
                                    <h4 style="line-height: 1.3;">3. Enter a formal Contract</h4>
                                    <p class="mb-0" style="line-height: 1.3;text-align: justify;">
                                        Once you conceptually agree, we sign a contract. Always know you are in good hands. We have been here for 1 years and still growing quickly, thus you can be rest assured we can support you long-term.
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                        <div class="col-md-6 d-none d-md-block">
                            <img class="img-fluid" src="{{asset('sandbox/img/illustrations/i5.png')}}" srcset="{{asset('sandbox/img/illustrations/i5@2x.png 2x')}}" alt="" />
                        </div>
                        <div class="col-md-6 d-none d-md-block">
                            <img class="img-fluid" src="{{asset('sandbox/img/illustrations/i3.png')}}" srcset="{{asset('sandbox/img/illustrations/i3@2x.png 2x')}}" alt="" />
                        </div>
                        <div class="col-md-6 mt-3">
                            <div class="card bg-pale-light">
                                <div class="card-body">
                                    <img src="{{asset('img/ilustrasi/svg/business.svg')}}" class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt="" />
                                    <h4 style="line-height: 1.3;">4. Implementation Starts</h4>
                                    <p class="mb-0" style="line-height: 1.3;text-align: justify;">
                                        On project-based service by default YE will perform a pre-development analysis, consistent progress demo, professional change management practice, testing based on a thorough test plan document - including security and performance tests, and guide you through UAT. Ultimately, our proven processes mitigate risks of delay and remove risks of project failure. You can get all of that too with the agile enhancement, if you request for a complete team.
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                    </div>
                    <!--/.row -->
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
            <div class="row text-center d-none">
                <div class="col-lg-10 mx-auto">
                    <div class="position-relative">
                        <div class="shape bg-dot red rellax w-16 h-18" data-rellax-speed="1" style="top: 1rem; left: -3.9rem;"></div>
                        <div class="shape rounded-circle bg-line primary rellax w-18 h-18" data-rellax-speed="1" style="bottom: 2rem; right: -3rem;"></div>
                        <video poster="{{asset('sandbox/img/photos/movie.jpg')}}" class="player" playsinline controls preload="none">
                            <source src="{{asset('sandbox/media/movie.mp4')}}" type="video/mp4">
                            <source src="{{asset('sandbox/media/movie.webm')}}" type="video/webm">
                        </video>
                    </div>
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <div class="overflow-hidden">
            <div class="divider text-light mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100">
                    <path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z" />
                </svg>
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-light">
        <div class="container py-14 py-md-16">
            <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center" style="margin-bottom:-10%;">
                <div class="col-lg-7 order-lg-2" style="margin-top:-10%;">
                    <figure><img class="w-auto" src="{{asset('img/ilustrasi/team_relationship_model.png')}}" srcset="{{asset('img/ilustrasi/team_relationship_model.png')}}" alt="" /></figure>
                </div>
                <!--/column -->
                <div class="col-lg-5" style="margin-top:-10%;">
                    <h3 class="display-4 mb-5">TEAM RELATIONSHIP MODEL</h3>
                    <p class="mb-6" style="text-align: justify;">Business continuity guaranteed. Project knowledge is owned by at least two people. Couple that with our scalable talent platform, your project will be safer with us.</p>
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <div class="overflow-hidden">
            <div class="divider text-navy mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100">
                    <path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z" />
                </svg>
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
</x-web-layout>