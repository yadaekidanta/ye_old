@if($collection->count() > 0)
    @foreach($collection as $item)
    <div class="job-list mb-10">
        <h3 class="mb-4">{{$item->department->name}}</h3>
        @foreach(\App\Models\HRM\VacancyJob::where('department_id',$item->department_id)->where('st','Open')->get() as $vacancy)
        @php
        $name = Str::of($vacancy->title)->explode(' ');
        $singkatan = '';
        foreach($name as $kata)
        {
        $singkatan .= substr($kata, 0, 1);
        }
        @endphp
        <a href="{{route('web.career.show',$vacancy->slug)}}" class="card mb-4 lift">
            <div class="card-body p-5">
                <span class="row justify-content-between align-items-center">
                    <span class="col-md-5 mb-2 mb-md-0 d-flex align-items-center text-body">
                        <span class="avatar bg-red text-white w-9 h-9 fs-17 me-3">{{$singkatan}}</span> {{$vacancy->title}} </span>
                        <span class="col-5 col-md-3 text-body d-flex align-items-center">
                            <i class="uil uil-clock me-1"></i> Full time
                        </span>
                        <span class="col-7 col-md-4 col-lg-3 text-body d-flex align-items-center">
                            <i class="uil uil-location-arrow me-1"></i> San Francisco, US
                        </span>
                        <span class="d-none d-lg-block col-1 text-center text-body">
                            <i class="uil uil-angle-right-b"></i>
                        </span>
                    </span>
                </span>
            </div>
            <!-- /.card-body -->
        </a>
        @endforeach
        <!-- /.card -->
    </div>
    @endforeach
@else
<div class="job-list mb-10">
    <h3 class="mb-4 d-none">We're not open position yet on this department</h3>
    <a href="javascript:;" class="card mb-4 lift">
        <div class="card-body p-5">
            <span class="row justify-content-between align-items-center">
                <span class="col-md-5 mb-2 mb-md-0 d-flex align-items-center text-body">
                    <span class="text-white w-9 h-9 fs-17 me-3"></span>We're not open position yet on this department</span>
            </span>
        </div>
        <!-- /.card-body -->
    </a>
</div>
@endif