<x-web-layout title="{{__('menu.career')}} - {{$data->title}}">
    <section class="wrapper bg-soft-primary">
        <div class="container pt-10 pb-19 pt-md-14 pb-md-20 text-center">
            <div class="row">
                <div class="col-md-10 col-xl-8 mx-auto">
                    <div class="post-header">
                        <h1 class="display-1 mb-5">{{$data->position->name}}</h1>
                        <ul class="post-meta fs-17 mb-5">
                            {{-- <li><i class="uil uil-clock"></i> Full time</li> --}}
                            {{-- <li><i class="uil uil-location-arrow"></i> Manchester, UK</li> --}}
                            <li><i class="uil uil-building"></i> {{$data->department->name}}</li>
                        </ul>
                        <!-- /.post-meta -->
                    </div>
                    <!-- /.post-header -->
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <section class="wrapper bg-light">
        <div class="container pb-14 pb-md-16">
            <div class="row">
                <div class="col-lg-10 mx-auto">
                    <div class="blog single mt-n17">
                        <div class="card shadow-lg">
                            <div class="card-body">
                                <h2 class="h1 mb-3">Job Description</h2>
                                {!!$data->description!!}
                                <h3 class="h2 mb-3 mt-9">Facilities</h3>
                                {!!$data->facilities!!}
                                <!--/.row -->
                                <h3 class="h2 mb-3 mt-9">Requirements</h3>
                                {!!$data->requirement!!}
                                <!--/.row -->
                                <a target="_blank" href="{{route('office.auth.index')}}" class="btn btn-primary rounded-pill">Apply Now</a>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.blog -->
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    @if(\App\Models\HRM\VacancyJob::where('id','!=',$data->id)->where('st','Open')->get()->count() > 0)
    <section class="wrapper bg-soft-primary">
        <div class="container py-14 py-md-16">
            <div class="row align-items-center mb-6">
                <div class="col-md-8 col-lg-9 col-xl-8 col-xxl-7 pe-xl-20">
                    <h2 class="display-6 mb-0">More Job Openings</h2>
                </div>
                <!--/column -->
                <div class="col-md-4 col-lg-3 ms-md-auto text-md-end mt-5 mt-md-0 d-none">
                    <a href="#" class="btn btn-primary rounded-pill mb-0">Explore Positions</a>
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
            <div class="row gy-6">
                @foreach (\App\Models\HRM\VacancyJob::where('id','!=',$data->id)->where('st','Open')->get() as $item)
                @php
                $name = Str::of($item->title)->explode(' ');
                $singkatan = '';
                foreach($name as $kata)
                {
                $singkatan .= substr($kata, 0, 1);
                }
                @endphp
                <div class="col-md-6 col-lg-4">
                    <a href="{{route('web.career.show',$item->slug)}}" class="card shadow-lg lift h-100">
                        <div class="card-body p-5 d-flex flex-row">
                            <div>
                                <span class="avatar bg-red text-white w-11 h-11 fs-20 me-4">{{$singkatan}}</span>
                            </div>
                            <div>
                                <span class="badge bg-pale-blue text-blue rounded py-1 mb-2 d-none">{{$item->department->name}}</span>
                                <h4 class="mb-1">{{$item->title}}</h4>
                                <p class="mb-0 text-body">{!!$item->description!!}</p>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
    </section>
    @endif
</x-web-layout>