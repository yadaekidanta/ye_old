<x-web-layout title="{{__('menu.about')}}">
    <section class="wrapper bg-soft-primary">
        <div class="container pt-10 pb-20 pt-md-14 pb-md-23 text-center">
            <div class="row">
                <div class="col-xl-5 mx-auto mb-6">
                    <h1 class="display-1 mb-3">About Us</h1>
                    <p class="lead mb-0">YE is an Enterprise software solutions company</p>
                </div>
                <!-- /column -->
            </div>
          <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-light">
        <div class="container pb-14 pb-md-16">
            <div class="row text-center mb-12 mb-md-15">
                <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2 mt-n18 mt-md-n22">
                    <figure><img class="w-auto" src="{{asset('sandbox/img/illustrations/i8.png')}}" srcset="./assets/img/illustrations/i8@2x.png 2x" alt="" /></figure>
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
            <div class="row gx-lg-8 gx-xl-12 gy-6 align-items-center">
                <div class="col-lg-6 order-lg-2">
                    <div class="row gx-md-5 gy-5">
                        <div class="col-md-4 offset-md-2 align-self-end">
                            <figure class="rounded"><img src="{{asset('sandbox/img/photos/g1.jpg')}}" srcset="{{asset('sandbox/img/photos/g1@2x.jpg 2x')}}" alt=""></figure>
                        </div>
                        <!--/column -->
                        <div class="col-md-6 align-self-end">
                            <figure class="rounded"><img src="{{asset('sandbox/img/photos/g2.jpg')}}" srcset="{{asset('sandbox/img/photos/g2@2x.jpg 2x')}}" alt=""></figure>
                        </div>
                        <!--/column -->
                        <div class="col-md-6 offset-md-1">
                            <figure class="rounded"><img src="{{asset('sandbox/img/photos/g3.jpg')}}" srcset="{{asset('sandbox/img/photos/g3@2x.jpg 2x')}}" alt=""></figure>
                        </div>
                        <!--/column -->
                        <div class="col-md-4 align-self-start">
                            <figure class="rounded"><img src="{{asset('sandbox/img/photos/g4.jpg')}}" srcset="{{asset('sandbox/img/photos/g4@2x.jpg 2x')}}" alt=""></figure>
                        </div>
                        <!--/column -->
                    </div>
                    <!--/.row -->
                </div>
                <!--/column -->
                <div class="col-lg-6">
                    <h3 class="display-5 mb-5">Great solutions doesn’t need to be complicated</h3>
                    <p>Yada Ekidanta(YE) is an Enterprise software solution and system integrator company, with delivery team in multiple sites in Indonesia. Our mission is to strengthen client's IT capacity and capabilities, automate their business processes; allowing them to innovate through technology.</p>
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
        <div class="overflow-hidden">
            <div class="divider text-gray mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100">
                    <path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z" />
                </svg>
            </div>
        </div>
    </section>
    <!-- /section -->
    <section class="wrapper bg-gray">
        <div class="container py-14 py-md-16">
            <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
                <div class="col-lg-7">
                    <figure><img class="w-auto" src="{{asset('img/ilustrasi/expertise.svg')}}" srcset="{{asset('img/ilustrasi/expertise.svg')}}" alt="" /></figure>
                </div>
                <!--/column -->
                <div class="col-lg-5">
                    <h3 class="display-5 mb-7 pe-xxl-5">Our Expertise</h3>
                    <div class="d-flex flex-row mb-4">
                        <div>
                            <img src="{{asset('sandbox/img/icons/lineal/check-list.svg')}}" class="svg-inject icon-svg icon-svg-sm text-blue me-4" alt="" />
                        </div>
                        <div>
                            <h4 class="mb-1">Waterfall</h4>
                            <p class="mb-1">Project-based where we scope out the requirements in detail before we start the project.</p>
                        </div>
                    </div>
                    <div class="d-flex flex-row mb-4">
                        <div>
                            <img src="{{asset('sandbox/img/icons/lineal/certificate.svg')}}" class="svg-inject icon-svg icon-svg-sm text-green me-4" alt="" />
                        </div>
                        <div>
                            <h4 class="mb-1">Agile</h4>
                            <p class="mb-1">on extended team service, customer get the help from our experts as additional resource and has free reign over the project management with flexible spec.</p>
                        </div>
                    </div>
                    <div class="d-flex flex-row">
                        <div>
                            <img src="{{asset('sandbox/img/icons/lineal/team.svg')}}" class="svg-inject icon-svg icon-svg-sm text-yellow me-4" alt="" />
                        </div>
                        <div>
                            <h4 class="mb-1">Support</h4>
                            <p class="mb-0">Our team consists of experienced and tech-savvy developers. We can help you to maintain your application professionally.</p>
                        </div>
                    </div>
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <div class="overflow-hidden">
            <div class="divider text-light mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100">
                    <path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z" />
                </svg>
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-light">
        <div class="container py-14 py-md-16">
            <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
                <div class="col-lg-4">
                    <h2 class="fs-15 text-uppercase text-line text-primary text-center mb-3">YE Management</h2>
                    <p>We have been standing for 1 years and will continue to keep growing for the next 15 or more years!</p>
                    <a href="#" class="btn btn-primary rounded-pill mt-3">See All Members</a>
                </div>
                <!--/column -->
                <div class="col-lg-8">
                    <div class="swiper-container text-center mb-6" data-margin="30" data-dots="true" data-items-xl="3" data-items-md="2" data-items-xs="1">
                        <div class="swiper">
                            <div class="swiper-wrapper">
                                @foreach($employee as $item)
                                <div class="swiper-slide">
                                    <img class="rounded-circle w-20 mx-auto mb-4" src="{{$item->image}}" srcset="{{$item->image}}" alt="" />
                                    <h4 class="mb-1">{{$item->name}}</h4>
                                    <div class="meta mb-2">{{$item->position->name}} {{$item->id == 3 ? '& COO' : ''}}</div>
                                    <p class="mb-2" style="line-height: 1.3;text-align: justify;text-justify: inter-word;">{{$item->jobdesc}}</p>
                                    <nav class="nav social justify-content-center text-center mb-0 d-none">
                                        <a href="#"><i class="uil uil-twitter"></i></a>
                                        <a href="#"><i class="uil uil-slack"></i></a>
                                        <a href="#"><i class="uil uil-linkedin"></i></a>
                                    </nav>
                                    <!-- /.social -->
                                </div>
                                <!--/.swiper-slide -->
                                @endforeach
                            </div>
                            <!--/.swiper-wrapper -->
                        </div>
                        <!-- /.swiper -->
                    </div>
                    <!-- /.swiper-container -->
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <div class="overflow-hidden">
            <div class="divider text-gray mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100">
                    <path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z" />
                </svg>
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-gray">
        <div class="container pt-14 pb-12 pt-md-16 pb-md-14">
            <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
                <div class="col-lg-12 text-center">
                    <h3 class="display-4">Our Core Value</h3>
                    <p>
                        What we believe and how do we run our business
                    </p>
                </div>
                <div class="col-lg-12 order-lg-2">
                    <div class="row gx-md-5 gy-5">
                        <div class="col-md-3">
                            <div class="card bg-pale-pink">
                                <div class="card-body">
                                    <img src="{{asset('sandbox/img/icons/lineal/profits.svg')}}" class="svg-inject icon-svg icon-svg-md text-yellow mb-3" alt="" />
                                    <h4 style="line-height: 1.3;">Do The Right Things</h4>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                        <div class="col-md-3">
                            <div class="card bg-pale-leaf">
                                <div class="card-body">
                                    <img src="{{asset('sandbox/img/icons/lineal/touch-screen.svg')}}" class="svg-inject icon-svg icon-svg-md text-red mb-3" alt="" />
                                    <h4 style="line-height: 1.3;">Empathy</h4>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                        <div class="col-md-3">
                            <div class="card bg-pale-violet">
                                <div class="card-body">
                                    <img src="{{asset('sandbox/img/icons/lineal/puzzle-2.svg')}}" class="svg-inject icon-svg icon-svg-md text-leaf mb-3" alt="" />
                                    <h4 style="line-height: 1.3;">Challenge</h4>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                        <div class="col-md-3">
                            <div class="card bg-pale-violet">
                                <div class="card-body">
                                    <img src="{{asset('sandbox/img/icons/lineal/paper.svg')}}" class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt="" />
                                    <h4 style="line-height: 1.3;">Learning</h4>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                        <div class="col-md-3">
                            <div class="card bg-pale-red">
                                <div class="card-body">
                                    <img src="{{asset('sandbox/img/icons/lineal/award-2.svg')}}" class="svg-inject icon-svg icon-svg-md text-leaf mb-3" alt="" />
                                    <h4 style="line-height: 1.3;">Optimism</h4>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                        <div class="col-md-3">
                            <div class="card bg-pale-orange">
                                <div class="card-body">
                                    <img src="{{asset('sandbox/img/icons/lineal/medal.svg')}}" class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt="" />
                                    <h4 style="line-height: 1.3;">Perseverance</h4>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                        <div class="col-md-3">
                            <div class="card bg-pale-yellow">
                                <div class="card-body">
                                    <img src="{{asset('sandbox/img/icons/lineal/balance.svg')}}" class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt="" />
                                    <h4 style="line-height: 1.3;">Balance & Enjoyment</h4>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                        <div class="col-md-3">
                            <div class="card bg-pale-blue">
                                <div class="card-body">
                                    <img src="{{asset('sandbox/img/icons/lineal/handshake.svg')}}" class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt="" />
                                    <h4 style="line-height: 1.3;">Respect & Appreciation</h4>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                    </div>
                    <!--/.row -->
                </div>
                <!--/column -->
                
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <div class="overflow-hidden">
            <div class="divider text-light mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100">
                    <path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z" />
                </svg>
            </div>
        </div>
        <!-- /.container -->
    </section>
    <section class="wrapper bg-light">
        <div class="container pt-14 pb-12 pt-md-16 pb-md-14">
            <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
                <div class="col-lg-12 text-center">
                    <h3 class="display-4">Our Culture</h3>
                    <p>
                        Our values continuously inspire us to improve and to boost an open dynamic and down-to-earth company culture.
                    </p>
                </div>
                <div class="col-lg-12 order-lg-2">
                    <div class="row gx-md-5 gy-5">
                        <div class="col-md-4">
                            <div class="card bg-pale-leaf">
                                <div class="card-body">
                                    <img src="{{asset('sandbox/img/icons/lineal/clock-2.svg')}}" class="svg-inject icon-svg icon-svg-md text-yellow mb-3" alt="" />
                                    <h4 style="line-height: 1.3;">Discipline</h4>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                        <div class="col-md-4">
                            <div class="card bg-pale-fuchsia">
                                <div class="card-body">
                                    <img src="{{asset('sandbox/img/icons/lineal/search.svg')}}" class="svg-inject icon-svg icon-svg-md text-red mb-3" alt="" />
                                    <h4 style="line-height: 1.3;">Passion To Learn</h4>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                        <!--/column -->
                        <div class="col-md-4">
                            <div class="card bg-pale-sky">
                                <div class="card-body">
                                    <img src="{{asset('sandbox/img/icons/lineal/smartphone-2.svg')}}" class="svg-inject icon-svg icon-svg-md text-leaf mb-3" alt="" />
                                    <h4 style="line-height: 1.3;">Excellence</h4>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.card -->
                        </div>
                    </div>
                    <!--/.row -->
                </div>
                <!--/column -->
                
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <div class="overflow-hidden">
            <div class="divider text-gray mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100">
                    <path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z" />
                </svg>
            </div>
        </div>
        <!-- /.container -->
    </section>
    <section class="wrapper bg-gray">
        <div class="container py-14 py-md-16">
            <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
                <div class="col-lg-7">
                    <figure><img class="w-auto" src="{{asset('sandbox/img/illustrations/i5.png')}}" srcset="./assets/img/illustrations/i5@2x.png 2x" alt="" /></figure>
                </div>
                <!--/column -->
                <div class="col-lg-5">
                    <h2 class="fs-15 text-uppercase text-line text-primary text-center mb-3">Get In Touch</h2>
                    <h3 class="display-5 mb-7">Got any questions? Don't hesitate to get in touch.</h3>
                    <div class="d-flex flex-row">
                        <div>
                            <div class="icon text-primary fs-28 me-4 mt-n1"> <i class="uil uil-location-pin-alt"></i> </div>
                        </div>
                        <div>
                            <h5 class="mb-1">Address</h5>
                            <address>Permata Buah Batu Blok C 15B, Bandung, Indonesia</address>
                        </div>
                    </div>
                    <div class="d-flex flex-row">
                        <div>
                            <div class="icon text-primary fs-28 me-4 mt-n1"> <i class="uil uil-phone-volume"></i> </div>
                        </div>
                        <div>
                            <h5 class="mb-1">Phone</h5>
                            <p>(62) 877 4800 5611</p>
                        </div>
                    </div>
                    <div class="d-flex flex-row">
                        <div>
                            <div class="icon text-primary fs-28 me-4 mt-n1"> <i class="uil uil-envelope"></i> </div>
                        </div>
                        <div>
                            <h5 class="mb-1">E-mail</h5>
                            <p class="mb-0"><a href="mailto:hello@yadaekidanta.com" class="link-body"><span class="__cf_email__" data-cfemail="4132202f25232e3901242c20282d6f222e2c">hello@yadaekidanta.com</span></a></p>
                        </div>
                    </div>
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <div class="overflow-hidden">
            <div class="divider text-navy mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100">
                    <path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z" />
                </svg>
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
</x-web-layout>