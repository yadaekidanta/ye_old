<x-web-layout title="{{__('menu.home')}}">
    <section class="wrapper bg-light">
        <div class="container pt-10 pt-md-14 pb-14 pb-md-16 text-center">
            <div class="row gx-lg-8 gx-xl-12 gy-10 gy-xl-0 mb-14 align-items-center">
                <div class="col-lg-7 order-lg-2">
                    <div class="row gx-md-5 gy-5">
                        <div class="col-md-4 offset-md-2 align-self-end">
                            <figure class="rounded"><img src="{{asset('sandbox/img/photos/g5.jpg')}}" srcset="{{asset('sandbox/img/photos/g5@2x.jpg 2x')}}" alt=""></figure>
                        </div>
                        <!--/column -->
                        <div class="col-md-6 align-self-end">
                            <figure class="rounded"><img src="{{asset('sandbox/img/photos/g6.jpg')}}" srcset="{{asset('sandbox/img/photos/g6@2x.jpg 2x')}}" alt=""></figure>
                        </div>
                        <!--/column -->
                        <div class="col-md-6 offset-md-1">
                            <figure class="rounded"><img src="{{asset('sandbox/img/photos/g7.jpg')}}" srcset="{{asset('sandbox/img/photos/g7@2x.jpg 2x')}}" alt=""></figure>
                        </div>
                        <!--/column -->
                        <div class="col-md-4 align-self-start">
                            <figure class="rounded"><img src="{{asset('sandbox/img/photos/g8.jpg')}}" srcset="{{asset('sandbox/img/photos/g8@2x.jpg 2x')}}" alt=""></figure>
                        </div>
                        <!--/column -->
                    </div>
                    <!--/.row -->
                </div>
                <!-- /column -->
                <div class="col-md-10 offset-md-1 offset-lg-0 col-lg-5 text-center text-lg-start">
                    <h1 class="display-1 fs-30 mb-5 mx-md-n5 mx-lg-0 mt-7">
                        Enterprise Digital Transformation Consultant & Integrator Company In Indonesia
                        <br class="d-md-none">
                        <span class="rotator-fade text-primary">
                            Agile Development, Project Based, Managed Services, Design Services, Technical Writer, Quality Assurance
                        </span>
                    </h1>
                    <p class="lead fs-lg mb-7" style="line-height: 1.3;">
                        YE is one of the Enteprise Digital Transfromation Consultant & Integrator company in Indonesia, specializing in software solutions and application delivery & managed services. Our mission is to enable enterprises to ride the wave of digital era with tech-enabled innovation & business process automation.
                    </p>
                    <span><a href="{{route('web.about')}}" class="btn btn-lg btn-primary rounded-pill me-2">Find out more about YE</a></span>
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
            <p class="text-center mb-8">As seen on</p>
            <div class="row row-cols-4 row-cols-md-4 row-cols-lg-7 row-cols-xl-7 gy-10 mb-2 d-flex align-items-center justify-content-center">
                <div class="col"><img class="img-fluid px-md-3 px-lg-0 px-xl-2 px-xxl-5" src="{{asset('sandbox/img/brands/c1.png')}}" alt="" /></div>
                <div class="col"><img class="img-fluid px-md-3 px-lg-0 px-xl-2 px-xxl-5" src="{{asset('sandbox/img/brands/c2.png')}}" alt="" /></div>
                <div class="col"><img class="img-fluid px-md-3 px-lg-0 px-xl-2 px-xxl-5" src="{{asset('sandbox/img/brands/c3.png')}}" alt="" /></div>
                <div class="col"><img class="img-fluid px-md-3 px-lg-0 px-xl-2 px-xxl-5" src="{{asset('sandbox/img/brands/c4.png')}}" alt="" /></div>
                <div class="col"><img class="img-fluid px-md-3 px-lg-0 px-xl-2 px-xxl-5" src="{{asset('sandbox/img/brands/c5.png')}}" alt="" /></div>
                <div class="col"><img class="img-fluid px-md-3 px-lg-0 px-xl-2 px-xxl-5" src="{{asset('sandbox/img/brands/c6.png')}}" alt="" /></div>
                <div class="col"><img class="img-fluid px-md-3 px-lg-0 px-xl-2 px-xxl-5" src="{{asset('sandbox/img/brands/c7.png')}}" alt="" /></div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
        <div class="overflow-hidden">
            <div class="divider text-soft-primary mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100">
                    <path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z" />
                </svg>
            </div>
        </div>
        <!-- /.overflow-hidden -->
    </section>
    <!-- /section -->
    @if($projects->count() > 0)
    <section class="wrapper bg-gradient-primary">
        <div class="container pt-12 pt-lg-8 pb-14 pb-md-17">
            <div class="row text-center">
                <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2">
                    <h2 class="fs-16 text-uppercase text-primary mb-3">CAPACITIES</h2>
                    <h3 class="display-3 px-xxl-10">Project, Agile and Managed Services</h3>
                    <p>
                        YE delivers a combination of IT consultation, custom software development, and IT resource outsourcing services to large enterprises for their digital transformation journey. Our solutions cut across various industries and our expertise covered many technologies.
                    </p>
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
            <div class="swiper-container grid-view nav-bottom nav-dark mb-14" data-margin="30" data-dots="false" data-nav="true" data-items-md="2" data-items-xs="1">
                <div class="swiper overflow-visible">
                    <div class="swiper-wrapper">
                        @foreach($projects as $p)
                        <div class="swiper-slide">
                            <figure class="rounded mb-7">
                                <a href="{{route('web.show_case',$p->slug)}}">
                                    <img src="{{$p->image}}" srcset="./assets/img/photos/sp1@2x.jpg 2x" alt="" />
                                </a>
                            </figure>
                            <div class="project-details d-flex justify-content-center flex-column">
                                <div class="post-header">
                                    <h2 class="post-title h3"><a href="{{route('web.show_case',$p->slug)}}" class="link-dark">{{$p->client->company_name}}</a></h2>
                                    <div class="post-category text-ash">{{$p->title}}</div>
                                </div>
                                <!-- /.post-header -->
                            </div>
                            <!-- /.project-details -->
                        </div>
                        @endforeach
                        <!--/.swiper-slide -->
                    </div>
                    <!--/.swiper-wrapper -->
                </div>
            </div>
        </div>
        <!-- /.container -->
    </section>
    @endif
    <!-- /section -->
    <section class="wrapper bg-gradient-reverse-primary">
        <div class="container pb-14 pb-md-16">
            <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
                <div class="col-lg-7">
                    <figure><img class="img-auto" src="{{asset('sandbox/img/illustrations/i22.png')}}" srcset="./assets/img/illustrations/i22@2x.png 2x" alt="" /></figure>
                </div>
                <!--/column -->
                <div class="col-lg-5">
                    <h2 class="fs-15 text-uppercase text-primary mb-3">CAPACITIES</h2>
                    <h3 class="display-3 mb-7">Digital Transformation Solution</h3>
                    <div class="accordion accordion-wrapper" id="accordionExample">
                        <div class="card plain accordion-item">
                            <div class="card-header" id="agile">
                                <button class="accordion-button" data-bs-toggle="collapse" data-bs-target="#agileCollapse" aria-expanded="true" aria-controls="agileCollapse">Agile Development</button>
                            </div>
                            <!--/.card-header -->
                            <div id="agileCollapse" class="accordion-collapse collapse show" aria-labelledby="agile" data-bs-parent="#accordionExample">
                                <div class="card-body">
                                    <p>
                                        On Extended Team Service, customer get the help from our experts as additional resource and has free reign over the project management with flexible spec.
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.accordion-collapse -->
                        </div>
                        <!--/.accordion-item -->
                        <div class="card plain accordion-item">
                            <div class="card-header" id="project">
                                <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#projectCollapse" aria-expanded="false" aria-controls="projectCollapse">Project Based</button>
                            </div>
                            <!--/.card-header -->
                            <div id="projectCollapse" class="accordion-collapse collapse" aria-labelledby="project" data-bs-parent="#accordionExample">
                                <div class="card-body">
                                    <p>
                                        Consult build all kind of custom made apps. For your company's promotional, operational, and human resource needs.
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.accordion-collapse -->
                        </div>
                        <!--/.accordion-item -->
                        <div class="card plain accordion-item">
                            <div class="card-header" id="managed">
                                <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#managedCollapse" aria-expanded="false" aria-controls="managedCollapse">Managed Services</button>
                            </div>
                            <!--/.card-header -->
                            <div id="managedCollapse" class="accordion-collapse collapse" aria-labelledby="managed" data-bs-parent="#accordionExample">
                                <div class="card-body">
                                    <p>
                                        Our team consists of experienced and tech-savvy developers. We can help you to maintain your application professionally.
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.accordion-collapse -->
                        </div>
                        <div class="card plain accordion-item">
                            <div class="card-header" id="design">
                                <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#designCollapse" aria-expanded="false" aria-controls="designCollapse">Design Services</button>
                            </div>
                            <!--/.card-header -->
                            <div id="designCollapse" class="accordion-collapse collapse" aria-labelledby="design" data-bs-parent="#accordionExample">
                                <div class="card-body">
                                    <p>
                                        Our design team is design studio within a large software company that will help you build an engaging product easily and quickly.
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.accordion-collapse -->
                        </div>
                        <div class="card plain accordion-item">
                            <div class="card-header" id="technical">
                                <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#technicalCollapse" aria-expanded="false" aria-controls="technicalCollapse">Technical Writter</button>
                            </div>
                            <!--/.card-header -->
                            <div id="technicalCollapse" class="accordion-collapse collapse" aria-labelledby="technical" data-bs-parent="#accordionExample">
                                <div class="card-body">
                                    <p>
                                        Map your business process according to the specifications and requirements needed in the application.
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.accordion-collapse -->
                        </div>
                        <!--/.accordion-item -->
                        <div class="card plain accordion-item">
                            <div class="card-header" id="qa">
                                <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#qaCollapse" aria-expanded="false" aria-controls="qaCollapse">Quality Assurance</button>
                            </div>
                            <!--/.card-header -->
                            <div id="qaCollapse" class="accordion-collapse collapse" aria-labelledby="qa" data-bs-parent="#accordionExample">
                                <div class="card-body">
                                    <p>
                                        Testing is the process to ensure applications or websites meet general or specific quality standards
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.accordion-collapse -->
                        </div>
                        <!--/.accordion-item -->
                    </div>
                    <!--/.accordion -->
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
        <div class="overflow-hidden">
            <div class="divider text-light mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100">
                    <path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z" />
                </svg>
            </div>
        </div>
        <!-- /.overflow-hidden -->
    </section>
    <!-- /section -->
    
    <section class="wrapper bg-light">
        <div class="container pt-6 pb-14 pb-md-16">
            <div class="row gx-lg-8 gx-xl-12 gy-10">
                <div class="col-lg-6 mb-0">
                    <h2 class="fs-16 text-uppercase text-primary mb-4">FAQ</h2>
                    <h3 class="display-3 mb-4">If you don't see an answer to your question, you can send us an email from our contact form.</h3>
                    <p class="mb-6">Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Nullam quis risus eget urna mollis ornare.</p>
                    <a href="#" class="btn btn-primary rounded-pill">All FAQ</a>
                </div>
                <!--/column -->
                <div class="col-lg-6">
                    <div id="accordion-3" class="accordion-wrapper">
                        <div class="card accordion-item shadow-lg">
                            <div class="card-header" id="accordion-heading-3-1">
                                <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#accordion-collapse-3-1" aria-expanded="false" aria-controls="accordion-collapse-3-1">How do I get my subscription receipt?</button>
                            </div>
                            <!-- /.card-header -->
                            <div id="accordion-collapse-3-1" class="collapse" aria-labelledby="accordion-heading-3-1" data-bs-target="#accordion-3">
                                <div class="card-body">
                                    <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cras mattis consectetur purus sit amet fermentum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sed odio dui. Cras justo odio, dapibus ac facilisis.</p>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.collapse -->
                        </div>
                        <!-- /.card -->
                        <div class="card accordion-item shadow-lg">
                            <div class="card-header" id="accordion-heading-3-2">
                                <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#accordion-collapse-3-2" aria-expanded="false" aria-controls="accordion-collapse-3-2">Are there any discounts for people in need?</button>
                            </div>
                            <!-- /.card-header -->
                            <div id="accordion-collapse-3-2" class="collapse" aria-labelledby="accordion-heading-3-2" data-bs-target="#accordion-3">
                                <div class="card-body">
                                    <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cras mattis consectetur purus sit amet fermentum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sed odio dui. Cras justo odio, dapibus ac facilisis.</p>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.collapse -->
                        </div>
                        <!-- /.card -->
                        <div class="card accordion-item shadow-lg">
                            <div class="card-header" id="accordion-heading-3-3">
                                <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#accordion-collapse-3-3" aria-expanded="false" aria-controls="accordion-collapse-3-3">Do you offer a free trial edit?</button>
                            </div>
                            <!-- /.card-header -->
                            <div id="accordion-collapse-3-3" class="collapse" aria-labelledby="accordion-heading-3-3" data-bs-target="#accordion-3">
                                <div class="card-body">
                                    <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cras mattis consectetur purus sit amet fermentum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sed odio dui. Cras justo odio, dapibus ac facilisis.</p>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.collapse -->
                        </div>
                        <!-- /.card -->
                        <div class="card accordion-item shadow-lg">
                            <div class="card-header" id="accordion-heading-3-4">
                                <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#accordion-collapse-3-4" aria-expanded="false" aria-controls="accordion-collapse-3-4">How do I reset my Account password?</button>
                            </div>
                            <!-- /.card-header -->
                            <div id="accordion-collapse-3-4" class="collapse" aria-labelledby="accordion-heading-3-4" data-bs-target="#accordion-3">
                                <div class="card-body">
                                    <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cras mattis consectetur purus sit amet fermentum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sed odio dui. Cras justo odio, dapibus ac facilisis.</p>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.collapse -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.accordion-wrapper -->
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
        <div class="overflow-hidden">
            <div class="divider text-navy mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100">
                    <path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z" />
                </svg>
            </div>
        </div>
        <!-- /.overflow-hidden -->
    </section>
</x-web-layout>