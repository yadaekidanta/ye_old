<x-web-layout title="{{__('menu.contact')}}">
    @section('custom_css')
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.js'></script>
	<link href='https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.css' rel='stylesheet' />
    <style>
        .mapboxgl-ctrl-bottom-left, .mapboxgl-ctrl-bottom-right{
            display:none;
        }
		.marker {
			display: block;
			border: none;
			border-radius: 50%;
			cursor: pointer;
			padding: 0;
		}
		.mapboxgl-popup {
			max-width: 400px;
			font: 12px/20px 'Helvetica Neue', Arial, Helvetica, sans-serif;
		}
	</style>
    @endsection
    <section class="wrapper image-wrapper bg-image bg-overlay bg-overlay-400 text-white" data-image-src="{{asset('sandbox/img/photos/bg3.jpg')}}">
        <div class="container pt-17 pb-20 pt-md-19 pb-md-21 text-center">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <h1 class="display-1 mb-3 text-white">Get in Touch</h1>
                    <nav class="d-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb text-white">
                            <li class="breadcrumb-item"><a href="{{route('web.home')}}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Contact</li>
                        </ol>
                    </nav>
                    <!-- /nav -->
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-light angled upper-end">
        <div class="container pb-11">
            <div class="row mb-14 mb-md-16">
                <div class="col-xl-10 mx-auto mt-n19">
                    <div class="card">
                        <div class="row gx-0">
                            <div class="col-lg-6 align-self-stretch">
                                <div class="map map-full rounded-top rounded-lg-start">
                                    <div id='map' class="gmap h-100"></div>
                                </div>
                                <!-- /.map -->
                            </div>
                            <!--/column -->
                            <div class="col-lg-6">
                                <div class="p-10 p-md-11 p-lg-14">
                                    <div class="d-flex flex-row">
                                        <div>
                                            <div class="icon text-primary fs-28 me-4 mt-n1"> <i class="uil uil-location-pin-alt"></i> </div>
                                        </div>
                                        <div class="align-self-start justify-content-start">
                                            <h5 class="mb-1">Address</h5>
                                            <address>Permata Buah Batu Blok C 15B<br class="d-none d-md-block" />Bandung, Indonesia</address>
                                        </div>
                                    </div>
                                    <!--/div -->
                                    <div class="d-flex flex-row">
                                        <div>
                                            <div class="icon text-primary fs-28 me-4 mt-n1"> <i class="uil uil-phone-volume"></i> </div>
                                        </div>
                                        <div>
                                            <h5 class="mb-1">Phone</h5>
                                            <p>(62) 877 4800 5611</p>
                                        </div>
                                    </div>
                                    <!--/div -->
                                    <div class="d-flex flex-row">
                                        <div>
                                            <div class="icon text-primary fs-28 me-4 mt-n1"> <i class="uil uil-envelope"></i> </div>
                                        </div>
                                        <div>
                                            <h5 class="mb-1">E-mail</h5>
                                            <p class="mb-0"><a href="mailto:hello@yadaekidanta.com" class="link-body"><span class="__cf_email__" data-cfemail="4132202f25232e3901242c20282d6f222e2c">hello@yadaekidanta.com</span></a></p>
                                        </div>
                                    </div>
                                    <!--/div -->
                                </div>
                                <!--/div -->
                            </div>
                            <!--/column -->
                        </div>
                        <!--/.row -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
                    <h2 class="display-4 mb-3 text-center">Drop Us a Line</h2>
                    <p class="lead text-center mb-10">Reach out to us from our contact form and we will get back to you shortly.</p>
                    <form class="contact-form needs-validation" id="form_contact">
                        <div class="messages"></div>
                        <div class="row gx-4">
                            <div class="col-md-6">
                                <div class="form-floating mb-4">
                                    <input id="form_name" type="text" name="First_Name" class="form-control" placeholder="Jane" required>
                                    <label for="form_name">First Name *</label>
                                    <div class="valid-feedback"> Looks good! </div>
                                    <div class="invalid-feedback"> Please enter your first name. </div>
                                </div>
                            </div>
                            <!-- /column -->
                            <div class="col-md-6">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="Last_Name" class="form-control" placeholder="Doe" required>
                                    <label for="form_lastname">Last Name *</label>
                                    <div class="valid-feedback"> Looks good! </div>
                                    <div class="invalid-feedback"> Please enter your last name. </div>
                                </div>
                            </div>
                            <!-- /column -->
                            <div class="col-md-6">
                                <div class="form-floating mb-4">
                                    <input id="form_email" type="email" name="Email" class="form-control" placeholder="jane.doe@example.com" required>
                                    <label for="form_email">Email *</label>
                                    <div class="valid-feedback"> Looks good! </div>
                                    <div class="invalid-feedback"> Please provide a valid email address. </div>
                                </div>
                            </div>
                            <!-- /column -->
                            <div class="col-md-6">
                                <div class="form-select-wrapper mb-4">
                                    <select class="form-select" id="form-select" name="Department">
                                        <option selected disabled value="">Select a department</option>
                                        @foreach ($department as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="valid-feedback"> Looks good! </div>
                                    <div class="invalid-feedback"> Please select a department. </div>
                                </div>
                            </div>
                            <!-- /column -->
                            <div class="col-12">
                                <div class="form-floating mb-4">
                                    <textarea id="form_message" name="Messages" class="form-control" placeholder="Your message" style="height: 150px" required></textarea>
                                    <label for="form_message">Message *</label>
                                    <div class="valid-feedback"> Looks good! </div>
                                    <div class="invalid-feedback"> Please enter your messsage. </div>
                                </div>
                            </div>
                            <!-- /column -->
                            <div class="col-12 text-center">
                                <button id="tombol_kontak" class="btn btn-primary rounded-pill btn-send mb-3" onclick="handle_save('#tombol_kontak','#form_contact','{{route('web.send_messages')}}','POST');">Send message</button>
                                <p class="text-muted"><strong>*</strong> These fields are required.</p>
                            </div>
                            <!-- /column -->
                        </div>
                        <!-- /.row -->
                    </form>
                    <!-- /form -->
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper image-wrapper bg-auto no-overlay bg-image text-center bg-map d-none" data-image-src="{{asset('sandbox/img/map.png')}}">
        <div class="container pt-0 pb-14 pt-md-16 pb-md-18">
            <div class="row">
                <div class="col-lg-9 col-xxl-8 mx-auto">
                    <h3 class="display-4 mb-8 px-xl-12">We are trusted by over 5000+ clients. Join them now and grow your business.</h3>
                </div>
                <!-- /.row -->
            </div>
            <!-- /column -->
            <div class="row">
                <div class="col-md-10 col-lg-9 col-xl-7 mx-auto">
                    <div class="row align-items-center counter-wrapper gy-4 gy-md-0">
                        <div class="col-md-4 text-center">
                            <h3 class="counter counter-lg text-primary">7518</h3>
                            <p>Completed Projects</p>
                        </div>
                        <!--/column -->
                        <div class="col-md-4 text-center">
                            <h3 class="counter counter-lg text-primary">5472</h3>
                            <p>Satisfied Customers</p>
                        </div>
                        <!--/column -->
                        <div class="col-md-4 text-center">
                            <h3 class="counter counter-lg text-primary">2184</h3>
                            <p>Expert Employees</p>
                        </div>
                        <!--/column -->
                    </div>
                    <!--/.row -->
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
        </div>
        <div class="overflow-hidden">
            <div class="divider text-navy mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100">
                    <path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z" />
                </svg>
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    @section('custom_js')
    <script>
        mapboxgl.accessToken = 'pk.eyJ1IjoieWFkYWVraWRhbnRhIiwiYSI6ImNrcXJpYm14eTFyaXYyb284eHhmaHJrcHgifQ.J0kCRmsw0sBIcs-TiD0SRg';
        var geojson = {
            'type': 'FeatureCollection',
            'features': [
                {
                    'type': 'Feature',
                    'properties': {
                        'iconSize': [60, 60]
                    },
                    'geometry': {
                        'type': 'Point',
                        'coordinates': [107.63865, -6.97383]
                    }
                }
            ]
        };
        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [107.60, -6.93],
            zoom: 11.15
        });
        geojson.features.forEach(function (marker) {
            // Create a DOM element for each marker.
            var el = document.createElement('div');
            el.className = 'marker';
            el.style.backgroundImage = "url({{asset('img/marker.png')}})";
            el.style.width = marker.properties.iconSize[0] + 'px';
            el.style.height = marker.properties.iconSize[1] + 'px';
            el.style.backgroundSize = '100%';
            new mapboxgl.Marker(el).setLngLat(marker.geometry.coordinates).addTo(map);
            $('.marker').click(function(){
                new mapboxgl.Popup(el).setLngLat(marker.geometry.coordinates).setHTML(marker.properties.description).addTo(map);
            });
        });
        map.on('click', function (e) {
            new mapboxgl.Popup()
            .setLngLat([107.63865, -6.97383])
            .setHTML('<h4>Hi, We are <span>Yada Ekidanta</span></h4><p style="margin-top:-30px;">Enterprise Digital Transformation Consultant & Integrator Company In Indonesia.</p>')
            .addTo(map);
        });
        // Change the cursor to a pointer when the mouse is over the places layer.
        // // Change it back to a pointer when it leaves.
        map.on('mouseleave', function () {
            map.getCanvas().style.cursor = '';
        });
    </script>
    @endsection
</x-web-layout>