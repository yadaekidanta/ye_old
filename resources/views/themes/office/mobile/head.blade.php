<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <title>
        {{config('app.name') ?? config('app.name')}}
    </title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" type="text/css" href="{{asset('azures/styles/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('azures/styles/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/toastr.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/swal.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('azures/fonts/css/fontawesome-all.min.css')}}">    
    <link rel="manifest" href="{{asset('manifest.json')}}" data-pwa-version="set_in_manifest_and_pwa_js">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/icon.png')}}">
    <style>
        .speach-icon button {
            padding-top: 0px !important;
            display: block;
            border-radius: 35px;
            width: 35px;
            height: 35px;
            line-height: 35px;
            margin-top: 3px;
            transform: translateY(10px);
        }
    </style>
</head>