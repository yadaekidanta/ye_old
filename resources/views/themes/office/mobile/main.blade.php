@php
$uri = request()->path();
@endphp
<!DOCTYPE HTML>
<html lang="en">
@include('themes.office.mobile.head')
    
<body class="theme-light" data-highlight="blue2">
    
<div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>
    
<div id="page">
    <!-- header and footer bar go here-->
    @auth
    @include('themes.office.mobile.header')
    @if(request()->is('office/chat/*'))
    @include('themes.office.mobile.chatting')
    @else
    @include('themes.office.mobile.menu')
    @endif
    @endauth
    
    <div class="page-content">
        {{$slot}}
        <!-- footer and footer card-->
        @if($uri == '*/chat')
        <div class="footer">
            @include('themes.office.mobile.footer')
        </div>
        @endif
    </div>    
    <!-- end of page content-->
    <div id="menu-language" class="menu menu-box-bottom menu-box-detached rounded-m" data-menu-height="340" data-menu-effect="menu-over">
        <div class="me-3 ms-3 mt-3">
            <h1 class="font-700 mb-0">Select your language</h1>
            <p class="font-11 mt-n2 mb-3">
                You can direct to multiple languages of your page.
            </p>            
            <div class="list-group list-custom-small">
                <a href="#"><img class="me-3 mt-n1" width="20" src="{{asset('azures/images/flags/United-States.png')}}"><span>English</span><i class="fa fa-angle-right"></i></a>
                <a href="#"><img class="me-3 mt-n1" width="20" src="{{asset('azures/images/flags/Indonesia.png')}}"><span>Indonesia</span><i class="fa fa-angle-right"></i></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    @include('themes.office.mobile.share')
    @include('themes.office.mobile.colors')
    @include('themes.office.mobile.aside')
    <div id="menu-install-pwa-android" class="menu menu-box-bottom menu-box-detached rounded-l" data-menu-height="350" data-menu-effect="menu-parallax">
        <div class="boxed-text-l mt-4">
            <img class="rounded-l mb-3" src="{{asset('img/icon.png')}}" alt="img" width="90">
            <h4 class="mt-3">{{config('app.name')}} on your Home Screen</h4>
            <p>
                Install {{config('app.name')}} on your home screen, and access it just like a regular app. It really is that simple!
            </p>
            <a href="#" class="pwa-install btn btn-s rounded-s shadow-l text-uppercase font-900 bg-highlight mb-2">Add to Home Screen</a><br>
            <a href="#" class="pwa-dismiss close-menu color-gray2-light text-uppercase font-900 opacity-60 font-10">Maybe later</a>
            <div class="clear"></div>
        </div>
    </div>   

    <!-- Install instructions for iOS -->
    <div id="menu-install-pwa-ios" class="menu menu-box-bottom menu-box-detached rounded-l" data-menu-height="320" data-menu-effect="menu-parallax">
        <div class="boxed-text-xl mt-4">
            <img class="rounded-l mb-3" src="{{asset('img/icon.png')}}" alt="img" width="90">
            <h4 class="mt-3">{{config('app.name')}} on your Home Screen</h4>
            <p class="mb-0 pb-3">
                Install {{config('app.name')}} on your home screen, and access it just like a regular app.  Open your Safari menu and tap "Add to Home Screen".
            </p>
            <div class="clear"></div>
            <a href="#" class="pwa-dismiss close-menu color-highlight font-800 opacity-80 text-center text-uppercase">Maybe later</a><br>
            <i class="fa-ios-arrow fa fa-caret-down font-40"></i>
        </div>
    </div>
    <div id="processing_toast" class="snackbar-toast bg-highlight color-white" data-bs-delay="1500" data-bs-autohide="true"><i class="fa fa-sync fa-spin me-3"></i>Processing...</div>
</div>
@include('themes.office.mobile.modal')
@include('themes.office.mobile.js')
@yield('custom_js')
</body>
</html>