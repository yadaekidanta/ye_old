<script type="text/javascript" src="{{asset('azures/scripts/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('azures/scripts/custom.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/toastr.js')}}"></script>
@guest
<script type="text/javascript" src="{{asset('js/auth.js')}}"></script>
@endguest
@auth
<script type="text/javascript" src="{{asset('js/swal.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugin.js')}}"></script>
<script type="text/javascript" src="{{asset('js/method.js')}}"></script>
@endauth
<script>
</script>