<div id="menu-language" class="menu menu-box-bottom menu-box-detached rounded-m" data-menu-height="340" data-menu-effect="menu-over">
    <div class="me-3 ms-3 mt-3">
        <h1 class="font-700 mb-0">Select your language</h1>
        <p class="font-11 mt-n2 mb-3">
            You can direct to multiple languages of your page.
        </p>            
        <div class="list-group list-custom-small">
            <a href="#"><img class="me-3 mt-n1" width="20" src="{{asset('azures/images/flags/United-States.png')}}"><span>English</span><i class="fa fa-angle-right"></i></a>
            <a href="#"><img class="me-3 mt-n1" width="20" src="{{asset('azures/images/flags/Indonesia.png')}}"><span>Indonesia</span><i class="fa fa-angle-right"></i></a>
        </div>
        <div class="clear"></div>
    </div>
</div>