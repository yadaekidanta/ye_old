@if ($paginator->hasPages())
<nav aria-label="pagination-demo">
    <ul class="pagination pagination- justify-content-center">
        @if ($paginator->onFirstPage())
        @else
        <li class="page-item">
            <a class="page-link rounded-xs color-black bg-transparent bg-theme shadow-xl border-0 paginasi" href="#" tabindex="-1" aria-disabled="true" halaman="{{ $paginator->previousPageUrl() }}"><i class="fa fa-angle-left"></i></a>
        </li>
        @endif
        @foreach ($elements as $element)
            @if (is_string($element))
                <li class="page-item"><a class="page-link rounded-xs color-black bg-theme shadow-l border-0" href="#">...</a></li>
            @endif
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active"><a class="page-link rounded-xs bg-highlight shadow-l border-0" href="#">{{ $page }}</a></li>
                    @else
                        <li class="page-item"><a class="page-link rounded-xs color-black bg-theme shadow-l border-0 paginasi" halaman="{{ $url }}" href="#">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach
        @if ($paginator->hasMorePages())
        <li class="page-item">
            <a class="page-link rounded-xs color-black bg-transparent bg-theme shadow-l border-0 paginasi" halaman="{{ $paginator->nextPageUrl() }}" href="#"><i class="fa fa-angle-right"></i></a>
        </li>
        @endif
    </ul>
</nav>
@endif