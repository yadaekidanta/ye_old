<form id="form_chat">
    <div id="footer-bar" class="d-flex">
        <input type="hidden" id="to_id_chat" name="to_id">
        <div class="me-3 speach-icon">
            <a href="#" data-menu="menu-upload" class="bg-gray-dark ms-2"><i class="fa fa-plus mt-2"></i></a>
        </div>
        <div class="flex-fill speach-input">
            <input type="text" id="messages_body" name="body" onkeyup="if(event.keyCode == 13){send_chat('#tombol_kirim_chat','#form_chat','{{route('office.chat.store')}}','POST')}" class="form-control" placeholder="Enter your Message here">
        </div>
        <div class="ms-3 speach-icon">
            <button id="tombol_kirim_chat" onclick="send_chat('#tombol_kirim_chat','#form_chat','{{route('office.chat.store')}}','POST');" class="bg-blue-dark me-2"><i class="fa fa-arrow-up mt-2"></i></button>
        </div>
    </div>
</form>