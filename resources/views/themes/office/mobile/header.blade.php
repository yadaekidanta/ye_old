<div class="header header-fixed header-auto-show header-logo-app">
    @if($uri == '*/dashboard')
    <a href="#" data-back-button class="header-title header-subtitle">Back to Pages</a>
    <a href="#" data-back-button class="header-icon header-icon-1"><i class="fas fa-arrow-left"></i></a>
    @else
    <a href="{{route('office.dashboard')}}" class="header-title">{{config('app.name')}}</a>
    <a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
    @endif
    <a href="{{route('office.chat.index')}}" class="header-icon header-icon-2 color-highlight"><i class="fas fa-comment-alt"></i><span class="badge bg-green-dark">3</span></a>
    <a href="#" class="header-icon header-icon-3 color-highlight"><i class="fas fa-bell"></i><span class="badge bg-green-dark">5</span></a>
</div>