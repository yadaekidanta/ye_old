<div id="footer-bar" class="footer-bar-5">
    <a href="{{route('office.dashboard')}}" class="{{request()->is('office/dashboard') ? 'active-nav' : ''}}">
        <i data-feather="home" data-feather-line="1" data-feather-size="21" data-feather-color="blue-dark" data-feather-bg="blue-fade-light"></i>
        <span>Dashboard</span>
    </a>
    @if(Auth::guard('office')->user()->position_id == 4)
    <a href="{{route('office.kpi.index')}}" class="{{request()->is('office/kpi') || request()->is('office/kpi/*') || request()->is('office/kpi-objective/*') ? 'active-nav' : ''}}">
        <i data-feather="users" data-feather-line="1" data-feather-size="21" data-feather-color="red-dark" data-feather-bg="red-fade-light"></i>
        <span>KPI</span>
    </a>
    @elseif(Auth::guard('office')->user()->position_id == 6)
    <a href="{{route('office.employee.index')}}" class="{{request()->is('office/employee') ? 'active-nav' : ''}}">
        <i data-feather="users" data-feather-line="1" data-feather-size="21" data-feather-color="red-dark" data-feather-bg="red-fade-light"></i>
        <span>Employee</span>
    </a>
    @elseif(Auth::guard('office')->user()->position_id == 13)
    @elseif(Auth::guard('office')->user()->position_id == 14)
    @elseif(Auth::guard('office')->user()->position_id == 17)
    <a href="index-media.html" class="{{request()->is('office/job') ? 'active-nav' : ''}}">
        <i data-feather="briefcase" data-feather-line="1" data-feather-size="21" data-feather-color="green-dark" data-feather-bg="green-fade-light"></i>
        <span>Employees</span>
    </a>
    <a href="index-media.html" class="{{request()->is('office/job') ? 'active-nav' : ''}}">
        <i data-feather="briefcase" data-feather-line="1" data-feather-size="21" data-feather-color="green-dark" data-feather-bg="green-fade-light"></i>
        <span>Jobs</span>
    </a>
    @elseif(Auth::guard('office')->user()->position_id == 63)
    @endif
    <a href="#" onclick="handle_open_modal('{{route('office.memo.index')}}','#customModal','#contentModal','GET');" class="{{request()->is('office/memos') ? 'active-nav' : ''}}">
        <i data-feather="file" data-feather-line="1" data-feather-size="21" data-feather-color="brown-dark" data-feather-bg="brown-fade-light"></i>
        <span>Memos</span>
    </a>
    <a href="{{route('office.setting')}}" class="{{request()->is('office/setting') ? 'active-nav' : ''}}">
        <i data-feather="settings" data-feather-line="1" data-feather-size="21" data-feather-color="dark-dark" data-feather-bg="gray-fade-light"></i>
        <span>Settings</span>
    </a>
    <a href="index-settings.html" class="{{request()->is('office/chat') ? 'active-nav' : ''}} d-none">
        <i data-feather="settings" data-feather-line="1" data-feather-size="21" data-feather-color="dark-dark" data-feather-bg="gray-fade-light"></i>
        <span>Chat</span>
    </a>
    <a href="index-settings.html" class="{{request()->is('office/notification') ? 'active-nav' : ''}} d-none">
        <i data-feather="settings" data-feather-line="1" data-feather-size="21" data-feather-color="dark-dark" data-feather-bg="gray-fade-light"></i>
        <span>Notification</span>
    </a>
</div>