<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="{{asset('keenthemes/plugins/global/plugins.bundle.js')}}"></script>
<script src="{{asset('keenthemes/js/scripts.bundle.js')}}"></script>
<script src="{{ asset('keenthemes//plugins/custom/fullcalendar/fullcalendar.bundle.js') }}"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Page Custom Javascript(used by this page)-->
@guest
<script src="{{asset('js/auth.js')}}"></script>
@endguest
<!--end::Page Custom Javascript-->
@auth
<script>
    localStorage.setItem("route_counter_notif", "{{route('office.counter_notif')}}");
    localStorage.setItem("route_notification", "{{route('office.notification')}}");
    localStorage.setItem("route_counter_chat", "{{route('office.counter_chat')}}");
</script>
<script src="{{asset('js/notif.js')}}"></script>
<script src="{{asset('js/plugin-office.js')}}"></script>
<script src="{{asset('js/method.js')}}"></script>
<script>
    var today = new Date();
    var curHr = today.getHours();
    if (curHr < 11) {
        $("#title_greet").html("{{__('custom.morning')}},");
    } else if (curHr >= 11 && curHr <= 15) {
        $("#title_greet").html("{{__('custom.afternoon')}},");
    }else if (curHr >= 15 && curHr <= 19) {
        $("#title_greet").html("{{__('custom.afternoons')}},");
    } else {
        $("#title_greet").html("{{__('custom.evening')}},");
    }
</script>
@endauth