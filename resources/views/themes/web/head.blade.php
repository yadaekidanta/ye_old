<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="keywords" content="bootstrap 5, business, corporate, creative, gulp, marketing, minimal, modern, multipurpose, one page, responsive, saas, sass, seo, startup, html5 template, site template">
	<meta name="author" content="elemis">
	<title>
        {{config('app.name') . ': ' .$title ?? config('app.name')}}
    </title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
	<link rel="shortcut icon" href="{{asset('img/icon.png')}}">
	<link rel="stylesheet" href="{{asset('sandbox/css/plugins.css')}}">
	<link rel="stylesheet" href="{{asset('sandbox/css/style.css')}}">
	<link rel="stylesheet" href="{{asset('sandbox/css/colors/sky.css')}}">
	<link rel="preload" href="{{asset('sandbox/css/fonts/urbanist.css')}}" as="style" onload="this.rel='stylesheet'">
	<link rel="stylesheet" href="{{asset('css/toastr.css')}}">
	@yield('custom_css')
</head>