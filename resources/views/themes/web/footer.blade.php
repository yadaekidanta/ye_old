<footer class="bg-navy text-inverse">
    <div class="container pt-12 pt-lg-6 pb-13 pb-md-15">
        {{-- <div class="d-lg-flex flex-row align-items-lg-center" style="display:none;">
            <h3 class="display-3 mb-6 mb-lg-0 pe-lg-20 pe-xl-22 pe-xxl-25 text-white">Join our community by using our services and grow your business.</h3>
            <a href="#" class="btn btn-primary rounded-pill mb-0 text-nowrap">Try It For Free</a>
        </div>
        <!--/div -->
        <hr class="mt-11 mb-12" /> --}}
        <div class="row gy-6 gy-lg-0">
            <div class="col-md-4 col-lg-3">
                <div class="widget">
                    <img class="mb-4" src="{{asset('img/icon.png')}}" style="width:10%;" srcset="./assets/img/logo-light@2x.png 2x" alt="" />
                    <p class="mb-4">© 2021 Yada Ekidanta. <br class="d-none d-lg-block" />All rights reserved.</p>
                    <nav class="nav social social-white">
                        <a href="https://twitter.com/YadaEkidanta" target="_blank"><i class="uil uil-twitter"></i></a>
                        <a href="https://facebook.com/YadaEkidanta" target="_blank"><i class="uil uil-facebook-f"></i></a>
                        <a href="https://linkedin.com/company/yada-ekidanta" target="_blank"><i class="uil uil-linkedin"></i></a>
                        <a href="https://instagram.com/yadaekidanta" target="_blank"><i class="uil uil-instagram"></i></a>
                    </nav>
                    <!-- /.social -->
                </div>
                <!-- /.widget -->
            </div>
            <!-- /column -->
            <div class="col-md-4 col-lg-3">
                <div class="widget">
                    <h4 class="widget-title text-white mb-3">Get in Touch</h4>
                    <address class="pe-xl-15 pe-xxl-17">Permata Buah Batu Blok C 15B, Bandung, Indonesia</address>
                    <a href="mailto:hello@yadaekidanta.com">hello@yadaekidanta.com</a><br /> (62) 877 4800 5611
                </div>
                <!-- /.widget -->
            </div>
            <!-- /column -->
            <div class="col-md-4 col-lg-3">
                <div class="widget">
                    <h4 class="widget-title text-white mb-3">Learn More</h4>
                    <ul class="list-unstyled  mb-0">
                        <li><a href="{{route('web.career.index')}}">Career</a></li>
                        <li><a href="{{route('web.terms-use')}}">Terms of Use</a></li>
                        <li><a href="{{route('web.privacy-policy')}}">Privacy Policy</a></li>
                    </ul>
                </div>
                <!-- /.widget -->
            </div>
            <!-- /column -->
            <div class="col-md-12 col-lg-3">
                <div class="widget">
                    <h4 class="widget-title text-white mb-3">Our Newsletter</h4>
                    <p class="mb-5">Subscribe to our newsletter to get our news & deals delivered to you.</p>
                    <div class="newsletter-wrapper">
                        <!-- Begin Mailchimp Signup Form -->
                        <div id="mc_embed_signup2">
                            <form id="form_sc" class="validate dark-fields">
                                <div id="mc_embed_signup_scroll2">
                                    <div class="mc-field-group input-group form-floating">
                                        <input type="email" name="email" class="required email form-control" placeholder="Email Address" id="sc_email">
                                        <label for="mce-EMAIL2">Email Address</label>
                                        <button onclick="handle_save('#tombol_sc','#form_sc','{{route('web.subscribe')}}','POST');" id="tombol_sc" class="btn btn-primary ">Subscribe</button>
                                    </div>
                                    <div id="mce-responses2" class="clear">
                                        <div class="response" id="mce-error-response2" style="display:none"></div>
                                        <div class="response" id="mce-success-response2" style="display:none"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--End mc_embed_signup-->
                    </div>
                    <!-- /.newsletter-wrapper -->
                </div>
                <!-- /.widget -->
            </div>
            <!-- /column -->
        </div>
        <!--/.row -->
    </div>
    <!-- /.container -->
</footer>