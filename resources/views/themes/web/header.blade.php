<header class="wrapper bg-light">
    <nav class="navbar navbar-expand-lg center-nav transparent navbar-light">
        <div class="container flex-lg-row flex-nowrap align-items-center">
            <div class="navbar-brand w-100">
                <a href="{{route('web.home')}}">
                    <img src="{{asset('img/icon.png')}}" style="width:10%;" srcset="{{asset('img/icon.png')}}" alt="" />
                </a>
            </div>
            <div class="navbar-collapse offcanvas offcanvas-nav offcanvas-start">
                <div class="offcanvas-header d-lg-none">
                    <h3 class="text-white fs-30 mb-0">Yada Ekidanta</h3>
                    <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="offcanvas-body ms-lg-auto d-flex flex-column h-100">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('/') ? 'active' : ''}}" href="{{route('web.home')}}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('about') ? 'active' : ''}}" href="{{route('web.about')}}">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('service') ? 'active' : ''}}" href="{{route('web.service')}}">Services</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('case-studies') || request()->is('case-studies/*') ? 'active' : ''}}" href="{{route('web.case')}}">Case Studies</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('contact') ? 'active' : ''}}" href="{{route('web.contact')}}">Contact</a>
                        </li>
                    </ul>
                    <!-- /.navbar-nav -->
                    <div class="offcanvas-footer d-lg-none">
                        <div>
                            <a href="mailto:hello@yadaekidanta.com" class="link-inverse">hello@yadaekidanta.com</a>
                            <br />(62) 877 4800 5611 <br />
                            <nav class="nav social social-white mt-4">
                                <a href="#"><i class="uil uil-twitter"></i></a>
                                <a href="#"><i class="uil uil-facebook-f"></i></a>
                                <a href="#"><i class="uil uil-instagram"></i></a>
                                <a href="#"><i class="uil uil-linkedin"></i></a>
                            </nav>
                            <!-- /.social -->
                        </div>
                    </div>
                    <!-- /.offcanvas-footer -->
                </div>
                <!-- /.offcanvas-body -->
            </div>
            <!-- /.navbar-collapse -->
            <div class="navbar-other w-100 d-flex ms-auto">
                <ul class="navbar-nav flex-row align-items-center ms-auto">
                    <li class="nav-item dropdown language-select text-uppercase">
                        <a class="nav-link dropdown-item dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">En</a>
                        <ul class="dropdown-menu">
                            <li class="nav-item"><a class="dropdown-item" href="#">En</a></li>
                            <li class="nav-item"><a class="dropdown-item" href="#">Id</a></li>
                        </ul>
                    </li>
                    {{-- <li class="nav-item d-none d-md-block">
                        <a href="" class="btn btn-sm btn-primary rounded-pill">Contact</a>
                    </li> --}}
                    <li class="nav-item"><a class="nav-link" data-bs-toggle="offcanvas" data-bs-target="#offcanvas-info"><i class="uil uil-info-circle"></i></a></li>
                    <li class="nav-item d-lg-none">
                        <button class="hamburger offcanvas-nav-btn"><span></span></button>
                    </li>
                </ul>
                <!-- /.navbar-nav -->
            </div>
            <!-- /.navbar-other -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- /.navbar -->
    <div class="offcanvas offcanvas-end text-inverse" id="offcanvas-info" data-bs-scroll="true">
        <div class="offcanvas-header">
            <h3 class="text-white fs-30 mb-0">Yada Ekidanta</h3>
            <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body pb-6">
            <div class="widget mb-8">
                <p>
                    YE is one of the Enteprise Digital Transfromation Consultant & Integrator company in Indonesia, specializing in software solutions and application delivery & managed services.
                    Our mission is to enable enterprises to ride the wave of digital era with tech-enabled innovation & business process automation.
                </p>
            </div>
            <!-- /.widget -->
            <div class="widget mb-8">
                <h4 class="widget-title text-white mb-3">Contact Info</h4>
                <address> Permata Buah Batu Blok C 15B<br />Bandung, Indonesia</address>
                <a href="mailto:hello@yadaekidanta.com">hello@yadaekidanta.com</a><br /> (62) 877 4800 5611
            </div>
            <!-- /.widget -->
            <div class="widget mb-8">
                <h4 class="widget-title text-white mb-3">Learn More</h4>
                <ul class="list-unstyled">
                    <li><a href="{{route('web.career.index')}}">Career</a></li>
                    <li><a href="{{route('web.terms-use')}}">Terms of Use</a></li>
                    <li><a href="{{route('web.privacy-policy')}}">Privacy Policy</a></li>
                </ul>
            </div>
            <!-- /.widget -->
        </div>
        <!-- /.offcanvas-body -->
    </div>
    <!-- /.offcanvas -->
</header>