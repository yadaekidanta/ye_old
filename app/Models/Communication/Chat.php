<?php

namespace App\Models\Communication;

use App\Models\HRM\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Chat extends Model
{
    use HasFactory;
    public $table = 'chat_messages';
    public function from ()
    {
        return $this->belongsTo(Employee::class,'from_id','id');
    }
    public function to ()
    {
        return $this->belongsTo(Employee::class,'to_id','id');
    }
}
