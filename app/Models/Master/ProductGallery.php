<?php

namespace App\Models\Master;

use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductGallery extends Model implements HasMedia
{
    use HasFactory;
    use HasMediaTrait;
    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
    ];

    public function photos()
    {
        return $this->morphMany(Media::class, 'model');
    }
    
}