<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class ProductFile extends Model implements HasMedia
{
    use HasFactory;
    use HasMediaTrait;

    protected $fillable = [
        'name',
        'description',
    ];

    public function photos()
    {
        return $this->morphMany(Media::class, 'model');
    }
}