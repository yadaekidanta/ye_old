<?php

namespace App\Models\Legality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LegalPolicy extends Model
{
    use HasFactory;
    public $timestamps = false;
}
