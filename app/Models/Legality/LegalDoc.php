<?php

namespace App\Models\Legality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LegalDoc extends Model
{
    use HasFactory;
    public $timestamps = false;
}
