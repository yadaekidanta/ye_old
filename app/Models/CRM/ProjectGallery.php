<?php

namespace App\Models\CRM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectGallery extends Model
{
    use HasFactory;
    public function getImageAttribute()
    {
        if($this->file){
            return asset('storage/' . $this->file);
        }else{
            return asset('img/icon.png');
        }
    }
}
