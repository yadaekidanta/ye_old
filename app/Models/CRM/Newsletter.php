<?php

namespace App\Models\CRM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Newsletter extends Model
{
    use HasFactory, Notifiable;
    protected $table = 'newsletter';
    // public function routeNotificationForMail($notification)
    // {
    //     // Return email address only...
    //     return $this->email;
 
    //     // Return email address and name...
    //     // return [$this->email_address => $this->name];
    // }
}
