<?php

namespace App\Models\CRM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;
    public function industry()
    {
        return $this->belongsTo(CompanyIndustry::class);
    }
    public function getImageAttribute()
    {
        if($this->company_logo){
            return asset('storage/' . $this->company_logo);
        }else{
            return asset('img/icon.png');
        }
    }
}
