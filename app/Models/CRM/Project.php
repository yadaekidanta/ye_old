<?php

namespace App\Models\CRM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
    protected $dates = ['start_at', 'due_at'];
    public function client ()
    {
        return $this->belongsTo(Client::class);
    }
    public function gallery ()
    {
        return $this->hasMany(ProjectGallery::class);
    }
    public function getImageAttribute()
    {
        if($this->thumbnail){
            return asset('storage/' . $this->thumbnail);
        }else{
            return asset('img/icon.png');
        }
    }
}
