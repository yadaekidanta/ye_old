<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyMail extends Model
{
    use HasFactory;
    protected $table = 'mails';
    public $timestamps = false;
}
