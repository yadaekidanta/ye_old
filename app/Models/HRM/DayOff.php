<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DayOff extends Model
{
    use HasFactory;
    public function employee ()
    {
        return $this->belongsTo(Employee::class,'employee_id','id');
    }
}