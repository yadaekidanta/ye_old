<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeKpiObjective extends Model
{
    use HasFactory;
    public function department ()
    {
        return $this->belongsTo(Department::class,'department_id','id');
    }
    public function position ()
    {
        return $this->belongsTo(Position::class,'position_id','id');
    }
}
