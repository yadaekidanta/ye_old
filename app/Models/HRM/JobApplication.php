<?php

namespace App\Models\HRM;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobApplication extends Model
{
    use HasFactory, Notifiable;
    public function employee()
    {
        return $this->belongsTo(Employee::class,'employee_id','id');
    }
    public function vacancy()
    {
        return $this->belongsTo(VacancyJob::class,'vacancy_id','id');
    }
}
