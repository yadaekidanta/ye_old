<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;
    public $timestamps = false;
    public function vacancy ()
    {
        return $this->hasMany(VacancyJob::class,'department_id','id');
    }
}
