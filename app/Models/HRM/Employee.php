<?php

namespace App\Models\HRM;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Str;
use App\Models\Communication\Chat;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Employee extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $fillable = [
        'name',
        'phone',
        'email',
        'password',
    ];
    protected $hidden = [
        'password',
        'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function getImageAttribute()
    {
        if($this->avatar){
            return asset('storage/' . $this->avatar);
        }else{
            return asset('img/icon.png');
        }
    }
    public function department ()
    {
        return $this->belongsTo(Department::class,'department_id','id');
    }
    public function position ()
    {
        return $this->belongsTo(Position::class,'position_id','id');
    }
    public function chat ()
    {
        return $this->hasMany(Chat::class,'to_id','id');
    }
    public static function calculate_profile($profile)
    {
        if ( ! $profile) {
            return 0;
        }
        $columns    = preg_grep('/(.+ed_at)|(.*id)/', array_keys($profile->makeHidden(['nip','jobdesc','email_verified_at','google_id','paypal','password','remember_token','last_seen','created_at','updated_at','department_id','avatar','position_id'])->toArray()), PREG_GREP_INVERT);
        $per_column = count($columns) > 1 ? 100 / count($columns) : 0;
        $total      = 0;
    
        foreach ($profile->toArray() as $key => $value) {
            if ($value !== NULL && $value !== [] && in_array($key, $columns)) {
                $total += $per_column;
            }
        }
        return round($total);
    }
}
