<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RegistrationEmployeeNotification extends Notification
{
    use Queueable;
    public $user;
    public $token;
    public function __construct($user,$token)
    {
        $this->user = $user;
        $this->token = $token;
    }
    public function via($notifiable)
    {
        return ['mail','database'];
    }
    public function toMail($notifiable)
    {
        return (new MailMessage)->subject('Yada Ekidanta | Thank you for your enthusiasm')->from('noreply@yadaekidanta.com')->view('email.register_employee',compact('notifiable'));
    }
    public function toArray($notifiable)
    {
        return [
            'tipe' => 'Registration Employee',
            'nama' => $notifiable->name,
            'pesan' => "Hi ".$notifiable->name.", Thank you for your enthusiasm, see you in the next step."
        ];
    }
}
