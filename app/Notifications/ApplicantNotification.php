<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ApplicantNotification extends Notification
{
    use Queueable;
    public $user;
    public $token;
    public function __construct($user,$token)
    {
        $this->user = $user;
        $this->token = $token;
    }
    public function via($notifiable)
    {
        return ['mail','database'];
    }
    public function toMail($notifiable)
    {
        return (new MailMessage)->subject('Yada Ekidanta | Application Send')->from('noreply@yadaekidanta.com')->view('email.applicant_employee',compact('notifiable'));
    }
    public function toArray($notifiable)
    {
        return [
            'tipe' => 'Application Employee',
            'pesan' => "Hi ".$notifiable->name.", Your application is under review, please wait for the result. Check your email and notification immediately."
        ];
    }
}
