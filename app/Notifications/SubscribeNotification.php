<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SubscribeNotification extends Notification
{
    use Queueable;
    public function __construct()
    {
        // 
    }
    public function via($notifiable)
    {
        return ['mail','database'];
    }
    public function toMail($notifiable)
    {
        return (new MailMessage)->subject('Yada Ekidanta | Thank you for subscribing')->from('noreply@yadaekidanta.com')->view('email.subscribe',compact('notifiable'));
    }
    public function toArray($notifiable)
    {
        return [
            'tipe' => 'Subscribe',
            'nama' => $notifiable->email,
            'pesan' => "Hi, Thank you for subscribing to ".config('app.name')." newsletter."
        ];
    }
}
