<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ApplicantRejectNotification extends Notification
{
    use Queueable;
    public $employee;
    public $job;
    public function __construct($employee,$job)
    {
        $this->employee = $employee;
        $this->job = $job;
    }
    public function via($notifiable)
    {
        return ['mail','database'];
    }
    public function toMail($notifiable)
    {
        $jabatan = $this->job->position->name;
        return (new MailMessage)->subject('Yada Ekidanta | Application Rejected')->from('noreply@yadaekidanta.com')->view('email.applicant_rejected',compact('notifiable','jabatan'));
    }
    public function toArray($notifiable)
    {
        $jabatan = $this->job->position->name;
        return [
            'tipe' => 'Application Rejected',
            'pesan' => "Dear, in place, Yours faithfully, Yada Ekidanta would like to thank you for your enthusiasm in the series of '$jabatan' interviews which were held on April 3, 2022. At the same time, we as the Yada Ekidanta Team would like to inform you that you have not passed at the interview stage. Keep the spirit and don't give up, hopefully this is the beginning of your journey in building a higher career path! `The pain of fighting is only temporary. You can feel it for a minute, an hour, a day, or a year. But if you give up, the pain will last forever.` (Lance Armstrong). Best Regards, Yada Ekidanta Bandung, Indonesia"
        ];
    }
}
