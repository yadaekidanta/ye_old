<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class WelcomeEmployeeNotification extends Notification
{
    use Queueable;
    public $employee;
    public $job;
    public function __construct($employee,$job)
    {
        $this->employee = $employee;
        $this->job = $job;
    }
    public function via($notifiable)
    {
        return ['mail','database'];
    }
    public function toMail($notifiable)
    {
        $jabatan = $this->job->position->name;
        return (new MailMessage)->subject('Yada Ekidanta | Application Accepted')->from('noreply@yadaekidanta.com')->view('email.welcome_employee',compact('notifiable','jabatan'));
    }
    public function toArray($notifiable)
    {
        $jabatan = $this->job->position->name;
        return [
            'tipe' => 'Welcome Employee',
            'nama' => $notifiable->name,
            'pesan' => "Hi ".$notifiable->name.", Your application is accepted, please click link below to join Whatsapp group."
        ];
    }
}
