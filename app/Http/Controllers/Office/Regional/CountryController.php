<?php

namespace App\Http\Controllers\Office\Regional;

use Illuminate\Http\Request;
use App\Models\Regional\Country;
use App\Models\Regional\Province;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CountryController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = Country::where('name', 'like', '%' . $request->keyword . '%')->paginate(10);
            return $this->render_view('regional.country.list', compact('collection'));
        }
        return $this->render_view('regional.country.main');
    }
    public function create()
    {
        return $this->render_view('regional.country.input', ['data' => new Country]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|unique:countries,code',
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('code')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code'),
                ]);
            }elseif ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $country = new Country;
        $country->code = $request->code;
        $country->name = $request->name;
        $country->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function show(Country $country)
    {
        return $this->render_view('regional.country.show', ['data' => $country]);
    }
    public function edit(Country $country)
    {
        return $this->render_view('regional.country.input', ['data' => $country]);
    }
    public function update(Request $request, Country $country)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|unique:countries,code,'.$country->id,
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('code')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code'),
                ]);
            }elseif ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $country->code = $request->code;
        $country->name = $request->name;
        $country->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function destroy(Country $country)
    {
        Province::where('country_id', $country->id)->delete();
        $country->delete();
        return response()->json([
            'message' => 'Successfully deleted country!'
        ]);
    }
}