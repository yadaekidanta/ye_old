<?php

namespace App\Http\Controllers\Office\Regional;

use Illuminate\Http\Request;
use App\Models\Regional\City;
use App\Models\Regional\Province;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\Regional\District;
use App\Models\Regional\Regency;
use App\Models\Regional\Subdistrict;
use Illuminate\Support\Facades\Validator;

class CityController extends Controller
{
    use OfficeResponseView;
    public function index(Province $province,Request $request)
    {
        if ($request->ajax()) {
            $collection = Regency::where('name', 'like', '%' . $request->keyword . '%')->where('province_id',$province->id)->paginate(10);
            return $this->render_view('regional.city.list', compact('collection'));
        }
        return $this->render_view('regional.city.main', compact('province'));
    }
    public function create(Province $province, Regency $city)
    {
        // $province = Province::get();
        return $this->render_view('regional.city.input', ['data' => new Regency, 'province' => $province]);
    }
    public function store(Request $request)
    {
        // dd($request->all(), $province);
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'postcode' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('province')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('province'),
                ]);
            }elseif ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('postcode')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('postcode'),
                ]);
            }
        }
        $city = new Regency;
        $city->province_id = $request->province;
        $city->name = $request->name;
        $city->postcode = $request->postcode;
        $city->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function show(Regency $city)
    {
        return $this->render_view('regional.city.show', ['data' => $city]);
    }
    public function edit(Province $province, Regency $city)
    {
        // $province = Province::get();
        return $this->render_view('regional.city.input', ['data' => $city, 'province' => $province]);
    }
    public function update(Request $request, Province $province, Regency $city)
    {
        $validator = Validator::make($request->all(), [
            'province' => 'required',
            'name' => 'required',
            'postcode' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('province')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('province'),
                ]);
            }elseif ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('postcode')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('postcode'),
                ]);
            }
        }
        $city->province_id = $request->province;
        $city->name = $request->name;
        $city->postcode = $request->postcode;
        $city->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function destroy(Province $province, Regency $city)
    {
        District::where('regency_id', $city->id)->delete();
        $city->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil dihapus',
        ]);
    }
    public function get_subdistrict(Request $request)
    {
        $collection = District::where('city_id',$request->city)->get();
        $list = "<option>Choose Subdistrict</option>";
        foreach($collection as $row){
            $list .= "<option value='$row->id'>$row->name</option>";
        }
        return $list;
    }
}