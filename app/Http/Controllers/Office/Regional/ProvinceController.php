<?php

namespace App\Http\Controllers\Office\Regional;

use Illuminate\Http\Request;
use App\Models\Regional\Country;
use App\Models\Regional\Province;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\Regional\City;
use App\Models\Regional\Regency;
use Illuminate\Support\Facades\Validator;

class ProvinceController extends Controller
{
    use OfficeResponseView;
    public function index(Country $country, Request $request)
    {
        if ($request->ajax()){
            $collection = Province::where('name', 'like', '%' . $request->keyword . '%')->where('country_id',$country->id)->paginate(10);
            return $this->render_view('regional.province.list', compact('collection'));
        }
        return $this->render_view('regional.province.main', compact('country'));
    }
    public function create(Country $country,Province $province)
    {
        // $country = Country::get();
        return $this->render_view('regional.province.input', ['data' => new Province, 'country' => $country]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'country' => 'required',
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('country')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('country'),
                ]);
            }elseif ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $province = new Province;
        $province->country_id = $request->country;
        $province->name = $request->name;
        $province->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function show(Province $province)
    {
        return $this->render_view('regional.province.show', ['data' => $province]);
    }
    public function edit(Country $country,Province $province)
    {
        // $country = Country::get();
        return $this->render_view('regional.province.input', ['data' => $province, 'country' => $country]);
    }
    public function update(Request $request, Country $country, Province $province)
    {
        $validator = Validator::make($request->all(), [
            'country' => 'required',
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('country')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('country'),
                ]);
            }elseif ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $province->country_id = $request->country;
        $province->name = $request->name;
        $province->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function destroy(Country $country,Province $province)
    {
        Regency::where('province_id', $province->id)->delete();
        $province->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil dihapus',
        ]);
    }
    public function get_city(Request $request)
    {
        $collection = Regency::where('province_id',$request->province)->get();
        $list = "<option>Choose City</option>";
        foreach($collection as $row){
            $list .= "<option value='$row->id'>$row->name</option>";
        }
        return $list;
    }
}