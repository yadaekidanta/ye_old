<?php

namespace App\Http\Controllers\Office\Regional;

use Illuminate\Http\Request;
use App\Models\Regional\Village;
use App\Models\Regional\District;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\Regional\Subdistrict;
use Illuminate\Support\Facades\Validator;

class VillageController extends Controller
{
    use OfficeResponseView;
    public function index(District $district, Request $request)
    {
        if ($request->ajax()) {
            $collection = Village::where('name', 'like', '%' . $request->keyword . '%')->where('subdistrict_id',$district->id)->paginate(10);
            return $this->render_view('regional.village.list', compact('collection'));
        }
        return $this->render_view('regional.village.main', compact('district'));
    }
    public function create(District $district, Village $village)
    {
        // $subdistrict = Subdistrict::get();
        return $this->render_view('regional.village.input', ['data' => new Village, 'district' => $district]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('subdistrict')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('subdistrict'),
                ]);
            }elseif ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $village = new Village;
        $village->subdistrict_id = $request->district;
        $village->name = $request->name;
        $village->postcode = $request->postcode;
        $village->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function show(Village $village)
    {
        return $this->render_view('regional.village.show', ['data' => $village]);
    }
    public function edit(District $district, Village $village)
    {
        // $subdistrict = Subdistrict::get();
        return $this->render_view('regional.village.input', ['data' => $village, 'district' => $district]);
    }
    public function update(Request $request, District $district, Village $village)
    {
        $validator = Validator::make($request->all(), [
            'subdistrict' => 'required',
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('subdistrict')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('subdistrict'),
                ]);
            }elseif ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $village->subdistrict_id = $request->subdistrict;
        $village->name = $request->name;
        $village->postcode = $request->postcode;
        $village->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function destroy(District $district, Village $village)
    {
        $village->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil dihapus',
        ]);
    }
}