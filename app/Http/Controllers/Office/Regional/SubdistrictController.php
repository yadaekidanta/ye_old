<?php

namespace App\Http\Controllers\Office\Regional;

use Illuminate\Http\Request;
use App\Models\Regional\City;
use App\Models\Regional\Village;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\Regional\District;
use App\Models\Regional\Regency;
use App\Models\Regional\Subdistrict;
use Illuminate\Support\Facades\Validator;

class SubdistrictController extends Controller
{
    use OfficeResponseView;
    public function index(Regency $city, Request $request)
    {
        if ($request->ajax()) {
            $collection = District::where('name', 'like', '%' . $request->keyword . '%')->where('regency_id',$city->id)->paginate(10);
            return $this->render_view('regional.subdistrict.list', compact('collection'));
        }
        return $this->render_view('regional.subdistrict.main', compact('city'));
    }
    public function create(Regency $city, District $district)
    {
        // $city = Regency::get();
        return $this->render_view('regional.subdistrict.input', ['data' => new District, 'city' => $city]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'postcode' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('city')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('city'),
                ]);
            }elseif ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('postcode')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('postcode'),
                ]);
            }
        }
        $district = new District;
        $district->regency_id = $request->city;
        $district->name = $request->name;
        $district->postcode = $request->postcode;
        $district->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function show(District $district)
    {
        return $this->render_view('regional.subdistrict.show', ['data' => $district]);
    }
    public function edit(Regency $city, District $district)
    {
        // $city = Regency::get();
        return $this->render_view('regional.subdistrict.input', ['data' => $district, 'city' => $city]);
    }
    public function update(Request $request, Regency $city, District $district)
    {
        $validator = Validator::make($request->all(), [
            'city' => 'required',
            'name' => 'required',
            'postcode' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('city')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('city'),
                ]);
            }elseif ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('postcode')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('postcode'),
                ]);
            }
        }
        $district->regency_id = $request->city;
        $district->name = $request->name;
        $district->postcode = $request->postcode;
        $district->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function destroy(Regency $city, District $district)
    {
        Village::where('subdistrict_id', $district->id)->delete();
        $district->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil dihapus',
        ]);
    }
    public function get_postcode(Request $request)
    {
        $collection = Subdistrict::where('id',$request->subdistrict)->first();
        return $collection;
    }
}