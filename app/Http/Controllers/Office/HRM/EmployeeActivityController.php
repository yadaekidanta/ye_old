<?php

namespace App\Http\Controllers\Office\HRM;

use Illuminate\Http\Request;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\HRM\EmployeeActivity;

class EmployeeActivityController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = EmployeeActivity::paginate(10);
            return $this->render_view('hrm.employee_activity.list',compact('collection'));
        }
        return $this->render_view('hrm.employee_activity.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(EmployeeActivity $employeeActivity)
    {
        //
    }
    public function edit(EmployeeActivity $employeeActivity)
    {
        //
    }
    public function update(Request $request, EmployeeActivity $employeeActivity)
    {
        //
    }
    public function destroy(EmployeeActivity $employeeActivity)
    {
        //
    }
}
