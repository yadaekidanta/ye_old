<?php

namespace App\Http\Controllers\Office\HRM;

use App\Models\HRM\DayOff;
use App\Models\HRM\Employee;
use Illuminate\Http\Request;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DayOffController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $employee = Employee::get();
            $collection = DayOff::where('date','LIKE','%'.$request->keyword.'%')->paginate(10);
            return $this->render_view('hrm.day_off.list',compact('collection','employee'));
        }
        return $this->render_view('hrm.day_off.main');
    }
    public function create()
    {
        $employee = Employee::get();
        return $this->render_view('hrm.day_off.input', ['data' => new DayOff, 'employee' => $employee]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'employee' => 'required',
            'date' => 'required',
            'amount' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('employee')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('employee'),
                ]);
            }elseif ($errors->has('date')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('date'),
                ]);
            }elseif ($errors->has('amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('amount'),
                ]);
            }
        }
        $dayOff = new DayOff;
        $dayOff->employee_id = $request->employee;
        $dayOff->date = $request->date;
        $dayOff->amount = $request->amount;
        $dayOff->st = 'Pending';
        $dayOff->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function show(DayOff $dayOff)
    {
        //
    }
    public function edit(DayOff $dayOff)
    {
        $employee = Employee::get();
        return $this->render_view('hrm.day_off.input', ['data' => $dayOff, 'employee' => $employee]);
    }
    public function update(Request $request, DayOff $dayOff)
    {
        $validator = Validator::make($request->all(), [
            'st' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('st')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('st'),
                ]);
            }
        }
        $dayOff->reason = $request->reason;
        $dayOff->st = $request->st;
        $dayOff->verified_by = Auth::guard('office')->user()->id;
        $dayOff->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function destroy(DayOff $dayOff)
    {
        $dayOff->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil dihapus',
        ]);
    }
    public function destroy_checked(Request $request)
    {
        foreach($request->id as $id){
            $dayOff = DayOff::find($id);
            $dayOff->delete();
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Day Off '. __('custom.deleted'),
        ]);
    }
}