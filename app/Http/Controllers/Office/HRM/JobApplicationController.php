<?php

namespace App\Http\Controllers\Office\HRM;

use Illuminate\Http\Request;
use App\Models\HRM\JobApplication;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;
use App\Notifications\ApplicantRejectNotification;
use App\Notifications\WelcomeEmployeeNotification;

class JobApplicationController extends Controller
{
    public function index(Request $request)
    {
       //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Request $request,$job_application)
    {
        if($request->ajax()){
            $collection = JobApplication::paginate(10);
            return $this->render_view('hrm.job_applicants.show_list', compact('collection'));
        }
        return $this->render_view('hrm.job_applicants.show');
    }
    public function edit(JobApplication $jobApplication)
    {
        //
    }
    public function update(Request $request, JobApplication $jobApplication)
    {
        //
    }
    public function destroy(JobApplication $jobApplication)
    {
        //
    }
    public function accept(JobApplication $job_applicant)
    {
        $job_applicant->st = 'a';
        $job_applicant->update();
        Notification::send($job_applicant->employee, new WelcomeEmployeeNotification($job_applicant->employee,$job_applicant->vacancy));
        return response()->json([
            'alert' => 'success',
            'message' => 'Job Application '. __('custom.accepted'),
        ]);
    }
    public function reject(JobApplication $job_applicant)
    {
        $job_applicant->st = 'r';
        $job_applicant->update();
        Notification::send($job_applicant->employee, new ApplicantRejectNotification($job_applicant->employee,$job_applicant->vacancy));
        return response()->json([
            'alert' => 'success',
            'message' => 'Job Application '. __('custom.rejected'),
        ]);
    }
}
