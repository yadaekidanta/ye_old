<?php

namespace App\Http\Controllers\Office\HRM;

use App\Models\HRM\Position;
use Illuminate\Http\Request;
use App\Models\HRM\Department;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PositionController extends Controller
{
    use OfficeResponseView;
    public function index()
    {
        //
    }
    public function create(Department $department)
    {
        return $this->render_view('hrm.position.input', ['data' => new Position, 'department' => $department]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'department' => 'required',
            'name' => 'required|unique:positions,name',
            'desc' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('department')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('department'),
                ]);
            }elseif ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('desc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('desc'),
                ]);
            }
        }
        $position = new Position;
        $position->department_id = $request->department;
        $position->name = $request->name;
        $position->desc = $request->desc;
        $position->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Position '. __('custom.saved'),
        ]);
    }
    public function show(Position $position)
    {
        //
    }
    public function edit(Department $department, Position $position)
    {
        return $this->render_view('hrm.position.input', ['data' => $position, 'department' => $department]);
    }
    public function update(Request $request,Department $department, Position $position)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:positions,name,'.$position->id,
            'desc' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('desc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('desc'),
                ]);
            }
        }
        $position->name = $request->name;
        $position->desc = $request->desc;
        $position->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Position '. __('custom.saved'),
        ]);
    }
    public function destroy(Department $department, Position $position)
    {
        $position->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Position '. __('custom.deleted'),
        ]);
    }
    public function destroy_checked(Request $request)
    {
        foreach($request->id as $id){
            $position = Position::find($id);
            $position->delete();
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Position '. __('custom.deleted'),
        ]);
    }
}
