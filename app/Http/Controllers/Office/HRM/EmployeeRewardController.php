<?php

namespace App\Http\Controllers\Office\HRM;

use App\Models\HRM\Employee;
use Illuminate\Http\Request;
use App\Models\HRM\EmployeeReward;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class EmployeeRewardController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = EmployeeReward::get();
            return $this->render_view('hrm.reward.list',compact('collection'));
        }
        return $this->render_view('hrm.reward.main');
    }
    public function create()
    {
        $employee = Employee::get();
        return $this->render_view('hrm.reward.input', ['data' => new EmployeeReward, 'employee' => $employee]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'employee' => 'required',
            'type' => 'required',
            'amount' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('employee')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('employee'),
                ]);
            }elseif ($errors->has('type')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('type'),
                ]);
            }elseif ($errors->has('amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('amount'),
                ]);
            }
        }
        $employeeReward = new EmployeeReward;
        $employeeReward->employee_id = $request->employee;
        $employeeReward->type = $request->type;
        $employeeReward->amount = $request->amount;
        $employeeReward->created_by = Auth::guard('office')->user()->id;
        $employeeReward->st = 'Pending';
        $employeeReward->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Employee Reward '. __('custom.saved'),
        ]);
    }
    public function show(EmployeeReward $employeeReward)
    {
        //
    }
    public function edit(EmployeeReward $employeeReward)
    {
        $employee = Employee::get();
        return $this->render_view('hrm.reward.input', ['data' => $employeeReward, 'employee' => $employee]);
    }
    public function update(Request $request, EmployeeReward $employeeReward)
    {
        $validator = Validator::make($request->all(), [
            'st' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('st')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('st'),
                ]);
            }
        }
        $employeeReward->st = $request->st;
        $employeeReward->reason = $request->reason;
        $employeeReward->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Employee Reward '. __('custom.saved'),
        ]);
    }
    public function destroy(EmployeeReward $employeeReward)
    {
        $employeeReward->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil dihapus',
        ]);
    }
}
