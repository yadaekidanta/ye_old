<?php

namespace App\Http\Controllers\Office\HRM;

use App\Models\HRM\Employee;
use Illuminate\Http\Request;
use App\Models\HRM\EmployeeKpi;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\HRM\EmployeeKpiKeyResult;
use App\Models\HRM\EmployeeKpiObjective;
use Illuminate\Support\Facades\Validator;

class EmployeeKpiKeyResultController extends Controller
{
    use OfficeResponseView;
    public function index()
    {
        //
    }
    public function create(EmployeeKpiObjective $kpiObjective)
    {
        $employee = Employee::where('position_id',$kpiObjective->position_id)->get();
        $superior = Employee::whereIn('position_id',[4,5,6,17])->get();
        return $this->render_view('hrm.kpi_key.input', ['data' => new EmployeeKpiKeyResult, 'kpiObjective' => $kpiObjective, 'employee' => $employee, 'superior' => $superior]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'employee' => 'required',
            'superior' => 'required',
            'quarter' => 'required',
            'key_result' => 'required',
            'description' => 'required',
            'measure_unit' => 'required',
            'commited_target' => 'required',
            'weight' => 'required',
            'review' => 'required',
            'supporting_data' => 'required',
            'score' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('employee')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('employee'),
                ]);
            }elseif ($errors->has('superior')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('superior'),
                ]);
            }elseif ($errors->has('quarter')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('quarter'),
                ]);
            }elseif ($errors->has('key_result')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('key_result'),
                ]);
            }elseif ($errors->has('description')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('description'),
                ]);
            }elseif ($errors->has('measure_unit')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('measure_unit'),
                ]);
            }elseif ($errors->has('commited_target')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('commited_target'),
                ]);
            }elseif ($errors->has('weight')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('weight'),
                ]);
            }elseif ($errors->has('review')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('review'),
                ]);
            }elseif ($errors->has('supporting_data')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('supporting_data'),
                ]);
            }elseif ($errors->has('score')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('score'),
                ]);
            }
        }
        $employeeKpiKeyResult = new EmployeeKpiKeyResult;
        $employeeKpiKeyResult->employee_id = $request->employee;
        $employeeKpiKeyResult->superior_id = $request->superior;
        $employeeKpiKeyResult->quarter = $request->quarter;
        $employeeKpiKeyResult->key_result = $request->key_result;
        $employeeKpiKeyResult->description = $request->description;
        $employeeKpiKeyResult->measure_unit = $request->measure_unit;
        $employeeKpiKeyResult->commited_target = $request->commited_target;
        $employeeKpiKeyResult->weight = $request->weight;
        $employeeKpiKeyResult->review = $request->review;
        $employeeKpiKeyResult->supporting_data = $request->supporting_data;
        $employeeKpiKeyResult->score = $request->score;
        $employeeKpiKeyResult->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Objective Key Result '. __('custom.saved'),
        ]);
    }
    public function show(EmployeeKpiKeyResult $employeeKpiKeyResult)
    {
        //
    }
    public function edit(EmployeeKpiObjective $employeeKpiObjective, EmployeeKpiKeyResult $employeeKpiKeyResult)
    {
        $employee = Employee::where('position_id',$employeeKpiObjective->position_id)->get();
        $superior = Employee::whereIn('position_id',[4,5,6,17])->get();
        return $this->render_view('hrm.employee_kpi_key_result.input', ['data' => new EmployeeKpiKeyResult, 'employeeKpiObjective' => $employeeKpiObjective, 'employee' => $employee, 'superior' => $superior]);
    }
    public function update(Request $request, EmployeeKpiKeyResult $employeeKpiKeyResult)
    {
        $validator = Validator::make($request->all(), [
            'employee' => 'required',
            'superior' => 'required',
            'quarter' => 'required',
            'key_result' => 'required',
            'description' => 'required',
            'measure_unit' => 'required',
            'commited_target' => 'required',
            'weight' => 'required',
            'review' => 'required',
            'supporting_data' => 'required',
            'score' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('employee')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('employee'),
                ]);
            }elseif ($errors->has('superior')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('superior'),
                ]);
            }elseif ($errors->has('quarter')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('quarter'),
                ]);
            }elseif ($errors->has('key_result')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('key_result'),
                ]);
            }elseif ($errors->has('description')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('description'),
                ]);
            }elseif ($errors->has('measure_unit')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('measure_unit'),
                ]);
            }elseif ($errors->has('commited_target')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('commited_target'),
                ]);
            }elseif ($errors->has('weight')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('weight'),
                ]);
            }elseif ($errors->has('review')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('review'),
                ]);
            }elseif ($errors->has('supporting_data')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('supporting_data'),
                ]);
            }elseif ($errors->has('score')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('score'),
                ]);
            }
        }
        $employeeKpiKeyResult->employee_id = $request->employee;
        $employeeKpiKeyResult->superior_id = $request->superior;
        $employeeKpiKeyResult->quarter = $request->quarter;
        $employeeKpiKeyResult->key_result = $request->key_result;
        $employeeKpiKeyResult->description = $request->description;
        $employeeKpiKeyResult->measure_unit = $request->measure_unit;
        $employeeKpiKeyResult->commited_target = $request->commited_target;
        $employeeKpiKeyResult->weight = $request->weight;
        $employeeKpiKeyResult->review = $request->review;
        $employeeKpiKeyResult->supporting_data = $request->supporting_data;
        $employeeKpiKeyResult->score = $request->score;
        $employeeKpiKeyResult->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Objective Key Result '. __('custom.saved'),
        ]);
    }
    public function destroy(EmployeeKpiKeyResult $employeeKpiKeyResult)
    {
        $employeeKpiKeyResult->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Objective Key Result '. __('custom.deleted'),
        ]);
    }
}