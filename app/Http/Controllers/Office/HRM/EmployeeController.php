<?php

namespace App\Http\Controllers\Office\HRM;

use App\Models\HRM\Employee;
use App\Models\HRM\Position;
use Illuminate\Http\Request;
use App\Models\Regional\City;
use App\Models\HRM\Department;
use App\Models\Regional\Province;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\Regional\Subdistrict;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = Employee::where('name','LIKE','%'.$request->keyword.'%')->where('email','LIKE','%'.$request->keyword.'%')->paginate(10);
            return $this->render_view('hrm.employee.list',compact('collection'));
        }
        return $this->render_view('hrm.employee.main');
    }
    public function create()
    {
        $province = Province::get();
        $department = Department::get();
        return $this->render_view('hrm.employee.input', ['data' => new Employee, 'province' => $province, 'department' => $department]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nip' => 'required|unique:employees,nip',
            'name' => 'required',
            'email' => 'required|unique:employees,email',
            'password' => 'required',
            'phone' => 'required|unique:employees,phone',
            'date_birth' => 'required',
            'jobdesc' => 'required',
            'address' => 'required',
            'province' => 'required',
            'city' => 'required',
            'subdistrict' => 'required',
            // 'postcode' => 'required',
            'department' => 'required',
            'position' => 'required',
            'st' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nip')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nip'),
                ]);
            }elseif ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            }elseif ($errors->has('phone')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }elseif ($errors->has('date_birth')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('date_birth'),
                ]);
            }elseif ($errors->has('jobdesc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jobdesc'),
                ]);
            }elseif ($errors->has('address')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('address'),
                ]);
            }elseif ($errors->has('province')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('province'),
                ]);
            }elseif ($errors->has('city')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('city'),
                ]);
            }elseif ($errors->has('subdistrict')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('subdistrict'),
                ]);
            }elseif ($errors->has('postcode')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('postcode'),
                ]);
            }elseif ($errors->has('department')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('department'),
                ]);
            }elseif ($errors->has('position')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('position'),
                ]);
            }elseif ($errors->has('st')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('st'),
                ]);
            }
        }
        $employee = new Employee;
        $employee->nip = $request->nip;
        $employee->name = $request->name;
        $employee->email = $request->email;
        $employee->password = Hash::make($request->password);
        $employee->phone = $request->phone;
        $employee->date_birth = $request->date_birth;
        $employee->jobdesc = $request->jobdesc;
        $employee->address = $request->address;
        $employee->province_id = $request->province;
        $employee->city_id = $request->city;
        $employee->subdistrict_id = $request->subdistrict;
        $employee->postcode = $request->postcode;
        $employee->department_id = $request->department;
        $employee->position_id = $request->position;
        $employee->st = $request->st;
        if(request()->file('avatar')){
            $avatar = request()->file('avatar')->store("avatar");
            $employee->avatar = $avatar;
        }
        if(request()->file('cv')){
            $cv = request()->file('cv')->store("cv");
            $employee->cv = $cv;
        }
        $employee->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function show(Employee $employee)
    {
        return $this->render_view('hrm.employee.show', ['data' => $employee]);
    }
    public function edit(Employee $employee)
    {
        $province = Province::get();
        $department = Department::get();
        return $this->render_view('hrm.employee.input', ['data' => $employee, 'province' => $province, 'department' => $department]);
    }
    public function update(Request $request, Employee $employee)
    {
        $validator = Validator::make($request->all(), [
            'nip' => 'required|unique:employees,nip,'.$employee->id,
            'name' => 'required',
            'email' => 'required|unique:employees,email,'.$employee->id,
            'phone' => 'required|unique:employees,phone,'.$employee->id,
            'date_birth' => 'required',
            'jobdesc' => 'required',
            'address' => 'required',
            'province' => 'required',
            'city' => 'required',
            'subdistrict' => 'required',
            // 'postcode' => 'required',
            'department' => 'required',
            'position' => 'required',
            'st' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nip')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nip'),
                ]);
            }elseif ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('phone')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }elseif ($errors->has('date_birth')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('date_birth'),
                ]);
            }elseif ($errors->has('jobdesc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jobdesc'),
                ]);
            }elseif ($errors->has('bio')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('bio'),
                ]);
            }elseif ($errors->has('impression')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('impression'),
                ]);
            }elseif ($errors->has('address')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('address'),
                ]);
            }elseif ($errors->has('province')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('province'),
                ]);
            }elseif ($errors->has('city')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('city'),
                ]);
            }elseif ($errors->has('subdistrict')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('subdistrict'),
                ]);
            }elseif ($errors->has('postcode')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('postcode'),
                ]);
            }elseif ($errors->has('department')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('department'),
                ]);
            }elseif ($errors->has('position')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('position'),
                ]);
            }
        }
        $employee->nip = $request->nip;
        $employee->name = $request->name;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->date_birth = $request->date_birth;
        $employee->jobdesc = $request->jobdesc;
        $employee->bio = $request->bio;
        $employee->impression = $request->impression;
        $employee->address = $request->address;
        $employee->province_id = $request->province;
        $employee->city_id = $request->city;
        $employee->subdistrict_id = $request->subdistrict;
        $employee->postcode = $request->postcode;
        $employee->department_id = $request->department;
        $employee->position_id = $request->position;
        if(request()->file('avatar')){
            Storage::delete($employee->avatar);
            $avatar = request()->file('avatar')->store("avatar");
            $employee->avatar = $avatar;
        }
        if(request()->file('cv')){
            Storage::delete($employee->cv);
            $cv = request()->file('cv')->store("cv");
            $employee->cv = $cv;
        }
        $employee->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function destroy(Employee $employee)
    {
        return response()->json([
            'alert' => 'success',
            'message' => 'Data tidak dapat dihapus',
        ]);
    }
    public function upload_cv(Request $request){
        $path = storage_path('tmp/employee_cv');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('file');
        $name = uniqid() . '_' . trim($file->getClientOriginalName());
        $file->move($path, $name);

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }
}
