<?php

namespace App\Http\Controllers\Office\HRM;

use Illuminate\Http\Request;
use App\Models\HRM\EmployeeKpi;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\HRM\EmployeeKpiObjective;
use Illuminate\Support\Facades\Validator;

class EmployeeKpiController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = EmployeeKpi::where('function','LIKE','%'.$request->keyword.'%')->paginate(10);
            return $this->render_view('hrm.kpi.list',compact('collection'));
        }
        return $this->render_view('hrm.kpi.main');
    }
    public function create()
    {
        return $this->render_view('hrm.kpi.input', ['data' => new EmployeeKpi]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'function' => 'required|unique:employee_kpis,function',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('function')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('function'),
                ]);
            }
        }
        $employeeKpi = new EmployeeKpi;
        $employeeKpi->function = $request->function;
        $employeeKpi->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'KPI '. __('custom.saved'),
        ]);
    }
    public function show(Request $request, EmployeeKpi $kpi)
    {
        if ($request->ajax()) {
            $collection = EmployeeKpiObjective::where('objective','LIKE','%'.$request->keyword.'%')->where('employee_kpi_id',$kpi->id)->paginate(10);
            return $this->render_view('hrm.kpi_objective.list',compact('collection'));
        }
        $kpidata = EmployeeKpi::get();
        return $this->render_view('hrm.kpi.show', ['data' => $kpi, 'kpidata' => $kpidata]);
    }
    public function edit(EmployeeKpi $kpi)
    {
        return $this->render_view('hrm.kpi.input', ['data' => $kpi]);
    }
    public function update(Request $request, EmployeeKpi $employeeKpi)
    {
        $validator = Validator::make($request->all(), [
            'function' => 'required|unique:employee_kpis,function,'.$employeeKpi->id,
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('function')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('function'),
                ]);
            }
        }
        $employeeKpi->function = $request->function;
        $employeeKpi->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'KPI '. __('custom.saved'),
        ]);
    }
    public function destroy(EmployeeKpi $kpi)
    {
        $kpi->delete();
        return response()->json([
            'alert' => 'info',
            'message' => 'KPI sudah dihapus',
        ]);
    }
}