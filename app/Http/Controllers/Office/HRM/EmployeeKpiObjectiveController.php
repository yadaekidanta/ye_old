<?php

namespace App\Http\Controllers\Office\HRM;

use App\Models\HRM\Position;
use Illuminate\Http\Request;
use App\Models\HRM\Department;
use App\Models\HRM\EmployeeKpi;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\HRM\EmployeeKpiKeyResult;
use App\Models\HRM\EmployeeKpiObjective;
use Illuminate\Support\Facades\Validator;

class EmployeeKpiObjectiveController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        
    }
    
    public function create(EmployeeKpi $kpi)
    {
        $department = Department::get();
        return $this->render_view('hrm.kpi_objective.input', ['data' => new EmployeeKpiObjective, 'kpi' => $kpi, 'department' => $department]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'objective' => 'required',
            'department' => 'required',
            'position' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('objective')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('objective'),
                ]);
            }elseif ($errors->has('department')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('department'),
                ]);
            }elseif ($errors->has('position')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('position'),
                ]);
            }
        }
        $employeeKpiObjective = new EmployeeKpiObjective;
        $employeeKpiObjective->employee_kpi_id = $request->kpi_id;
        $employeeKpiObjective->objective = $request->objective;
        $employeeKpiObjective->department_id = $request->department;
        $employeeKpiObjective->position_id = $request->position;
        $employeeKpiObjective->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'KPI Objective '. __('custom.saved'),
        ]);
    }
    
    public function show(Request $request, EmployeeKpiObjective $kpiObjective)
    {
        if ($request->ajax()) {
            $collection = EmployeeKpiKeyResult::where('key_result','LIKE','%'.$request->keyword.'%')->where('employee_kpi_objective_id',$kpiObjective->id)->paginate(10);
            return $this->render_view('hrm.kpi_key.list',compact('collection'));
        }
        $data = EmployeeKpiObjective::get();
        return $this->render_view('hrm.kpi_objective.show', ['data' => $data, 'objective' => $kpiObjective]);
    }
    
    public function edit(EmployeeKpi $kpi, EmployeeKpiObjective $kpiObjective)
    {
        $department = Department::get();
        return $this->render_view('hrm.kpi_objective.input', ['data' => $kpiObjective, 'kpi' => $kpi, 'department' => $department]);
    }
    
    public function update(Request $request, EmployeeKpi $kpi, EmployeeKpiObjective $kpiObjective)
    {
        $validator = Validator::make($request->all(), [
            'objective' => 'required',
            'department' => 'required',
            'position' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('objective')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('objective'),
                ]);
            }elseif ($errors->has('department')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('department'),
                ]);
            }elseif ($errors->has('position')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('position'),
                ]);
            }
        }
        $kpiObjective->employee_kpi_id = $request->kpi_id;
        $kpiObjective->objective = $request->objective;
        $kpiObjective->department_id = $request->department;
        $kpiObjective->position_id = $request->position;
        $kpiObjective->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'KPI Objective '. __('custom.saved'),
        ]);
    }
    
    public function destroy(EmployeeKpi $kpi, EmployeeKpiObjective $kpiObjective)
    {
        $kpiObjective->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'KPI Objective '. __('custom.deleted'),
        ]);
    }
}