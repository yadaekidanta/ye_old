<?php

namespace App\Http\Controllers\Office\HRM;

use App\Models\HRM\Employee;
use Illuminate\Http\Request;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\HRM\EmployeeCertificate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class EmployeeCertificateController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = EmployeeCertificate::paginate(10);
            return $this->render_view('hrm.employee_certificate.list',compact('collection'));
        }
        return $this->render_view('hrm.employee_certificate.main');
    }
    public function create()
    {
        $employee = Employee::get();
        return $this->render_view('hrm.employee_certificate.input', ['data' => new EmployeeCertificate, 'employee' => $employee]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'employee' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('employee')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('employee'),
                ]);
            }
        }
        $employeeCertificate = new EmployeeCertificate;
        $employeeCertificate->employee_id = $request->employee;
        if(request()->file('file')){
            $file = request()->file('file')->store("employee_certificate");
            $employeeCertificate->file = $file;
        }
        $employeeCertificate->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function show(EmployeeCertificate $employeeCertificate)
    {
        //
    }
    public function edit(EmployeeCertificate $employeeCertificate)
    {
        $employee = Employee::get();
        return $this->render_view('hrm.employee_certificate.input', ['data' => $employeeCertificate, 'employee' => $employee]);
    }
    public function update(Request $request, EmployeeCertificate $employeeCertificate)
    {
        $validator = Validator::make($request->all(), [
            'employee' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('employee')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('employee'),
                ]);
            }
        }
        $employeeCertificate->employee_id = $request->employee;
        if(request()->file('file')){
            Storage::delete($employeeCertificate->file);
            $file = request()->file('file')->store("employee_certificate");
            $employeeCertificate->file = $file;
        }
        $employeeCertificate->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function destroy(EmployeeCertificate $employeeCertificate)
    {
        $employeeCertificate->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil dihapus',
        ]);
    }
    public function destroy_checked(Request $request)
    {
        foreach($request->id as $id){
            $employeeCertificate = EmployeeCertificate::find($id);
            $employeeCertificate->delete();
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Employee Certificate '. __('custom.deleted'),
        ]);
    }
}
