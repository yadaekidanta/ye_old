<?php

namespace App\Http\Controllers\Office\HRM;

use App\Models\HRM\Position;
use Illuminate\Http\Request;
use App\Models\HRM\Department;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class DepartmentController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = Department::where('name','LIKE','%'.$request->keyword.'%')->paginate(10);
            return $this->render_view('hrm.department.list',compact('collection'));
        }
        return $this->render_view('hrm.department.main');
    }
    public function create()
    {
        return $this->render_view('hrm.department.input', ['data' => new Department]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:departments,name',
            'desc' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('desc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('desc'),
                ]);
            }
        }
        $department = new Department;
        $department->name = $request->name;
        $department->desc = $request->desc;
        $department->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Department '. __('custom.saved'),
        ]);
    }
    public function show(Request $request, Department $department)
    {
        if ($request->ajax()) {
            $collection = Position::where('name','LIKE','%'.$request->keyword.'%')->where('department_id',$department->id)->paginate(10);
            return $this->render_view('hrm.position.list',compact('collection'));
        }
        $collection = Department::get();
        return $this->render_view('hrm.department.show',compact('department','collection'));
    }
    public function edit(Department $department)
    {
        return $this->render_view('hrm.department.input', ['data' => $department]);
    }
    public function update(Request $request, Department $department)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:departments,name,'.$department->id,
            'desc' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('desc')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('desc'),
                ]);
            }
        }
        $department->name = $request->name;
        $department->desc = $request->desc;
        $department->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Department '. __('custom.saved'),
        ]);
    }
    public function destroy(Department $department)
    {
        $department->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Department '. __('custom.deleted'),
        ]);
    }
    public function destroy_checked(Request $request)
    {
        foreach($request->id as $id){
            $department = Department::find($id);
            $department->delete();
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Department '. __('custom.deleted'),
        ]);
    }
    public function get_position(Request $request)
    {
        $collection = Position::where('department_id',$request->department)->get();
        $list = "<option>Choose Position</option>";
        foreach($collection as $row){
            $list .= "<option value='$row->id'>$row->name</option>";
        }
        return $list;
    }
}