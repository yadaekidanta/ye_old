<?php

namespace App\Http\Controllers\Office\HRM;

use App\Models\Master\Bank;
use Illuminate\Http\Request;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\HRM\EmployeeBankAccount;
use Illuminate\Support\Facades\Validator;

class EmployeeBankAccountController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if($request->ajax()){
            $collection = EmployeeBankAccount::where('employee_id',Auth::guard('office')->user()->id)->paginate(10);
            return $this->render_view('hrm.bank.list',compact('collection'));
        }
        return $this->render_view('hrm.bank.main',compact('collection'));
    }
    public function create()
    {
        $bank = Bank::get();
        return $this->render_view('hrm.bank.input',['data' => new EmployeeBankAccount,'bank' => $bank]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bank' => 'required',
            'code' => 'required|unique:employee_bank_accounts,code',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('bank')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('bank'),
                ]);
            }elseif ($errors->has('code')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code'),
                ]);
            }
        }
        $employeeBankAccount = new EmployeeBankAccount;
        $employeeBankAccount->bank_id = $request->bank;
        $employeeBankAccount->code = $request->code;
        $employeeBankAccount->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function show(EmployeeBankAccount $employeeBankAccount)
    {
        //
    }
    public function edit(EmployeeBankAccount $employeeBankAccount)
    {
        //
    }
    public function update(Request $request, EmployeeBankAccount $employeeBankAccount)
    {
        //
    }
    public function destroy(EmployeeBankAccount $employeeBankAccount)
    {
        $employeeBankAccount->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil dihapus',
        ]);
    }
}
