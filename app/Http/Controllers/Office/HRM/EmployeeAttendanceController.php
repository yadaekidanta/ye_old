<?php

namespace App\Http\Controllers\Office\HRM;

use Illuminate\Support\Str;
use App\Models\HRM\Employee;
use Illuminate\Http\Request;
use App\Traits\OfficeResponseView;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\HRM\EmployeeAttendance;

class EmployeeAttendanceController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keyword = $request->keyword;
            $explode_month = str::beforeLast($request->keyword,'-');
            $month = str::after($explode_month,'-');
            $year = str::before($request->keyword,'-');
            $date1 = date($year.'-'.$month.'-'.'01');
            $date2 = date("Y-m-d");

            $diff = abs(strtotime($date2) - strtotime($date1));

            $years = floor($diff/(365*60*60*24));
            $months = floor(($diff-$years*365*60*60*24)/(30*60*60*24));
            $total_month = number_format($months,0)+1;
            // dd($total_month);
            $days = floor(($diff-$years*365*60*60*24-$months*30*60*60*24)/(60*60*24));
            $days = DB::select(DB::raw(" 
                SELECT
                    DATE_FORMAT(cal.my_date, '%e %b %Y') as tanggal,
                    COUNT( t.id ) AS total 
                FROM
                    (
                    SELECT
                        s.start_date + INTERVAL ( days.d ) DAY AS my_date 
                    FROM
                        ( SELECT LAST_DAY( CURRENT_DATE ) + INTERVAL 1 DAY - INTERVAL $total_month MONTH AS start_date, LAST_DAY( CURRENT_DATE ) AS end_date ) AS s
                    LEFT JOIN days ON days.d <= DATEDIFF( s.end_date, s.start_date ) ) AS cal LEFT JOIN employee_attendances AS t ON DATE_FORMAT(presence_at, '%Y-%m-%d') >= cal.my_date 
                    AND DATE_FORMAT(presence_at, '%Y-%m-%d') < cal.my_date + INTERVAL 1 DAY 
                WHERE
                    MONTH ( my_date ) = '$month' AND YEAR(my_date) = '$year'
                GROUP BY
                    tanggal
                ORDER BY
                    LENGTH(tanggal), tanggal
            "));
            $arr = array();
            foreach($days as $item){
                $temp=array(
                    "tanggal"=>$item->tanggal,
                    "total"=>$item->total
                );
                array_push($arr,$temp);
            }
            $day = json_encode($arr);
            $collection = Employee::get();
            return $this->render_view('hrm.attendance.list',compact('collection','days','keyword'));
        }
        return $this->render_view('hrm.attendance.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(EmployeeAttendance $employeeAttendance)
    {
        //
    }
    public function edit(EmployeeAttendance $employeeAttendance)
    {
        //
    }
    public function update(Request $request, EmployeeAttendance $employeeAttendance)
    {
        //
    }
    public function destroy(EmployeeAttendance $employeeAttendance)
    {
        //
    }
}
