<?php

namespace App\Http\Controllers\Office\HRM;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\HRM\Department;
use App\Models\HRM\VacancyJob;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\HRM\JobApplication;
use Illuminate\Support\Facades\Validator;

class VacancyJobController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if($request->ajax()){
            $collection = VacancyJob::where('title','LIKE','%'.$request->keyword.'%')->paginate(10);
            return $this->render_view('hrm.vacancy_job.list', compact('collection'));
        }
        return $this->render_view('hrm.vacancy_job.main');
    }
    public function create()
    {
        $department = Department::get();
        return $this->render_view('hrm.vacancy_job.input', ['data' => new VacancyJob, 'department' => $department]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'department' => 'required',
            'position' => 'required',
            'title' => 'required',
            'description' => 'required',
            'requirement' => 'required',
            'facilities' => 'required',
            'rates' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('department')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('department'),
                ]);
            }elseif ($errors->has('position')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('position'),
                ]);
            }elseif ($errors->has('title')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('title'),
                ]);
            }elseif ($errors->has('description')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('description'),
                ]);
            }elseif ($errors->has('requirement')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('requirement'),
                ]);
            }elseif ($errors->has('facilities')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('facilities'),
                ]);
            }elseif ($errors->has('rates')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('rates'),
                ]);
            }
        }
        $vacancyJob = new VacancyJob;
        $vacancyJob->department_id = $request->department;
        $vacancyJob->position_id = $request->position;
        $vacancyJob->title = $request->title;
        $vacancyJob->slug = Str::slug($request->title);
        $vacancyJob->description = $request->description;
        $vacancyJob->requirement = $request->requirement;
        $vacancyJob->facilities = $request->facilities;
        $vacancyJob->rates = Str::remove(',',$request->rates);
        $vacancyJob->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Vacancy Job '. __('custom.saved'),
        ]);
    }
    public function show(Request $request,$vacancyJob)
    {
        if ($request->ajax()) {
            $collection = JobApplication::where('vacancy_id',$vacancyJob)->paginate(10);
            return $this->render_view('hrm.vacancy_job.show_list',compact('collection'));
        }
        return $this->render_view('hrm.vacancy_job.show', ['data' => $vacancyJob]);
    }
    public function edit(VacancyJob $vacancy)
    {
        $department = Department::get();
        return $this->render_view('hrm.vacancy_job.input', ['data' => $vacancy, 'department' => $department]);
    }
    public function update(Request $request, VacancyJob $vacancyJob)
    {
        $validator = Validator::make($request->all(), [
            'department' => 'required',
            'position' => 'required',
            'title' => 'required',
            'description' => 'required',
            'requirement' => 'required',
            'facilities' => 'required',
            'rates' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('department')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('department'),
                ]);
            }elseif ($errors->has('position')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('position'),
                ]);
            }elseif ($errors->has('title')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('title'),
                ]);
            }elseif ($errors->has('description')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('description'),
                ]);
            }elseif ($errors->has('requirement')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('requirement'),
                ]);
            }elseif ($errors->has('facilities')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('facilities'),
                ]);
            }elseif ($errors->has('rates')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('rates'),
                ]);
            }
        }
        $vacancyJob->department_id = $request->department;
        $vacancyJob->position_id = $request->position;
        $vacancyJob->title = $request->title;
        $vacancyJob->slug = Str::slug($request->title);
        $vacancyJob->description = $request->description;
        $vacancyJob->requirement = $request->requirement;
        $vacancyJob->facilities = $request->facilities;
        $vacancyJob->rates = $request->rates;
        $vacancyJob->st = $request->st;
        $vacancyJob->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Vacancy Job '. __('custom.saved'),
        ]);
    }
    public function destroy(VacancyJob $vacancyJob)
    {
        $vacancyJob->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Vacancy Job '. __('custom.deleted'),
        ]);
    }
    public function open(VacancyJob $vacancy)
    {
        $vacancy->st = 'Open';
        $vacancy->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Vacancy Job Open',
        ]);
    }
    public function close(VacancyJob $vacancy)
    {
        $vacancy->st = 'Closed';
        $vacancy->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Vacancy Job Closed',
        ]);
    }
}
