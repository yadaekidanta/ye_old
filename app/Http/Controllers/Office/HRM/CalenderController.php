<?php

namespace App\Http\Controllers\Office\HRM;

use Carbon\Carbon;
use App\Models\HRM\DayOff;
use App\Models\HRM\Employee;
use Illuminate\Http\Request;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use GrahamCampbell\ResultType\Success;
use Illuminate\Support\Facades\Validator;

class CalenderController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = DayOff::where('st', 'Approved')->get();
            $data = $data->map(function ($item) {
                return [
                    'id' => $item->id,
                    'title' => $item->employee->name,
                    'start' => Carbon::parse($item->start)->format('Y-m-d'),
                    'end' => Carbon::parse($item->start)->addDay($item->amount)->format('Y-m-d'),
                    'description' => $item->reason,
                ];
            });
            return response()->json($data);
        }
        return $this->render_view('hrm.calender.main');
    }

    public function create()
    {
        $employees = Employee::get();
        return $this->render_view('hrm.calender.input', ['data' => new DayOff, 'employees' => $employees]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'employee_id' => 'required',
            'start' => 'required',
            'amount' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('employee')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('employee'),
                ]);
            } elseif ($errors->has('start')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('start'),
                ]);
            } elseif ($errors->has('amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('amount'),
                ]);
            }
        }

        $data = new DayOff;
        $data->employee_id = $request->employee_id;
        $data->date = $request->start;
        $data->amount = $request->amount;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil ditambahkan'
        ]);
    }

    public function show($id)
    {
        $data = DayOff::find($id);
        return $this->render_view('hrm.calender.show', compact('data'));
    }

    public function edit($id)
    {
        $data = DayOff::find($id);
        $employees = Employee::get();
        return $this->render_view('hrm.calender.input', compact('data', 'employees'));
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'employee_id' => 'required',
            'start' => 'required',
            'amount' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('employee')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('employee'),
                ]);
            } elseif ($errors->has('start')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('start'),
                ]);
            } elseif ($errors->has('amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('amount'),
                ]);
            }
        }
        $data = DayOff::find($id);
        $data->employee_id = $request->employee_id;
        $data->date = $request->start;
        $data->amount = $request->amount;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil diubah'
        ]);
    }


    public function destroy($id)
    {
        DayOff::find($id)->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil dihapus'
        ]);
    }
}
