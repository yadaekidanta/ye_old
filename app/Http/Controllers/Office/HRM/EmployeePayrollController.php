<?php

namespace App\Http\Controllers\Office\HRM;

use App\Models\HRM\Employee;
use Illuminate\Http\Request;
use App\Traits\OfficeResponseView;
use App\Models\HRM\EmployeePayroll;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class EmployeePayrollController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = EmployeePayroll::get();
            return $this->render_view('hrm.payroll.list',compact('collection'));
        }
        return $this->render_view('hrm.payroll.main');
    }
    public function create()
    {
        $employee = Employee::get();
        return $this->render_view('hrm.payroll.input', ['data' => new EmployeePayroll, 'employee' => $employee]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'employee' => 'required',
            'bank_account' => 'required',
            'amount' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('employee')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('employee'),
                ]);
            }elseif ($errors->has('bank_account')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('bank_account'),
                ]);
            }elseif ($errors->has('amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('amount'),
                ]);
            }
        }
        $employeePayroll = new EmployeePayroll;
        $employeePayroll->employee_id = $request->employee;
        $employeePayroll->employee_bank_account_id = $request->bank_account;
        $employeePayroll->amount = $request->amount;
        if(request()->file('file')){
            $file = request()->file('file')->store("employee_payroll");
            $employeePayroll->file = $file;
        }
        $employeePayroll->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Employee Payroll '. __('custom.saved'),
        ]);
    }
    public function show(EmployeePayroll $employeePayroll)
    {
        //
    }
    public function edit(EmployeePayroll $employeePayroll)
    {
        $employee = Employee::get();
        return $this->render_view('hrm.payroll.input', ['data' => $employeePayroll, 'employee' => $employee]);
    }
    public function update(Request $request, EmployeePayroll $employeePayroll)
    {
        $validator = Validator::make($request->all(), [
            'employee' => 'required',
            'bank_account' => 'required',
            'amount' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('employee')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('employee'),
                ]);
            }elseif ($errors->has('bank_account')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('bank_account'),
                ]);
            }elseif ($errors->has('amount')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('amount'),
                ]);
            }
        }
        $employeePayroll->employee_id = $request->employee;
        $employeePayroll->employee_bank_account_id = $request->bank_account;
        $employeePayroll->amount = $request->amount;
        if(request()->file('file')){
            Storage::delete($employeePayroll->file);
            $file = request()->file('file')->store("employee_payroll");
            $employeePayroll->file = $file;
        }
        $employeePayroll->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Employee Payroll '. __('custom.saved'),
        ]);
    }
    public function destroy(EmployeePayroll $employeePayroll)
    {
        $employeePayroll->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Employee Payroll '. __('custom.deleted'),
        ]);
    }
}
