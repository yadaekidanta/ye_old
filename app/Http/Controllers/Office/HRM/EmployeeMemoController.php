<?php

namespace App\Http\Controllers\Office\HRM;

use Illuminate\Http\Request;
use App\Models\HRM\EmployeeMemo;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class EmployeeMemoController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        $collection = EmployeeMemo::where('employee_id', Auth::guard('office')->user()->id)->get();
        return $this->render_view('hrm.memo.list', compact('collection'));
        // return $this->render_view('hrm.memo.main');
    }
    public function create()
    {
        return $this->render_view('hrm.memo.input', ['data' => new EmployeeMemo]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'body' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('title')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('title'),
                ]);
            } elseif ($errors->has('body')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('body'),
                ]);
            }
        }
        $employeeMemo = new EmployeeMemo;
        $employeeMemo->employee_id = Auth::guard('office')->user()->id;
        $employeeMemo->title = $request->title;
        $employeeMemo->body = $request->body;
        $employeeMemo->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Memo ' . __('custom.saved'),
        ]);
    }
    public function show(EmployeeMemo $employeeMemo)
    {
        return $this->render_view('hrm.memo.show', ['data' => $employeeMemo]);
    }
    public function edit(EmployeeMemo $employeeMemo)
    {
        return $this->render_view('hrm.memo.input', ['data' => $employeeMemo]);
    }
    public function update(Request $request, EmployeeMemo $employeeMemo)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'body' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('title')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('title'),
                ]);
            } elseif ($errors->has('body')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('body'),
                ]);
            }
        }
        $employeeMemo->title = $request->title;
        $employeeMemo->body = $request->body;
        $employeeMemo->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Memo ' . __('custom.saved'),
        ]);
    }
    public function destroy(EmployeeMemo $employeeMemo)
    {
        $employeeMemo->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Memo ' . __('custom.deleted'),
        ]);
    }
}
