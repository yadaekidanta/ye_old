<?php

namespace App\Http\Controllers\Office\HRM;

use App\Models\HRM\Employee;
use Illuminate\Http\Request;
use App\Models\HRM\EmployeeBpjs;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class EmployeeBpjsController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = EmployeeBpjs::where('code','LIKE','%'.$request->keyword.'%')->paginate(10);
            return $this->render_view('hrm.employee_bpjs.list',compact('collection'));
        }
        return $this->render_view('hrm.employee_bpjs.main');
    }
    public function create()
    {
        $employee = Employee::get();
        return $this->render_view('hrm.employee_bpjs.input', ['data' => new EmployeeBpjs, 'employee' => $employee]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'employee' => 'required|unique:employee_bpjs,employee_id',
            'code' => 'required|unique:employee_bpjs,code',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('employee')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('employee'),
                ]);
            }elseif ($errors->has('code')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code'),
                ]);
            }
        }
        $employeeBpjs = new EmployeeBpjs;
        $employeeBpjs->employee_id = $request->employee;
        $employeeBpjs->code = $request->code;
        $employeeBpjs->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function show(EmployeeBpjs $employeeBpjs)
    {
        //
    }
    public function edit(EmployeeBpjs $employeeBpjs)
    {
        $employee = Employee::get();
        return $this->render_view('hrm.employee_bpjs.input', ['data' => $employeeBpjs, 'employee' => $employee]);
    }
    public function update(Request $request, EmployeeBpjs $employeeBpjs)
    {
        $validator = Validator::make($request->all(), [
            'employee' => 'required|unique:employee_bpjs,employee_id,'.$employeeBpjs->id,
            'code' => 'required|unique:employee_bpjs,code,'.$employeeBpjs->id,
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('employee')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('employee'),
                ]);
            }elseif ($errors->has('code')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code'),
                ]);
            }
        }
        $employeeBpjs->employee_id = $request->employee;
        $employeeBpjs->code = $request->code;
        $employeeBpjs->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function destroy(EmployeeBpjs $employeeBpjs)
    {
        return response()->json([
            'alert' => 'info',
            'message' => 'Data tidak dapat dihapus',
        ]);
    }
}
