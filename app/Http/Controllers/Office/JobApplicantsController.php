<?php

namespace App\Http\Controllers\Office;

use App\Models\HRM\Employee;
use App\Models\HRM\Position;
use Illuminate\Http\Request;
use App\Models\Regional\City;
use App\Models\HRM\Department;
use App\Models\Regional\Province;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\HRM\JobApplication;
use App\Models\HRM\VacancyJob;
use App\Models\Regional\Subdistrict;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class JobApplicantsController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = JobApplication::where('employee_id','=',Auth::guard('office')->user()->id)->paginate(10);
            return $this->render_view('job_applicants.list',compact('collection'));
        }
        return $this->render_view('job_applicants.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        $job = JobApplication::where('vacancy_id', '=',$request->id)->where('employee_id',Auth::guard('office')->user()->id)->first();
        $validator = Validator::make($request->all(), [
            'expected_salary ' => 'required',
            'message' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('expected_salary')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('expected_salary'),
                ]);
            }elseif ($errors->has('message')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('message'),
                ]);
            }
        }
        $jobapplicants = new JobApplication;
        $jobapplicants->vacancy_id = $request->id;
        $jobapplicants->employee_id = Auth::guard('office')->user()->id;
        $jobapplicants->expected_salary = $request->expected_salary;
        $jobapplicants->messages = $request->message;
        $jobapplicants->st = 'p';

        if($job){
            return response()->json([
                'alert' => 'error',
                'message' => 'You Have Done Submit Your Applicants',
            ]);
        }else{
            $jobapplicants->save();
            return response()->json([
                'alert' => 'success',
                'message' => 'Your Job Applicants Has Been Sent',
            ]);
        }
        
    }
    public function show(JobApplication $jobapplicant)
    {
        // return $this->render_view('jobapplicants.show', ['data' => $employee]);
    }
    public function edit(VacancyJob $my_applicant)
    {
        return $this->render_view('job_applicants.input', ['data' => $my_applicant]);
    }
    public function update(Request $request, JobApplication $jobapplicant)
    {
       //
    }
    public function destroy(JobApplication $jobapplicant)
    {
        //
    }
    public function upload_cv(Request $request){
        //
    }
}
