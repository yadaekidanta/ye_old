<?php

namespace App\Http\Controllers\Office\Legality;

use Illuminate\Http\Request;
use App\Models\Legality\LegalDoc;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\Legality\LegalDocType;
use Illuminate\Support\Facades\Validator;

class LegalDocTypeController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = LegalDocType::where('name','LIKE','%'.$request->keyword.'%')->paginate(10);
            return $this->render_view('legality.type.list',compact('collection'));
        }
        return $this->render_view('legality.type.main');
    }
    public function create()
    {
        return $this->render_view('legality.type.input',['data' => new LegalDocType]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:legal_doc_types,name',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $legalDocType = new LegalDocType;
        $legalDocType->name = $request->name;
        $legalDocType->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function show(LegalDocType $legalDocType)
    {
        //
    }
    public function edit(LegalDocType $legalDocType)
    {
        return $this->render_view('legality.type.input',['data' => $legalDocType]);
    }
    public function update(Request $request, LegalDocType $legalDocType)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:legal_doc_types,name,'.$legalDocType->id,
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $legalDocType->name = $request->name;
        $legalDocType->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil diubah',
        ]);
    }
    public function destroy(LegalDocType $legalDocType)
    {
        $legal = LegalDoc::where('doc_type_id',$legalDocType->id)->get();
        foreach ($legal as $key => $value) {
            $value->doc_type_id = null;
            $value->update();
        }
        $legalDocType->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil dihapus',
        ]);
    }
}
