<?php

namespace App\Http\Controllers\Office\Legality;

use Illuminate\Http\Request;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\Legality\ReferenceNumber;
use Illuminate\Support\Facades\Validator;

class ReferenceNumberController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = ReferenceNumber::where('name','LIKE','%'.$request->keyword.'%')->paginate(10);
            return $this->render_view('legality.reference.list',compact('collection'));
        }
        return $this->render_view('legality.reference.main');
    }
    public function create()
    {
        return $this->render_view('legality.reference.input', ['data' => new ReferenceNumber]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|unique:reference_numbers',
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('code')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code'),
                ]);
            }elseif ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $referenceNumber = new ReferenceNumber;
        $referenceNumber->code = $request->code;
        $referenceNumber->name = $request->name;
        $referenceNumber->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function show(ReferenceNumber $referenceNumber)
    {
        //
    }
    public function edit(ReferenceNumber $referenceNumber)
    {
        return $this->render_view('legality.reference.input', ['data' => $referenceNumber]);
    }
    public function update(Request $request, ReferenceNumber $referenceNumber)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|unique:reference_numbers,code,'.$referenceNumber->id,
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('code')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code'),
                ]);
            }elseif ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $referenceNumber->code = $request->code;
        $referenceNumber->name = $request->name;
        $referenceNumber->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil diubah',
        ]);
    }
    public function destroy(ReferenceNumber $referenceNumber)
    {
        $referenceNumber->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil dihapus',
        ]);
    }
}
