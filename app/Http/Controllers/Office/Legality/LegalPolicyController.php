<?php

namespace App\Http\Controllers\Office\Legality;

use Illuminate\Http\Request;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\Legality\LegalPolicy;
use Illuminate\Support\Facades\Validator;

class LegalPolicyController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = LegalPolicy::where('title','LIKE','%'.$request->keyword.'%')->paginate(10);
            return $this->render_view('legality.policy.list',compact('collection'));
        }
        return $this->render_view('legality.policy.main');
    }
    public function create()
    {
        return $this->render_view('legality.policy.input', ['data' => new LegalPolicy]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:legal_policies,title',
            'body' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('title')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('title'),
                ]);
            }elseif ($errors->has('body')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('body'),
                ]);
            }
        }
        $legalPolicy = new LegalPolicy;
        $legalPolicy->title = $request->title;
        $legalPolicy->body = $request->body;
        $legalPolicy->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function show(LegalPolicy $legalPolicy)
    {
        //
    }
    public function edit(LegalPolicy $legalPolicy)
    {
        return $this->render_view('legality.policy.input', ['data' => $legalPolicy]);
    }
    public function update(Request $request, LegalPolicy $legalPolicy)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:legal_policies,title,'.$legalPolicy->id,
            'body' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('title')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('title'),
                ]);
            }elseif ($errors->has('body')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('body'),
                ]);
            }
        }
        $legalPolicy->title = $request->title;
        $legalPolicy->body = $request->body;
        $legalPolicy->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil diubah',
        ]);
    }
    public function destroy(LegalPolicy $legalPolicy)
    {
        $legalPolicy->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil dihapus',
        ]);
    }
}
