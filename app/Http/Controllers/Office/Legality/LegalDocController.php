<?php

namespace App\Http\Controllers\Office\Legality;

use Illuminate\Http\Request;
use App\Models\Legality\LegalDoc;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\Legality\LegalDocType;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class LegalDocController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = LegalDoc::paginate(10);
            return $this->render_view('legality.doc.list',compact('collection'));
        }
        return $this->render_view('legality.doc.main');
    }
    public function create()
    {
        $type = LegalDocType::get();
        return $this->render_view('legality.doc.input',['data' => new LegalDoc,'type' => $type]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required',
            'code' => 'required|unique:legal_docs,code',
            'attachment' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('type')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('type'),
                ]);
            }elseif ($errors->has('code')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code'),
                ]);
            }elseif ($errors->has('attachment')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('attachment'),
                ]);
            }
        }
        $legalDoc = new LegalDoc;
        $legalDoc->doc_type_id = $request->type;
        $legalDoc->code = $request->code;
        if(request()->file('attachment')){
            $attachment = request()->file('attachment')->store("legal_doc");
            $legalDoc->attachment = $attachment;
        }
        $legalDoc->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function show(LegalDoc $legalDoc)
    {
        //
    }
    public function edit(LegalDoc $legalDoc)
    {
        $type = LegalDocType::get();
        return $this->render_view('legality.doc.input',['data' => $legalDoc,'type' => $type]);
    }
    public function update(Request $request, LegalDoc $legalDoc)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required',
            'code' => 'required|unique:legal_docs,code,'.$legalDoc->id,
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('type')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('type'),
                ]);
            }elseif ($errors->has('code')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code'),
                ]);
            }
        }
        $legalDoc->doc_type_id = $request->type;
        $legalDoc->code = $request->code;
        if(request()->file('attachment')){
            Storage::delete($legalDoc->attachment);
            $attachment = request()->file('attachment')->store("legal_doc");
            $legalDoc->attachment = $attachment;
        }
        $legalDoc->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil diubah',
        ]);
    }
    public function destroy(LegalDoc $legalDoc)
    {
        $legalDoc->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil dihapus',
        ]);
    }
}
