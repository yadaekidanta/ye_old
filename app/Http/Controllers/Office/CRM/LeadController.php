<?php

namespace App\Http\Controllers\Office\CRM;

use App\Models\CRM\Lead;
use Illuminate\Http\Request;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LeadController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = Lead::where('name','LIKE','%'.$request->keyword.'%')->orWhere('email','LIKE','%'.$request->keyword.'%')->orWhere('phone','LIKE','%'.$request->keyword.'%')->paginate(10);
            return $this->render_view('crm.lead.list',compact('collection'));
        }
        return $this->render_view('crm.lead.main');
    }
    public function create()
    {
        return $this->render_view('crm.lead.input', ['data' => new Lead]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:leads,name',
            'email' => 'required|unique:leads,email|email',
            'phone' => 'required|unique:leads,phone|max:13',
            'referral' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('phone')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }elseif ($errors->has('referral')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('referral'),
                ]);
            }
        }
        $data = new Lead;
        $data->employee_id = Auth::guard('office')->user()->id;
        $data->name = $request->name;
        $data->phone = $request->phone;
        $data->lead_referral_source = $request->referral;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Leads '. __('custom.saved'),
            'redirect' => 'input',
            'route' => route('office.lead.show',$data->id),
        ]);
    }
    public function show(Lead $lead)
    {
        return $this->render_view('crm.lead.show', ['data' => $lead]);
    }
    public function edit(Lead $lead)
    {
        return $this->render_view('crm.lead.input', ['data' => $lead]);
    }
    public function update(Request $request, Lead $lead)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:leads,name,'.$lead->id,
            'email' => 'unique:leads,email|email,'.$lead->id,
            'phone' => 'unique:leads,phone|max:13,'.$lead->id,
            'referral' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('phone')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }elseif ($errors->has('referral')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('referral'),
                ]);
            }
        }
        $lead->employee_id = Auth::guard('office')->user()->id;
        $lead->name = $request->name;
        $lead->phone = $request->phone;
        $lead->lead_referral_source = $request->referral;
        $lead->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Leads '. __('custom.saved'),
            'redirect' => 'input',
            'route' => route('office.lead.show',$lead->id),
        ]);
    }
    public function destroy(Lead $lead)
    {
        $lead->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Lead '. __('custom.deleted'),
        ]);
    }
    public function destroy_checked(Request $request)
    {
        foreach($request->id as $id){
            $leads = Lead::find($id);
            $leads->delete();
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Leads '. __('custom.deleted'),
        ]);
    }
}
