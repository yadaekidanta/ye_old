<?php

namespace App\Http\Controllers\Office\CRM;

use Illuminate\Http\Request;
use App\Models\CRM\ClientActivity;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ClientActivityController extends Controller
{
    use OfficeResponseView;
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'notes' => 'required',
            'type' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('date')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('date'),
                ]);
            }elseif ($errors->has('notes')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('notes'),
                ]);
            }elseif ($errors->has('type')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('type'),
                ]);
            }elseif ($errors->has('status')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('status'),
                ]);
            }
        }
        $data = new ClientActivity;
        $data->employee_id = Auth::guard('office')->user()->id;
        $data->client_id = $request->lead_id;
        $data->date = $request->date;
        $data->notes = $request->notes;
        $data->type = $request->type;
        $data->st = $request->status;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Client activity '. __('custom.saved'),
            'redirect' => 'input',
            'route' => route('office.client.show',$data->client_id),
        ]);
    }
    public function show(ClientActivity $clientActivity)
    {
        //
    }
    public function edit(ClientActivity $clientActivity)
    {
        //
    }
    public function update(Request $request, ClientActivity $clientActivity)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'notes' => 'required',
            'type' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('date')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('date'),
                ]);
            }elseif ($errors->has('notes')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('notes'),
                ]);
            }elseif ($errors->has('type')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('type'),
                ]);
            }elseif ($errors->has('status')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('status'),
                ]);
            }
        }
        $clientActivity->employee_id = Auth::guard('office')->user()->id;
        $clientActivity->date = $request->date;
        $clientActivity->notes = $request->notes;
        $clientActivity->type = $request->type;
        $clientActivity->st = $request->status;
        $clientActivity->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Client activity '. __('custom.saved'),
            'redirect' => 'input',
            'route' => route('office.client.show',$clientActivity->client_id),
        ]);
    }
    public function destroy(ClientActivity $clientActivity)
    {
        $id = $clientActivity->lead_id;
        $clientActivity->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Client activity '. __('custom.deleted'),
            'redirect' => 'input',
            'route' => route('office.client.show',$id),
        ]);
    }
}
