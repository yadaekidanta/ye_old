<?php

namespace App\Http\Controllers\Office\CRM;

use Illuminate\Http\Request;
use App\Models\CRM\ProjectSpend;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;

class ProjectSpendController extends Controller
{
    use OfficeResponseView;
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(ProjectSpend $projectSpend)
    {
        //
    }
    public function edit(ProjectSpend $projectSpend)
    {
        //
    }
    public function update(Request $request, ProjectSpend $projectSpend)
    {
        //
    }
    public function destroy(ProjectSpend $projectSpend)
    {
        //
    }
}
