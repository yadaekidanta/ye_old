<?php

namespace App\Http\Controllers\Office\CRM;

use Illuminate\Http\Request;
use App\Models\CRM\LeadActivity;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LeadActivityController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'notes' => 'required',
            'type' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('date')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('date'),
                ]);
            }elseif ($errors->has('notes')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('notes'),
                ]);
            }elseif ($errors->has('type')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('type'),
                ]);
            }elseif ($errors->has('status')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('status'),
                ]);
            }
        }
        $data = new LeadActivity;
        $data->employee_id = Auth::guard('office')->user()->id;
        $data->lead_id = $request->lead_id;
        $data->date = $request->date;
        $data->notes = $request->notes;
        $data->type = $request->type;
        $data->st = $request->status;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Lead activity '. __('custom.saved'),
            'redirect' => 'input',
            'route' => route('office.lead.show',$data->lead_id),
        ]);
    }
    public function show(LeadActivity $leadActivity)
    {
        //
    }
    public function edit(LeadActivity $leadActivity)
    {
        //
    }
    public function update(Request $request, LeadActivity $leadActivity)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'notes' => 'required',
            'type' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('date')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('date'),
                ]);
            }elseif ($errors->has('notes')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('notes'),
                ]);
            }elseif ($errors->has('type')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('type'),
                ]);
            }elseif ($errors->has('status')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('status'),
                ]);
            }
        }
        $leadActivity->employee_id = Auth::guard('office')->user()->id;
        $leadActivity->date = $request->date;
        $leadActivity->notes = $request->notes;
        $leadActivity->type = $request->type;
        $leadActivity->st = $request->status;
        $leadActivity->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Lead activity '. __('custom.saved'),
            'redirect' => 'input',
            'route' => route('office.lead.show',$leadActivity->lead_id),
        ]);
    }
    public function destroy(LeadActivity $leadActivity)
    {
        $id = $leadActivity->lead_id;
        $leadActivity->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Lead activity '. __('custom.deleted'),
            'redirect' => 'input',
            'route' => route('office.lead.show',$id),
        ]);
    }
}
