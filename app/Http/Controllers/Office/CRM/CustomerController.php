<?php

namespace App\Http\Controllers\Office\CRM;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\CRM\Client AS Customer;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = Customer::where('name','LIKE','%'.$request->keyword.'%')->where('type','Customer')->paginate(10);
            return $this->render_view('crm.customer.list',compact('collection'));
        }
        return $this->render_view('crm.customer.main');
    }
    public function create()
    {
        return $this->render_view('crm.customer.input', ['data' => new Customer]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:clients,name',
            'email' => 'required|unique:clients,email|email',
            'phone' => 'required|unique:clients,phone|max:13',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('phone')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }
        }
        $data = new Customer;
        $data->employee_id = Auth::guard('office')->user()->id;
        $data->name = $request->name;
        $data->username = Str::lower(Str::before($request->email, '@'));
        $data->phone = $request->phone;
        $data->email = Str::lower($request->email);
        $data->email_verified_at = date('Y-m-d H:i:s');
        $data->password = Hash::make($request->password);
        $data->type = 'Customer';
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Customer '. __('custom.saved'),
        ]);
    }
    public function show(Customer $customer)
    {
        return $this->render_view('crm.customer.show', ['data' => $customer]);
    }
    public function edit(Customer $customer)
    {
        return $this->render_view('crm.customer.input', ['data' => $customer]);
    }
    public function update(Request $request, Customer $customer)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:clients,name,'.$customer->id,
            'email' => 'unique:clients,email|email,'.$customer->id,
            'phone' => 'unique:clients,phone|max:13,'.$customer->id,
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('phone')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }
        }
        $customer->employee_id = Auth::guard('office')->user()->id;
        $customer->name = $request->name;
        $customer->phone = $request->phone;
        if($request->email){
            $customer->username = Str::lower(Str::before($request->email, '@'));
            $customer->email = Str::lower($request->email);
            $customer->email_verified_at = date('Y-m-d H:i:s');
        }
        if($request->password){
            $customer->password = Hash::make($request->password);
        }
        $customer->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Customer '. __('custom.saved'),
        ]);
    }
    public function destroy(Customer $customer)
    {
        $customer->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Customer '. __('custom.deleted'),
        ]);
    }
    public function destroy_checked(Request $request)
    {
        foreach($request->id as $id){
            $customer = Customer::find($id);
            if($customer->avatar){
                Storage::delete($customer->avatar);
            }
            if($customer->company_logo){
                Storage::delete($customer->company_logo);
            }
            $customer->delete();
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Customer '. __('custom.deleted'),
        ]);
    }
}
