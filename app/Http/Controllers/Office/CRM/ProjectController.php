<?php

namespace App\Http\Controllers\Office\CRM;

use App\Models\CRM\Project;
use Illuminate\Http\Request;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = Project::where('title','LIKE','%'.$request->keyword.'%')->paginate(10);
            return $this->render_view('crm.project.list',compact('collection'));
        }
        return $this->render_view('crm.project.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Project $project)
    {
        //
    }
    public function edit(Project $project)
    {
        //
    }
    public function update(Request $request, Project $project)
    {
        //
    }
    public function destroy(Project $project)
    {
        //
    }
}
