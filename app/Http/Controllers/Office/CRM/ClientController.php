<?php

namespace App\Http\Controllers\Office\CRM;

use App\Models\CRM\Client;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = Client::where('name','LIKE','%'.$request->keyword.'%')->where('type','Client')->paginate(10);
            return $this->render_view('crm.client.list',compact('collection'));
        }
        return $this->render_view('crm.client.main');
    }
    public function create()
    {
        return $this->render_view('crm.client.input', ['data' => new Client]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:clients,name',
            'email' => 'required|unique:clients,email|email',
            'phone' => 'required|unique:clients,phone|max:13',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('phone')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }
        }
        $data = new Client;
        $data->employee_id = Auth::guard('office')->user()->id;
        $data->name = $request->name;
        $data->username = Str::lower(Str::before($request->email, '@'));
        $data->phone = $request->phone;
        $data->email = Str::lower($request->email);
        $data->email_verified_at = date('Y-m-d H:i:s');
        $data->password = Hash::make($request->password);
        $data->type = 'Client';
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Client '. __('custom.saved'),
        ]);
    }
    public function show(Client $client)
    {
        return $this->render_view('crm.client.show', ['data' => $client]);
    }
    public function edit(Client $client)
    {
        return $this->render_view('crm.client.input', ['data' => $client]);
    }
    public function update(Request $request, Client $client)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:clients,name,'.$client->id,
            'email' => 'unique:clients,email|email,'.$client->id,
            'phone' => 'unique:clients,phone|max:13,'.$client->id,
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('phone')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }
        }
        $client->employee_id = Auth::guard('office')->user()->id;
        $client->name = $request->name;
        $client->phone = $request->phone;
        if($request->email){
            $client->username = Str::lower(Str::before($request->email, '@'));
            $client->email = Str::lower($request->email);
            $client->email_verified_at = date('Y-m-d H:i:s');
        }
        if($request->password){
            $client->password = Hash::make($request->password);
        }
        $client->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Client '. __('custom.saved'),
        ]);
    }
    public function destroy(Client $client)
    {
        $client->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Client '. __('custom.deleted'),
        ]);
    }
    public function destroy_checked(Request $request)
    {
        foreach($request->id as $id){
            $client = Client::find($id);
            if($client->avatar){
                Storage::delete($client->avatar);
            }
            if($client->company_logo){
                Storage::delete($client->company_logo);
            }
            $client->delete();
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Client '. __('custom.deleted'),
        ]);
    }
}
