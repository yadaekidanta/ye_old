<?php

namespace App\Http\Controllers\Office\CRM;

use Illuminate\Http\Request;
use App\Models\CRM\Newsletter;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;

class NewsletterController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = Newsletter::where('email','LIKE','%'.$request->keyword.'%')->paginate(10);
            return $this->render_view('crm.newsletter.list',compact('collection'));
        }
        return $this->render_view('crm.newsletter.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Newsletter $newsletter)
    {
        //
    }
    public function edit(Newsletter $newsletter)
    {
        //
    }
    public function update(Request $request, Newsletter $newsletter)
    {
        //
    }
    public function destroy(Newsletter $newsletter)
    {
        //
    }
}
