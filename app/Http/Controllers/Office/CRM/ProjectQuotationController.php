<?php

namespace App\Http\Controllers\Office\CRM;

use Illuminate\Http\Request;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\CRM\ProjectQuotation;

class ProjectQuotationController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = ProjectQuotation::where('name','LIKE','%'.$request->keyword.'%')->paginate(10);
            return $this->render_view('crm.project_quotation.list',compact('collection'));
        }
        return $this->render_view('crm.project_quotation.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(ProjectQuotation $projectQuotation)
    {
        //
    }
    public function edit(ProjectQuotation $projectQuotation)
    {
        //
    }
    public function update(Request $request, ProjectQuotation $projectQuotation)
    {
        //
    }
    public function destroy(ProjectQuotation $projectQuotation)
    {
        //
    }
}
