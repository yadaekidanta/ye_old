<?php

namespace App\Http\Controllers\Office\CRM;

use App\Models\CRM\Inbox;
use Illuminate\Http\Request;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;

class InboxController extends Controller
{
    use OfficeResponseView;
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Inbox $inbox)
    {
        //
    }
    public function edit(Inbox $inbox)
    {
        //
    }
    public function update(Request $request, Inbox $inbox)
    {
        //
    }
    public function destroy(Inbox $inbox)
    {
        //
    }
}
