<?php

namespace App\Http\Controllers\Office;

use App\Models\HRM\Employee;
use App\Models\HRM\Position;
use Illuminate\Http\Request;
use App\Models\Regional\City;
use App\Models\HRM\Department;
use App\Models\Regional\Province;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\Regional\Subdistrict;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // dd(Auth::guard('office')->user()->calculate_profile(Auth::guard('office')->user()));
            $collection = Auth::guard('office')->user();
            return $this->render_view('profile.list',compact('collection'));
        }
        return $this->render_view('profile.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Employee $employee)
    {
        // return $this->render_view('profile.show', ['data' => $employee]);
    }
    public function edit(Employee $employee)
    {
        $province = Province::get();
        $department = Department::get();
        return $this->render_view('profile.input', ['data' => $employee, 'province' => $province, 'department' => $department]);
    }
    public function update(Request $request, Employee $profile)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:employees,email,'.$profile->id,
            'phone' => 'required|unique:employees,phone,'.$profile->id,
            'date_birth' => 'required',
            'address' => 'required',
            'province' => 'required',
            'city' => 'required',
            'subdistrict' => 'required',
            'postcode' => 'required',
            'impression ' => 'required',
            'bio' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('bio')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('bio'),
                ]);
            }elseif ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('phone')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }elseif ($errors->has('date_birth')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('date_birth'),
                ]);
            }elseif ($errors->has('bio')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('bio'),
                ]);
            }elseif ($errors->has('impression')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('impression'),
                ]);
            }elseif ($errors->has('address')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('address'),
                ]);
            }elseif ($errors->has('province')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('province'),
                ]);
            }elseif ($errors->has('city')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('city'),
                ]);
            }elseif ($errors->has('subdistrict')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('subdistrict'),
                ]);
            }elseif ($errors->has('postcode')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('postcode'),
                ]);
            }elseif ($errors->has('avatar')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('avatar'),
                ]);
            }
        }
       
        $profile->name = $request->name;
        $profile->email = $request->email;
        $profile->phone = $request->phone;
        $profile->date_birth = $request->date_birth;
        $profile->bio = $request->bio;
        $profile->impression = $request->impression;
        $profile->address = $request->address;
        $profile->province_id = $request->province;
        $profile->city_id = $request->city;
        $profile->subdistrict_id = $request->subdistrict;
        $profile->postcode = $request->postcode;
        if(request()->file('avatar')){
            Storage::delete($profile->avatar);
            $avatar = request()->file('avatar')->store("avatar");
            $profile->avatar = $avatar;
        }
        if(request()->file('cv')){
            Storage::delete($profile->cv);
            $cv =  $request->file('cv')->store("cv");
            $profile->cv = $cv;
        }
        
        $profile->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil dilengkapi',
            'redirect' => 'reload'
        ]);
    }
    public function destroy(Employee $employee)
    {
        return response()->json([
            'alert' => 'success',
            'message' => 'Data tidak dapat dihapus',
        ]);
    }
    public function upload_cv(Request $request){
        $path = storage_path('tmp/employee_cv');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('file');
        $name = uniqid() . '_' . trim($file->getClientOriginalName());
        $file->move($path, $name);

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }
}
