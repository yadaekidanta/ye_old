<?php

namespace App\Http\Controllers\Office\Master;

use Illuminate\Http\Request;
use App\Models\Master\DueReminder;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class DueReminderController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = DueReminder::where('name','LIKE','%'.$request->keyword.'%')->paginate(10);;
            return $this->render_view('master.due_reminders.list',compact('collection'));
        }
        return $this->render_view('master.due_reminders.main');
    }
    public function create()
    {
        return $this->render_view('master.due_reminders.input',['data' => new DueReminder()]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:banks,name',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $data = new DueReminder();
        $data->name = $request->name;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Reminders '. __('custom.saved'),
        ]);
    }
    public function show(DueReminder $dueReminder)
    {
        //
    }
    public function edit(DueReminder $dueReminder)
    {
        return $this->render_view('master.due_reminders.input',['data' => $dueReminder]);
    }
    public function update(Request $request, DueReminder $dueReminder)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:banks,name,'.$dueReminder->id,
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $dueReminder->name = $request->name;
        $dueReminder->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Reminder '. __('custom.updated'),
        ]);
    }
    public function destroy(DueReminder $dueReminder)
    {
        $dueReminder->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Bank '. __('custom.deleted'),
        ]);
    }
}