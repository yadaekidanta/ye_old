<?php

namespace App\Http\Controllers\Office\Master;

use Illuminate\Http\Request;
use App\Models\Master\Product;
use App\Models\Master\ProductFile;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\Master\ProductCategory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    private $mediaCollection = 'photo';
    use OfficeResponseView;
    public function index(ProductCategory $productCategory, Request $request)
    {
        if ($request->ajax()) {
            $collection = Product::where('title','LIKE','%'.$request->keyword.'%')->where('product_category_id',$productCategory->id)->paginate(10);
            return $this->render_view('master.product.list',compact('collection'),);
        }
        // dd($productCategory);
        return $this->render_view('master.product.main',compact('productCategory'));
    }
    public function create(ProductCategory $productCategory)
    {
        return $this->render_view('master.product.input',['data' => new Product, 'productCategory' => $productCategory]);
    }
    public function store(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'product_name' => 'required|unique:products,name',
            'title' => 'required',
            'tags' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('product_name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_name'),
                ]);
            }
        }
        $data = new Product;
        if(request()->file('thumbnail')){
            Storage::delete($data->thumbnail);
            $thumbnail = request()->file('thumbnail')->store("thumbnail");
            $data->thumbnail = $thumbnail;
        }
        
        foreach ($request->input('product_gallery', []) as $file) {
            $data->addMedia(storage_path('tmp/product_gallery/' . $file))->toMediaCollection($this->mediaCollection);
        }
        $data->product_category_id = $request->product_category_id;
        $data->demo_url = $request->demo_url;
        $data->title = Str::title($request->product_name);
        $data->slug = Str::slug($request->product_name);
        $data->description = $request->description;
        $data->tags = $request->tags;
        $data->demo_url = $request->demo_url;
        $data->regular_price = $request->price_reguler;
        $data->extend_price = $request->price_extend;
        if ($request->price_reguler > 0){
            $data->is_regular_price = 1;
        } else{
            $data->is_regular_price = 0;
        }
        if ($request->price_extend > 0){
            $data->is_extend_price = 1;
        } else {
            $data->is_extend_price = 0;
        }
        $data->regular_discount = $request->regular_discount;
        $data->extend_discount = $request->extend_discount;
        $data->st = $request->status;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Product '. __('custom.saved'),
        ]);
    }
    public function show(Product $product)
    {
        return $this->render_view('master.product.show',['data' => $product]);
    }

    public function edit(ProductCategory $productCategory)
    {
        return $this->render_view('master.product.input',['data' => new Product, 'productCategory' => $productCategory]);
    }
    public function update(Request $request, Product $product)
    {
        $validator = Validator::make($request->all(), [
            'product_name' => 'required|unique:product_categories,name',
            'title' => 'required',
            'tags' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('product_name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('product_name'),
                ]);
            }
        }
        if(request()->file('thumbnail')){
            Storage::delete($product->thumbnail);
            $thumbnail = request()->file('thumbnail')->store("thumbnail");
            $product->thumbnail = $thumbnail;
        }
        foreach ($request->input('product_gallery', []) as $file) {
            $product->addMedia(storage_path('tmp/product_gallery/' . $file))->toMediaCollection($this->mediaCollection);
        }
        $product->product_category_id = $request->product_category_id;
        $product->demo_url = $request->demo_url;
        $product->title = Str::title($request->product_name);
        $product->slug = Str::slug($request->product_name);
        $product->description = $request->description;
        $product->tags = $request->tags;
        $product->demo_url = $request->demo_url;
        $product->regular_price = $request->price_reguler;
        $product->extend_price = $request->price_extend;
        if ($request->price_reguler > 0){
            $product->is_regular_price = 1;
        } else{
            $product->is_regular_price = 0;
        }
        if ($request->price_extend > 0){
            $product->is_extend_price = 1;
        } else {
            $product->is_extend_price = 0;
        }
        $product->regular_discount = $request->regular_discount;
        $product->extend_discount = $request->extend_discount;
        $product->st = $request->status;
        
        $product->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'productcategory '. __('custom.updated'),
        ]);
    }
    public function destroy( $productCategory, $product)
    {
        Product::where('id', $product)->where('product_category_id', $productCategory)->delete();
        return response()->json([
            'success' => true,
            'message' => 'product '. __('custom.deleted'),
        ]);
    }
}