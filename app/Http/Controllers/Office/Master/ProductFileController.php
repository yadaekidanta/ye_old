<?php

namespace App\Http\Controllers\Office\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductFileController extends Controller
{
    private $mediaCollection = 'photo';
    public function upload(Request $request)
    {
        $path = storage_path('tmp/product_file');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('file');
        $name = uniqid() . '_' . trim($file->getClientOriginalName());
        $file->move($path, $name);

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }
}