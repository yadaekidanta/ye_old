<?php

namespace App\Http\Controllers\Office\Master;

use App\Models\Master\Isic;
use Illuminate\Http\Request;
use App\Models\Master\IsicType;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class IsicTypeController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = IsicType::where('name', 'like', '%' . $request->keyword . '%')->paginate(10);
            return $this->render_view('master.isic_type.list', compact('collection'));
        }
        return $this->render_view('master.isic_type.main');
    }

    public function create()
    {
        return $this->render_view('master.isic_type.input', ['data' => new IsicType]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $isictype = new IsicType;
        $isictype->name = $request->name;
        $isictype->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }

    public function show(IsicType $isicType, Request $request)
    {
        if ($request->ajax()) {
            $collection = Isic::where('name', 'like', '%' . $request->keyword . '%')->where('isic_type_id',$isicType->id)->paginate(10);
            return $this->render_view('master.isic.list', compact('collection'));
        }
        return $this->render_view('master.isic.main', compact('isicType'));
    } 

    public function edit(IsicType $isicType)
    {
        return $this->render_view('master.isic_type.input', ['data' => $isicType]);
    }

    public function update(Request $request, IsicType $isicType)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $isicType->name = $request->name;
        $isicType->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }

    public function destroy(IsicType $isicType)
    {
        $isicType->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil dihapus',
        ]);
    }
}