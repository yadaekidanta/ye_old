<?php

namespace App\Http\Controllers\Office\Master;

use App\Models\Master\Isic;
use Illuminate\Http\Request;
use App\Models\Master\IsicType;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class IsicController extends Controller
{
    use OfficeResponseView;
    public function index(IsicType $isicType, Request $request)
    {
        if ($request->ajax()) {
            $collection = Isic::where('name', 'like', '%' . $request->keyword . '%')->where('isic_type_id',$isicType->id)->paginate(10);
            return $this->render_view('master.isic.list', compact('collection'));
        }
        return $this->render_view('master.isic.main', compact('isicType'));
    }
    
    public function create(IsicType $isicType)
    {
        return $this->render_view('master.isic.input', ['data' => new Isic, 'isicType' => $isicType]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }else if ($errors->has('code')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code'),
                ]);
            }
        }
        $isictype = new Isic;
        $isictype->isic_type_id = $request->isicType;
        $isictype->name = $request->name;
        $isictype->code = $request->code;
        $isictype->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }

    public function show( Isic $isic)
    {
        return $this->render_view('master.isic.show', ['data' => $isic]);
    }

    public function edit(IsicType $isicType, Isic $isic)
    {
        return $this->render_view('master.isic.input', ['data' => $isic, 'isicType' => $isicType]);
    }
    
    public function update(Request $request, IsicType $isicType, Isic $isic)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }else if ($errors->has('code')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('code'),
                ]);
            }
        }
        $isic->name = $request->name;
        $isic->code = $request->code;
        $isic->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    
    public function destroy(IsicType $isicType, Isic $isic)
    {
        $isic->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil dihapus',
        ]);
    }
}