<?php

namespace App\Http\Controllers\Office\Master;

use Illuminate\Http\Request;
use App\Models\Master\Product;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use App\Models\Master\ProductCategory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProductCategoryController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = ProductCategory::where('name','LIKE','%'.$request->keyword.'%')->paginate(10);
            return $this->render_view('master.product_category.list',compact('collection'));
        }
        return $this->render_view('master.product_category.main');
    }
    public function create()
    {
        $category = ProductCategory::where('product_category_id', '')->get();
        return $this->render_view('master.product_category.input',['data' => new ProductCategory,'category' => $category]);
    }
    public function create_modal()
    {
        $category = ProductCategory::where('product_category_id', '')->get();
        return $this->render_view('master.product_category.input_modal',['data' => new ProductCategory,'category' => $category]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_name' => 'required|unique:product_categories,name',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('category_name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('category_name'),
                ]);
            }
        }
        $data = new ProductCategory;
        if(request()->file('thumbnail')){
            Storage::delete($data->thumbnail);
            $thumbnail = request()->file('thumbnail')->store("thumbnail");
            $data->thumbnail = $thumbnail;
        }
        $data->name = $request->category_name;
        $data->product_category_id = $request->product_category_id;
        if($request->description){
            $data->description = $request->description;
        }
        if($request->meta_description){
            $data->tag_description = $request->meta_description;
        }
        $data->tag_title = $request->meta_title;
        $data->tag_keyword = $request->tag_keyword;
        $data->st = $request->status == "" ? "Unpublished" : $request->status;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Product Category '. __('custom.saved'),
        ]);
    }
    public function show(ProductCategory $productCategory)
    {
        return $this->render_view('master.product_category.show',['data' => $productCategory]);
    }
    public function edit(ProductCategory $productCategory)
    {
        $category = ProductCategory::where('product_category_id', '')->get();
        return $this->render_view('master.product_category.input',['data' => $productCategory,'category' => $category]);
    }
    public function update(Request $request, ProductCategory $productCategory)
    {
        $validator = Validator::make($request->all(), [
            'category_name' => 'required|unique:product_categories,name,'.$productCategory->id,
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('category_name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('category_name'),
                ]);
            }
        }
        if(request()->file('thumbnail')){
            Storage::delete($productCategory->thumbnail);
            $thumbnail = request()->file('thumbnail')->store("category");
            $productCategory->thumbnail = $thumbnail;
        }
        $productCategory->name = $request->category_name;
        $productCategory->product_category_id = $request->product_category_id;
        if($request->description){
            $productCategory->description = $request->description;
        }
        $productCategory->tag_title = $request->meta_title;
        if($request->meta_description){
            $productCategory->tag_description = $request->meta_description;
        }
        $productCategory->tag_keyword = $request->tag_keyword;
        $productCategory->st = $request->status == "" ? "Unpublished" : $request->status;
        $productCategory->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Product Category '. __('custom.saved'),
        ]);
    }
    public function destroy(Request $request)
    {
        foreach($request->id as $id){
            $productCategory = ProductCategory::find($id);
            if($productCategory->thumbnail != null){
                Storage::delete($productCategory->thumbnail);
            }
            $product = Product::where('product_category_id',$productCategory->id)->get();
            foreach($product as $p){
                if($p->thumbnail != null){
                    Storage::delete($p->thumbnail);
                }
                $p->delete();
            }
            $productCategory->delete();
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Product Category '. __('custom.deleted'),
        ]);
    }
}

?>