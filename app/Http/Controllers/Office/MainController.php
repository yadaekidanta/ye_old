<?php

namespace App\Http\Controllers\Office;

use Illuminate\Http\Request;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
    use OfficeResponseView;
    public function index()
    {
        if(Auth::guard('office')->user()->department_id == 0){
            if(Auth::guard('office')->user()->calculate_profile(Auth::guard('office')->user()) == 100){
                return redirect()->route('office.vacancy.index');
            }
            else{
                return redirect()->route('office.profile.index');
            }
        }
        else{
            return $this->render_view('dashboard.main');
        }
    }
    public function setting()
    {
        return $this->render_view('setting.main');
    }
    public function switch($language = '')
    {
        // Simpan locale ke session.
        request()->session()->put('locale', $language);
 
        // Arahkan ke halaman sebelumnya.
        return redirect()->back();
    }
}
