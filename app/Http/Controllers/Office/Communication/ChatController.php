<?php

namespace App\Http\Controllers\Office\Communication;

use App\Models\HRM\Employee;
use Illuminate\Http\Request;
use App\Models\Communication\Chat;
use App\Traits\OfficeResponseView;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ChatController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $collection = Employee::where('name','LIKE','%'.$request->keyword.'%')->where('department_id','!=',0)->where('id','!=', Auth::guard('office')->user()->id)->orderBy('position_id','ASC')->get();
            return $this->render_view('chat.list',compact('collection'));
        }
        return $this->render_view('chat.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'body' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('body')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('body'),
                ]);
            }
        }
        $data = new Chat;
        $data->from_id = Auth::guard('office')->user()->id;
        $data->to_id = $request->to_id;
        $data->body = $request->body;
        $data->type = 'e';
        $data->created_at = date('Y-m-d H:i:s');
        $data->save();
        return response()->json([
            'alert' => 'success',
        ]);
    }
    public function show(Request $request, Employee $employee)
    {
        $employees = Employee::where('name','LIKE','%'.$request->keyword.'%')->where('department_id','!=',0)->where('id','!=', Auth::guard('office')->user()->id)->orderBy('position_id','ASC')->get();
        if ($request->ajax()) {
            $collection = Chat::whereRaw('(from_id = '.$employee->id.' AND to_id = '.Auth::guard('office')->user()->id.') OR (from_id = '.Auth::guard('office')->user()->id.' AND to_id = '.$employee->id.')')->orderBy('created_at','ASC')->get();
            $collections = Chat::where('to_id', Auth::guard('office')->user()->id)->orderBy('created_at','ASC')->get();
            foreach($collections as $item)
            {
                if($item->from_id != Auth::guard('office')->user()->id){
                    $item->read_at = date('Y-m-d H:i:s');
                    $item->update();
                }
            }
            return $this->render_view('chat.show_list',compact('collection','employee'));
        }
        return $this->render_view('chat.show',compact('employee','employees'));
    }
    public function edit(Chat $chat)
    {
        //
    }
    public function update(Request $request, Chat $chat)
    {
        //
    }
    public function destroy(Chat $chat)
    {
        //
    }
    public function counter(){
        return response()->json([
            'total_notif' => Chat::where('to_id', Auth::guard('office')->user()->id)->where('read_at',null)->orderBy('created_at','ASC')->get()->count(),
        ]);
    }
}
