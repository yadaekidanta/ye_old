<?php

namespace App\Http\Controllers\Office\Setting;

use App\Models\Setting\Faq;
use Illuminate\Http\Request;
use App\Traits\OfficeResponseView;
use App\Models\Setting\FaqCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class FaqCategoryController extends Controller
{
    use OfficeResponseView;
    public function index(Request $request)
    {
        if ($request->ajax()){
            $collection = FaqCategory::where('name','LIKE','%'.$request->keyword.'%')->paginate(10);
            return $this->render_view('setting.faq_category.list',compact('collection'));
        }
        return $this->render_view('setting.faq_category.main');
    }
    public function create()
    {
        return $this->render_view('setting.faq_category.input', ['data' => new FaqCategory]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:faq_categories',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $faqCategory = new FaqCategory;
        $faqCategory->name = $request->name;
        $faqCategory->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function show(Request $request, FaqCategory $faqCategory)
    {
        if ($request->ajax()){
            $collection = Faq::where('question','LIKE','%'.$request->keyword.'%')->paginate(10);
            return $this->render_view('setting.faq_category.show_list',compact('collection'));
        }
        return $this->render_view('setting.faq_category.show', ['data' => $faqCategory]);
    }
    public function edit(FaqCategory $faqCategory)
    {
        return $this->render_view('setting.faq_category.input', ['data' => $faqCategory]);
    }
    public function update(Request $request, FaqCategory $faqCategory)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:faq_categories,'.$faqCategory->id,
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $faqCategory->name = $request->name;
        $faqCategory->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function destroy(FaqCategory $faqCategory)
    {
        Faq::where('faq_category_id',$faqCategory->id)->update(['faq_category_id' => 0]);
        $faqCategory->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
}
