<?php

namespace App\Http\Controllers\Office\Setting;

use App\Models\Setting\Faq;
use Illuminate\Http\Request;
use App\Traits\OfficeResponseView;
use App\Models\Setting\FaqCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class FaqController extends Controller
{
    use OfficeResponseView;
    public function index()
    {
        //
    }
    public function create(FaqCategory $faqCategory)
    {
        return $this->render_view('setting.faq.input',['data' => new Faq, 'category' => $faqCategory]);
    }
    public function store(Request $request, FaqCategory $faqCategory)
    {
        $validator = Validator::make($request->all(), [
            'question' => 'required|unique:faqs',
            'answer' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('question')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('question'),
                ]);
            }elseif ($errors->has('answer')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('answer'),
                ]);
            }
        }
        $faq = new Faq;
        $faq->faq_category_id = $faqCategory->id;
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function show(Faq $faq)
    {
        //
    }
    public function edit(FaqCategory $faqCategory, Faq $faq)
    {
        return $this->render_view('setting.faq.input',['data' => $faq, 'category' => $faqCategory]);
    }
    public function update(Request $request, FaqCategory $faqCategory, Faq $faq)
    {
        $validator = Validator::make($request->all(), [
            'question' => 'required|unique:faqs,question,'.$faq->id,
            'answer' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('question')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('question'),
                ]);
            }elseif ($errors->has('answer')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('answer'),
                ]);
            }
        }
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil disimpan',
        ]);
    }
    public function destroy(FaqCategory $faqCategory, Faq $faq)
    {
        $faq->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Data berhasil dihapus',
        ]);
    }
}
