<?php

namespace App\Http\Controllers\Web;

use App\Models\CRM\Inbox;
use App\Models\CRM\Project;
use App\Models\HRM\Employee;
use Illuminate\Http\Request;
use App\Models\CRM\Newsletter;
use App\Models\HRM\Department;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Stevebauman\Location\Facades\Location;
use App\Notifications\SubscribeNotification;
use Illuminate\Support\Facades\Notification;

class MainController extends Controller
{
    public function index()
    {
        $projects = Project::paginate(5);
        return view('pages.web.home.main',compact('projects'));
    }
    public function about()
    {
        $employee = Employee::where('department_id','!=',0)->get();
        return view('pages.web.about.main', compact('employee'));
    }
    public function identify()
    {
        return view('pages.web.identify.main');
    }
    public function service()
    {
        return view('pages.web.service.main');
    }
    public function case(Request $request)
    {
        if ($request->ajax()) {
            // return view('pages.web.case.list',compact('collection'));
        }
        $collection = Project::paginate(5);
        return view('pages.web.case.main',compact('collection'));
    }
    public function show_case(Project $project)
    {
        return view('pages.web.case.show',compact('project'));
    }
    public function contact()
    {
        $department = Department::whereIn('id', [8,9])->get();
        return view('pages.web.contact.main', compact('department'));
    }
    public function blog()
    {
        return view('pages.web.blog.main');
    }
    public function carrier(Request $request)
    {
        $ip = $request->ip();
        $data = Location::get($ip);
        dd($data);
        // return view('pages.web.carrier.main');
    }
    public function switch($language = '')
    {
        // Simpan locale ke session.
        request()->session()->put('locale', $language);
 
        // Arahkan ke halaman sebelumnya.
        return redirect()->back();
    }
    public function terms_use()
    {
        return view('pages.web.terms_use.main');
    }
    public function privacy_policy()
    {
        return view('pages.web.privacy_policy.main');
    }
    public function subscribe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:newsletter,email',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }
        }
        $data = new Newsletter;
        $data->email = $request->email;
        $data->save();
        Notification::send($data, new SubscribeNotification($data));
        return response()->json([
            'alert' => 'success',
            'message' => 'Successfully subscribe',
        ]);
    }
    public function send_messages(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'First_Name' => 'required',
            'Last_Name' => 'required',
            'Email' => 'required',
            'Messages' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('First_Name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('First_Name'),
                ]);
            }
            elseif ($errors->has('Last_Name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('Last_Name'),
                ]);
            }
            elseif ($errors->has('Email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('Email'),
                ]);
            }
            elseif ($errors->has('Messages')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('Messages'),
                ]);
            }
        }
        $data = new Inbox();
        $data->fname = $request->First_Name;
        $data->lname = $request->Last_Name;
        $data->department_id = $request->Department ? $request->Department : 0;
        $data->email = $request->Email;
        $data->messages = $request->Messages;
        $data->created_at = now();
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'We will contact you soon',
        ]);
    }
}
