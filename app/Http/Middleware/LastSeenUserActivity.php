<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\Models\CRM\Client;
use App\Models\HRM\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class LastSeenUserActivity
{
    public function handle(Request $request, Closure $next)
    {
        if (Auth::guard('customer')->check()) {
            $expireTime = Carbon::now()->addMinute(1); // keep online for 1 min
            Cache::put('is_client_online'.Auth::guard('customer')->user()->id, true, $expireTime);

            //Last Seen
            Client::where('id', Auth::guard('customer')->user()->id)->update(['last_seen' => Carbon::now()]);
        }elseif (Auth::guard('office')->check()) {
            $expireTime = Carbon::now()->addMinute(1); // keep online for 1 min
            Cache::put('is_employee_online'.Auth::guard('office')->user()->id, true, $expireTime);

            //Last Seen
            Employee::where('id', Auth::guard('office')->user()->id)->update(['last_seen' => Carbon::now()]);
        }
        return $next($request);
    }
}
