<?php

namespace App\View\Components;

use Illuminate\View\Component;

class CustomerLayout extends Component
{
    public function __construct()
    {
    }
    public function render()
    {
        return view('themes.customer.desktop.main');
    }
}
