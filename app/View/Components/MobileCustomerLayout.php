<?php

namespace App\View\Components;

use Illuminate\View\Component;

class MobileCustomerLayout extends Component
{
    public function __construct()
    {
    }
    public function render()
    {
        return view('themes.customer.mobile.main');
    }
}
