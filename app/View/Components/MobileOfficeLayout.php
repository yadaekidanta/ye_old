<?php

namespace App\View\Components;

use Illuminate\View\Component;

class MobileOfficeLayout extends Component
{
    public function __construct()
    {
    }
    public function render()
    {
        return view('themes.office.mobile.main');
    }
}
